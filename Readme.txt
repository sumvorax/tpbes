================================================================================

/// TODO ///
This is a set of programs solving the Smoluchowski-type kinetic equations using
the fast algorithms of linear and polylinear algebra.

Codes are developed for compilation with Intel Compilers (icc, icpc) using MKL
subroutines.

This program set is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

/// FIXME ///
================================================================================
    Contributors:
================================================================================

Sergey Matveev     -- project maintainer
Dmitry Zheltkov    -- libtt, solvers
Daniil Stefonishin -- libcp, multiple (3 and more) particles collisions solvers

/// FIXME ///
================================================================================
    Directory structure:
================================================================================

./                  -- Root of all the subroutines

./conf/             -- Configuration files and user-defined functions
                       for Population-Balance operators

./doc/              -- Documentation

./src/              -- Source files for the subroutines

./src/blas/         -- Blas function wrappers

./src/operator/     -- Classes implementing Population-Balance operators

./src/scheme/       -- Class implementing explicit finite-difference integration
                       schemes (RK1, RK2 and RK4)

./src/tensor/       -- Basic classes for handling low-rank matrices and low-rank
                       tensors in TT-format

./src/utility/      -- Utility functions 

./src/solver/       -- Solvers of various mathematical models of aggregation
                       and fragmentation processes

./test/             -- Tests

./tool/             -- Tools

/// FIXME ///
================================================================================
    References:
================================================================================

1.  Matveev S.A., Smirnov A.P. and Tyrtyshnikov E.E.
    "A fast numerical method for the Cauchy problem for the Smoluchowski
    equation."
    Journal of Computational Physics (2015)

2.  Matveev S.A., Smirnov A.P., Tyrtyshnikov E.E., Brilliantov N.V.
    "A fast numerical method for solving the Smoluchowski-type kinetic equations
    of aggregation and fragmentation processes."
    Vychislitel'nye Metody i Programmirovanie  (2014)

3.  A. A. Sorokin, V. F. Strizhov, M. N. Demin, A. P. Smirnov
    "Monte-Carlo Modeling of Aerosol Kinetics."
    Atomic Energy (2015)

4.  Ball R.C., et al.
    "Collective oscillations in irreversible coagulation driven by monomer
    inputs and large-cluster outputs."
    Physical review letters (2012)

5.  Brilliantov N. V., et al.
    "Size distribution of particles in Saturn’s rings from aggregation
    and fragmentation."
    Proceedings of the National Academy of Sciences (2015)

6.  Matveev S.A.
    "A Parallel Implementation of a Fast Method for Solving the
    Smoluchowski-Type Kinetic Equations of Aggregation and Fragmentation
    Processes."
    Vychislitel'nye Metody i Programmirovanie (2015)

7.  Zheltkov D.A. Tyrtyshnikov E.E
    "A parallel implementation of the matrix cross approximation method",
    Vychislitel'nye Metody i Programmirovanie (2015)

8.  Oseledets I.V., Saluev T.G., Savostyanov D.V., Dolgov S.V.
    Tensor Train Toolbox, python version
    https://github.com/oseledets/ttpy

9.  Smirnov A.P., Matveev S.A., Zheltkov D.A., Tyrtyshnikov E.E.
    "Fast and accurate finite-difference method solving multicomponent
    Smoluchowski coagulation equation with source and sink terms."
    Procedia computer science, 80:2141–2146 (2016)

10. Matveev S.A., Zheltkov D.A., Tyrtyshnikov E.E., and Smirnov A.P.
    "Tensor train versus monte carlo for the multicomponent smoluchowski
    coagulation equation"
    Journal of Computational Physics, 316:164–179, (2016)

11. Matveev S.A., Stadnichuk V., Smirnov A. P., Tyrtyshnikov E.E.,
    Ampilogova N., Brilliantov N. V.
    "Anderson acceleration method of finding steady-state particle size
    distribution for a wide class of aggregation-fragmentation models."
    Computer Physics Communications (accepted on Nov 7 2017)

================================================================================
