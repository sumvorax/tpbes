set terminal png
set output "test.png"
n=15 #number of intervals
max=3. #max value
min=-3. #min value
width=(max-min)/n #interval width
#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0
set boxwidth width*0.9
set style fill solid 0.5 # fill style

bin(x,width)=width*floor(x/width)+width/2.0

countpoints(file) = system( sprintf("grep -v '^#' %s| wc -l", file) )
file1count = countpoints ("res.dat")

t=0.5
#set xrange [0:10]
#set logscale y
#set yrange [1e-3:1]
#set logscale x
ker=""
plot "res".ker.".dat" using (bin(($1),width)):( 1.0/(width*file1count)) smooth freq with boxes t 'start',\
  "fin".ker.".dat" using    (bin(($1),width)):( 1.0/(width*file1count)) smooth freq with boxes t 'fin',\
      exp(-x) lw 3,\
      exp(-x/(1+t/2)) / (1+t/2)**2 lw 4;

set output 'test2.png'

plot "res".ker.".dat" using (bin(($1),width)):(bin(($1),width) * 1.0/(width*file1count)) smooth freq with boxes t 'start',\
     "fin".ker.".dat" using (bin(($1),width)):(bin(($1),width) * 1.0/(width*file1count)) smooth freq with boxes t 'fin',\
     "values.dat" u 1:2 w l lw 7,\
     x* exp(-x) lw 3,\
     x * exp(-x/(1+t/2)) / (1+t/2)**2 lw 4;
quit

set output 'test3.png'


file2count = countpoints ("fin.dat")
file3count = countpoints ("fin_ball3.dat")

plot "fin.dat" using (bin(($1),width)):(bin(($1),width) * 1.0/(width*file2count)) smooth freq with boxes t 'const',\
     "fin_ball3.dat" using (bin(($1),width)):(bin(($1),width) * 1.0/(width*file3count)) smooth freq with boxes t 'ball'
