#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int cmpfunc (const void * a, const void * b);
int max(int a, int b);
void delete_and_insert(double *s, double *new_s, const int i, const int j, const int N);
void insert_particle(double *s, double *new_s, double particle, const int N);
void add_fragments(double *s, double *new_s, const int n1, const int n2, const int N);
