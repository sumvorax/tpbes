//#include "mkl.h"
#include "cross.h"
#include <stdio.h>
#include <cblas.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

double a(int i, int j)
{
    double u = ((double) i) + 1.;
    double v = ((double) j) + 1.;
    return 2.0 + pow(u, 1.0/3.0) * pow(v, -1.0/3.0) + pow(u, -1.0/3.0) * pow(v, 1.0/3.0);
        return i + j + 2.;
    //return 1.0/(i+1.0) + 1.0/(j+1.0);
    //return (sqrt(1.0/(i+1) + 1.0/(j+1)));
        double h = 1e-1;
        //return exp( i* h * j * h); 
        //return exp(- i * h - j * h) * gsl_sf_bessel_I0(2.0 * sqrt( i * h * j * h ));
    //return 1.0/(i + j + 2.0);//i+j+1.0;
    return sqrt(i+j+1.0);

}

double* low_rank_matvec(double *U, double *V, int rank, int n, double* x, const char option)
{
//  options
//
//  f -- full            matvec
//  l -- lower      triangle matvec
//  u -- upper      triangle matvec
//  p -- lower persymmetric triangle matvec
//  q -- upper persymmetric triangle matvec
// allocate memory
    int R = rank;
    int M = n;
    int N = n;
    double *result = (double*) malloc( M * sizeof(double) );
    double w;
    
    if (option == 'f')
    {
        //full matvec
        double *buff  = (double*) malloc( R * sizeof(double) );
        // compute transpose(V)*x
        #pragma omp parallel for
        for (int j = 0; j < R; j++)
        {   
            buff[j] = 0.0;
            for (int i = 0; i < M; i++)
                buff[j] = buff[j] + V[R * i + j] * x[i];
        }

        // compute U * (V*x)

        #pragma omp parallel for
        for (int i = 0; i < M; i++)
        {
            result[i] = 0.0;
            for (int j = 0; j < R; j++)
                result[i] = result[i] + U[R * i + j] * buff[j];
        }

        //free memory
        free(buff);
        buff = NULL;
        return result;
    }
    else if (option == 'p')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
            //lower persymmetric triangle
        //#pragma omp parallel for private(w) 
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + (N - 1 - j)] * x[N - 1 - j];
                //workspace[M * omp_get_thread_num() * M + j] += w * U[M * i + j];
                result[j] += w * U[M * i + j];
            }
        }
    }
    else if (option == 'l')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
        result[i] = 0.0;
        //lower triangle
        //#pragma omp parallel for
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + j] * x[j];
                //workspace[M * omp_get_thread_num() + j] += w * U[ M * i + j]
                result[j] += w * U[M * i + j];
        }
        }
    }
    else if (option == 'u')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
        // upper triangle   
        //#pragma omp parallel for 
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + (N - 1 - j)] * x[N - 1 - j];
                    //workspace[M * omp_get_thread_num() + N - 1 - j] += w * U[ M * i + N - 1 - j]
                result[N - 1 - j] += w * U[M * i + N - 1 - j];
            }       
        }
    }
    else if (option == 'q')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
        //upper persymmetric triagnle
        //#pragma omp parallel for 
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + j] * x[j];
                //workspace[M * omp_get_thread_num() + N - 1 - j] += w * U[ M * i + N - 1 -j]
                result[N - j - 1] += w * U[M * i + N - j - 1];
            }
        }
    }
    else
    {
        printf("incorrect matvec option\nreturn null\n");
        free(result);
        result = NULL;
    }
    return result;
}


int cross_mod(double (A(int i, int j, double *ddata)), int n, double *U, double *V, int decomposition_rank, double maxeps, double *data)
{
// returns estimated rank
    char if_print = 1;
    int r, i, j, i_p, j_p, j_upd, seed;
    double *u, *v; // vectors for skeleton;
    double u_norm =0.0, v_norm =0.0, UV_norm =0.0, UV_element, max;//for norms
    double U_norm = 0.0; 
    double V_norm = 0.0;
    u = (double *) malloc (n * sizeof(double));
    v = (double *) malloc (n * sizeof(double));

    //srand(seed);
    //j_p = rand() % n;//starting column
    j_p = 0;
    for ( r = 0; r < decomposition_rank; r++)
    {
        if (if_print)
        {
            printf("sweep %d\n", r);
            printf("j_p = %d\n", j_p);
        }
        max = 0.0;
        for ( i = 0; i < n; i++)//search max through the column and construct vector u_r
        {
            u[i] = A(i, j_p, data);
            for ( j = 0; j < r; j++)
                u[i] -= U[decomposition_rank * i + j] * V[decomposition_rank * j_p + j];

            if (fabs(u[i]) > max)
            {
                i_p = i;// position of the column max
                max = fabs(u[i]);
            }
        }

        if (if_print)
            printf("i_p = %d, max %lf\n", i_p, max);
        max = 0.0;
        for ( i = 0; i < n; i++)//search max through the line and construct vector v_r
        {
            v[i] = A(i_p, i, data);
            for ( j = 0; j < r; j++)
                v[i] -= U[decomposition_rank * i_p + j] * V[decomposition_rank * i + j];

            if ((i != j_p) && ( fabs(v[i]) > max) )
            {
                j_upd = i;// position of the line max
                max = fabs(v[i]);
            }
        }
         

        for( i = 0; i < n; i++)//fullfill the skeleton into U and V factors
        {
            if (v[j_p] != 0)
            {
                u[i] /= v[j_p];
                U[ decomposition_rank * i + r] = u[i];
                V[ decomposition_rank * i + r] = v[i];
            }
            else
            {
                //printf("I don't like it but ok\n");
                u[i] = 0.0;
                U[ decomposition_rank * i + r] = 0.0;
                V[ decomposition_rank * i + r] = 0.0;
            }
        }

        /*if (r < decomposition_rank)
        {
            double el; 
            max = 0.0;
            for ( i = 0; i < n; i++)//search max through the line and construct vector v_r
            {
                el = A(i_p, i, data);
                for ( j = 0; j < r + 1; j++)
                    el -= U[decomposition_rank * i_p + j] * V[decomposition_rank * i + j];
                if ((i != j_p) && ( fabs(el) > max) )
                {
                    j_upd = i;// position of the line max
                    max = fabs(el);
                }
            }
        }*/
        j_p = j_upd;//update j_p for the next step

        if (max == 0)
        {
            printf("seems like we need to stop!\n");
            return r;
        }

        //check stopping criteria ||u||_2 * ||v||_2 <= maxeps * ||A_r||_F <= maxeps * ||U_r||_F * ||V_r||_F 
        // we check just ||u||_2 * ||v||_2 <= maxeps * ||U_r||_F * ||V_r||_F
        u_norm  = cblas_dnrm2(n, u, 1);
        v_norm  = cblas_dnrm2(n, v, 1);
        U_norm = U_norm + u_norm * u_norm;// (||U||_F)**2
        V_norm = V_norm + v_norm * v_norm;// (||V||_F)**2
        UV_norm = sqrt(U_norm * V_norm);
        //UV_norm =  cblas_dnrm2(n * (r + 1), U, 1) * cblas_dnrm2(n * (r + 1), V, 1);
        if ( if_print && ((r + 1) == 1) )
            printf("debug %lf\n", UV_norm - u_norm * v_norm);
        if(if_print)
            printf("eps = %E\n\n", (n - r - 1) * u_norm  * v_norm / UV_norm);

        if ( (n - r - 1) * u_norm * v_norm < maxeps * UV_norm)
        {
            printf("u_norm %lf v_norm %lf UV_norm %lf\n", u_norm, v_norm, UV_norm);
            printf("succeded with stopping criteria\n rank = %d, maxrank = %d, maxeps = %E\n", r + 1, decomposition_rank, maxeps);
            return r + 1;
        }
    }
    return r + 1;
}



int cross(double (A(int i, int j)), int n, double *U, double *V, int decomposition_rank, double maxeps)
{
// returns estimated rank
    char if_print = 1;
    int r, i, j, i_p, j_p, j_upd, seed;
    double *u, *v; // vectors for skeleton;
    double u_norm =0.0, v_norm =0.0, UV_norm =0.0, UV_element, max;//for norms
    double U_norm = 0.0; 
    double V_norm = 0.0;
    u = (double *) malloc (n * sizeof(double));
    v = (double *) malloc (n * sizeof(double));

    //srand(seed);
    //j_p = rand() % n;//starting column
    j_p = 0;
    for ( r = 0; r < decomposition_rank; r++)
    {
        if (if_print)
        {
            printf("sweep %d\n", r);
            printf("j_p = %d\n", j_p);
        }
        max = 0.0;
        for ( i = 0; i < n; i++)//search max through the column and construct vector u_r
        {
            u[i] = A(i, j_p);
            for ( j = 0; j < r; j++)
                u[i] -= U[decomposition_rank * i + j] * V[decomposition_rank * j_p + j];

            if (fabs(u[i]) > max)
            {
                i_p = i;// position of the column max
                max = fabs(u[i]);
            }
        }
        if (if_print)
            printf("i_p = %d\n", i_p);
        max = 0.0;
        for ( i = 0; i < n; i++)//search max through the line and construct vector v_r
        {
            v[i] = A(i_p, i);
            for ( j = 0; j < r; j++)
                v[i] -= U[decomposition_rank * i_p + j] * V[decomposition_rank * i + j];

            if ((i != j_p) && ( fabs(v[i]) > max) )
            {
                j_upd = i;// position of the line max
                max = fabs(v[i]);
            }
        }
        
        if (max == 0)
        {
            printf("seems like we need to stop!\n");
            return r;
        }
        for( i = 0; i < n; i++)//fullfill the skeleton into U and V factors
        {
            u[i] /= v[j_p];
            U[ decomposition_rank * i + r] = u[i];
            V[ decomposition_rank * i + r] = v[i];
        }
        j_p = rand() %n;//j_upd;//update j_p for the next step

        //check stopping criteria ||u||_2 * ||v||_2 <= maxeps * ||A_r||_F <= maxeps * ||U_r||_F * ||V_r||_F 
        // we check just ||u||_2 * ||v||_2 <= maxeps * ||U_r||_F * ||V_r||_F
        u_norm  = cblas_dnrm2(n, u, 1);
        v_norm  = cblas_dnrm2(n, v, 1);
        //UV_norm = UV_norm + U_norm * v_norm * v_norm + V_norm * u_norm * u_norm + u_norm * v_norm;
        U_norm = U_norm + u_norm * u_norm;// (||U||_F)**2
        V_norm = V_norm + v_norm * v_norm;// (||V||_F)**2
        UV_norm = sqrt(U_norm * V_norm);
        //UV_norm =  cblas_dnrm2(n * (r + 1), U, 1) * cblas_dnrm2(n * (r + 1), V, 1);
        if ( if_print && ((r + 1) == 1) )
            printf("debug %lf\n", UV_norm - u_norm * v_norm);
        if(if_print)
            printf("eps = %E\n\n", (n - r - 1) * u_norm  * v_norm / UV_norm);

        if ( (n - r - 1) * u_norm * v_norm < maxeps * UV_norm)
        {
            printf("u_norm %lf v_norm %lf UV_norm %lf\n", u_norm, v_norm, UV_norm);
            printf("succeded with stopping criteria\n rank = %d, maxrank = %d, maxeps = %E\n", r + 1, decomposition_rank, maxeps);
            return r + 1;
        }
    }
    return r + 1;
}

void reallocate_to_real_rank(double **U, double **V, int n, int rank, int real_rank)
{
//rank -- overestimated rank
// real_rank -- real rank <= rank
        if (rank <= real_rank)
        {
            printf("confusion \n");
            return;
        }
        int i; int j;
    double *_U = (double *) malloc(n * real_rank * sizeof(double));
    double *_V = (double *) malloc(n * real_rank * sizeof(double));
    for (i = 0; i < n; i++)
        for (j = 0; j < real_rank; j++)
        {
            _U[real_rank * i + j] = U[0][rank * i + j];
            _V[real_rank * i + j] = V[0][rank * i + j];
        }
    free(U[0]);
    free(V[0]);
    U[0] = _U;
    V[0] = _V;
        return;
}

/*
int main(int argc, char **argv)
{
    int n = 5000, rank = 10, real_rank;
    double *U, *V, el, maxeps = 1e-6;
    int i,j,r;
    if (argc < 4)
    {
        printf("need arguments : n , maxrank maxeps\n");
        return 1;
    }
    sscanf(argv[1], "%d", &n);
    sscanf(argv[2], "%d", &rank);
    sscanf(argv[3], "%lf", &maxeps);
    U = (double *) malloc (n * rank * sizeof(double));
    V = (double *) malloc (n * rank * sizeof(double));
    real_rank = cross (a, n, U, V, rank, maxeps);

    printf("reallocate the memory for U, V\n");
    //reallocate memory
    double *_U = (double *) malloc(n * real_rank * sizeof(double));
    double *_V = (double *) malloc(n * real_rank * sizeof(double));
    for (i = 0; i < n; i++)
        for (j = 0; j < real_rank + 1; j++)
        {
            _U[real_rank * i + j] = U[rank * i + j];
            _V[real_rank * i + j] = V[rank * i + j];
        }
    free(U);
    free(V);
    U = _U;
    V = _V;
    _U = NULL;
    _V = NULL;
        //compute the error
    double error = 0.0, full_norm = 0.0 ;
    if ( n <= 10000)
    {
                printf("compute the error\n");
                #pragma omp parallel for reduction (+:full_norm) reduction(+:error) private(el,j,r)
        for(i = 0; i < n; i++)
        {
            for (j = 0; j < n; j++)
            {
                el = 0.0;   
                for(r = 0; r < real_rank; r++)
                {
                    el += U[real_rank * i + r] * V[real_rank * j + r];
                }
                full_norm += a(i,j) * a(i,j);
                error += (el - a(i,j)) * (el - a(i,j));
                //printf("%lf ", el);
            }
            //printf("\n");
        }
        printf("error norm = %E\n", sqrt(error/full_norm) );
                printf("compute matvec\n");
                double *ones = (double *) malloc (n * sizeof(double));
                double *res1 = (double *) malloc (n * sizeof(double));
                #pragma omp parallel for
                for (i = 0; i < n; i++)
                {
                    ones[i] = 1.0;
                    res1[i] = 0.0;
                }
                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < n; j++)
                        res1[i] = res1[i] + a(i, j) * ones[j];
                }
                double *res2 = low_rank_matvec(U, V, real_rank, n, ones, 'f');
                cblas_daxpy(n, -1., res1, 1, res2, 1);
                double matvec_err = cblas_dnrm2(n, res2, 1);
                printf("error matvec = %E\n", matvec_err);
    }
        free(U);
        free(V);
    return 0;
        
}
*/
