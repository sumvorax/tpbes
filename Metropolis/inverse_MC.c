#include "cross.h"
#include "generate_samples.h"
#include "manipulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double K2(double u, double v)
{
//    return 1.0;//0.01;
    return 2.0 + pow(u, 1.0/3.0) * pow(v, -1.0/3.0) + pow(u, -1.0/3.0) * pow(v, 1.0/3.0);
    return pow(pow(u, 1.0/3.0) + pow(v, 1.0/3.0), 2.0l) * sqrt(1.0 / u + 1.0 / v);
}

double K1(int i, int j, double *data)
{    
    double u = data[i];
    double v = data[j]; 
    return K2(u, v);
    //return 1.0 + pow(u, 1.0/3.0) * pow(v, -1.0/3.0) + pow(u, -1.0/3.0) * pow(v, 1.0/3.0);
    //return pow(pow(data[i], 1.0/3.0) + pow(data[j], 1.0/3.0), 2.0l) * sqrt(1.0 / data[i] + 1.0 / data[j]);
}


double target_p(double x)
{ // initial PSD
    return exp(-fabs(x))/2.0;;
}

double *DSMC_step(double *samples, int n_part, double (*K)(int, int, double *), double *tau, double *current_shift)
{
    int i, i_coll, j_coll;

    int rank = 30;
    double maxeps = 1e-8;
    
    double *new_samples;

    new_samples = (double *) malloc( (n_part - 1) * sizeof(double));
    
    if (tau[0] < 0)
    {
        printf("Khren'!\n");
        exit(4);
    }
    i_coll = rand() % n_part;
    j_coll = rand() % n_part;
    while (i_coll == j_coll)    
            j_coll = rand() % n_part;
    current_shift[0] = 0.0;

    //int f = 1;
    while (1)// sample collision
    {   // collision event, shift time
        //printf("sample collision\n");
        double p = my_random(0, 1);
        double p_coll = K(i_coll, j_coll, samples) / tau[0] * n_part * (n_part - 1); 
        if (p <= p_coll) // accept merge and finish step
        {
            // update tau
            for (i = 0; i < n_part; i++) 
            {
                if ((i != i_coll) && (i!= j_coll))
                {
                    tau[0] = tau[0] - 2. * K(i_coll, i, samples) - 2. * K(i, j_coll, samples);
                }
            }
            tau[0] = tau[0] - 2. * K(i_coll, j_coll, samples);// avoid account this pair twice
            for (i = 0; i < n_part; i++)
                if ((i != i_coll) && (i != j_coll))
                {
                    tau[0] = tau[0] + 2. * K2(samples[i_coll] + samples[j_coll], samples[i]);
                }
            delete_and_insert(samples, new_samples, i_coll, j_coll, n_part);
            //printf("success after  %d \ntries\n", f);
            return new_samples;
        }
        else // just collision, no merge
        {
            //current_shift[0] = current_shift[0] + 2.0 / tau[0];// 2.0 /  n_part / (n_part - 1) / max;
            //printf("fail %d \ntry again\n", f);
            //f = f + 1;
            i_coll = rand() % n_part;
            j_coll = rand() % n_part;
            while (i_coll == j_coll)    
                j_coll = rand() % n_part;
        }
    }
    //return new_samples;
}

void DSMC_init_step(double *samples, int n_part, double (*K)(int, int, double *), double *tau)
{
    int i, j, i_coll, j_coll;
    i_coll = rand() % n_part;
    j_coll = rand() % n_part;
    int rank = 30;
    double maxeps = 1e-8;
    
    double *U, *V;
    U = (double *) calloc (n_part * rank , sizeof(double));
    V = (double *) calloc (n_part * rank , sizeof(double));
    int real_rank = cross_mod (K1, n_part, U, V, rank, maxeps, samples);// reapproximate kernel
    printf("real rank = %d\n", real_rank); 
    reallocate_to_real_rank(&U, &V, n_part, rank, real_rank);
    double *ones = (double *) malloc(n_part * sizeof(double));
    for (i = 0; i < n_part; i++)
        ones[i] = 1.0;    
    double *x = low_rank_matvec(U, V, real_rank, n_part, ones, 'f');
    tau[0] = 0.0;
    for (i = 0; i < n_part; i++)// substract diagonal
    {
        tau[0] = tau[0] + x[i] - K(i, i, samples);
        if (x[i] < 0.0)
        {
            printf("Trouble with approximation of kernel!\n");
            exit(2);
        }
    }
    
    double tau_val = 0.0;
    for (i = 0; i < n_part; i++)
        for (j = 0; j < n_part; j++)
        {
            if ( i != j)
                tau_val = tau_val + K(i, j, samples);
        }

    printf("full val error %lf\n", (tau[0] - tau_val));
    printf("check %lf\n", (tau[0] - K2(1.0, 1.0) * n_part * (n_part - 1.)));
    
    tau[0] = tau_val;
    printf("Init tau %E\n", tau[0]);
    //tau = 2.0 / tau; // DSMC normalizer and mean time-step//
    free(U);
    free(V);
    free(x);
    free(ones);
}


int main (int argc, char **argv) 
{
    int i, j, n;
    time_t t;
    double *samples;
    n = 5000;
    double target_time = 2.0;
    sscanf(argv[1], "%d", &n);
    sscanf(argv[2], "%lf", &target_time);
    int tot_n = n;
    samples = (double *) malloc(n * sizeof(double));
    srand((unsigned) time(&t));
    i = 0;
    // Metropolis-Hastings
    samples = create_samples(n, target_p, 2.0, 0.0);
    for (i = 0; i < n; i++)
       samples[i] = fabs(samples[i]);
    // mono-disperse init conditions
    for (i = 0; i < n; i++)
       samples[i] = 1.0;
    qsort(samples, n, sizeof(double), cmpfunc);
    FILE *DATA_samples = fopen("res.dat", "w+");    
    for (i = 0; i < n; i++)
       fprintf(DATA_samples, "%lf\n", (samples[i]));
    fclose(DATA_samples);
    
    int rank, real_rank;
    rank = 40;
    double maxeps = 1e-6;
    
    double current_time = 0.0;
    double current_shift = 0.0;
    int n_part = n;
    double *new_samples;
    double tau;
    
    printf("Init OK\n");
    double mean_mass = 0.0;
    double init_mass = 0.0;
    double nmean_mass;
    for (j = 0; j < n_part; j++)
    {
        init_mass = init_mass + samples[j];
        mean_mass = mean_mass + samples[j]; 
    }
    init_mass = init_mass / n_part;
    mean_mass = mean_mass / init_mass;

    int n_mc_steps = n / 2;
    double cell_size = 1.0;
    tau = n_part * (n_part - 1.0) * K2(1.0, 1.0); // for mono-disperse init cond
    //DSMC_init_step(samples, n_part, K1, &tau); // general init cond
    
    while ( (current_time < target_time))
    {
        for (i = 0; i < n_mc_steps; i++)
        {
            mean_mass = 0.0;
            for (j = 0; j < n_part; j++)
                mean_mass = mean_mass + samples[j];
            mean_mass = mean_mass / n_part;
            new_samples = DSMC_step(samples, n_part, K1, &tau, &current_shift);
           
            nmean_mass = 0.0;
            for (j = 0; j < n_part - 1; j++)
                nmean_mass = nmean_mass+ new_samples[j]; 
            nmean_mass = nmean_mass / (n_part - 1);
            
            current_time = current_time + 2.0 / tau * n_part * (n_part - 1) * (nmean_mass - mean_mass) / init_mass;
            n_part = n_part - 1;
            
            //printf("mean move = %lf\n", nmean_mass - mean_mass);
            /*double prop = K1(n_part - 1, 0, new_samples) / K1(0, 0, new_samples);*/
            
            if (current_shift < 0.0)
            {
                printf("FAILURE, negative time shift ... %E!!!\n", current_shift);
                exit(1);
            }

            free(samples); 
            samples = new_samples;
            
            //mean_mass = mean_mass;
            printf("%d %E %lf %lf\n", i+1, current_time, cell_size, ((double)n_part ) / tot_n);

            if ( (current_time) > target_time)
            {
                //printf("We Should Stop1\n");
                break;
            }
            //printf("MC step %d done, cur time %E\n", i+1, current_time);
        }
        // debug break, no cloning!
        //break;
        if ( (current_time) > target_time)
        {
            //printf("We Should Stop2\n");
            printf("step %d time = %E N = %lf\n", i+1, current_time, ((double)n_part ) / tot_n);
            printf("n_part = %d\n max_n = %d mass = %lf cell_size = %lf\n", n_part, n, nmean_mass * ((double) n_part), cell_size);
            break;
        }
        //printf("CLONE ALL PARTICLES\n");// it means that we double the cell; hence tot numbers of particles virtually double
        new_samples = (double *) malloc (n * sizeof(double));// clone all particles

        double re_init_validate = 4.0 * tau;
        for (i = 0; i < n_part; i++)// n_part = n / 2 after n_mc_steps
        {
            new_samples[2 * i] =  samples[i];
            new_samples[2 * i + 1] = samples[i];
            re_init_validate = re_init_validate + 2.0 * K1(i, i, samples);
        }
        free(samples);

        samples = new_samples;        
        n_part = n; 
        tot_n = tot_n * 2.;
        cell_size = cell_size * 2.;
        //current_shift = 0.0;
        //DSMC_init_step(samples, n_part, K1, &tau);
        tau = re_init_validate;
    } 
    DATA_samples = fopen("fin.dat", "w+");
    for (i = 0; i < n_part; i++)
    {
       fprintf(DATA_samples, "%lf\n", (samples[i]));
    }
    fclose(DATA_samples);
    free(samples);
    return(0);
}
