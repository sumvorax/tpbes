#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double my_random(double left, double right);
double majorant(double x, double mu);
double *create_samples(int n, double (*P)(double x), const double sigma_p, const double mu_p);
