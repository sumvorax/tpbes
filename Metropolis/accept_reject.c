#include "manipulation.h"
#include "generate_samples.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


double lambda=0.005;

double K2(double u, double v)
{
//    return 1.;
  return pow(u, 0.95) * pow(v, -0.95) + pow(u, -0.95) * pow(v, 0.95);
    return 2.0 + pow(u, 1.0/3.0) * pow(v, -1.0/3.0) + pow(u, -1.0/3.0) * pow(v, 1.0/3.0);
    return pow(pow(u, 1.0/3.0) + pow(v, 1.0/3.0), 2.0l) * sqrt(1.0 / u + 1.0 / v);
}

double K1(int i, int j, double *data)
{    
    double u = data[i];
    double v = data[j]; 
    return K2(u, v);
    //return 1.0 + pow(u, 1.0/3.0) * pow(v, -1.0/3.0) + pow(u, -1.0/3.0) * pow(v, 1.0/3.0);
    //return pow(pow(data[i], 1.0/3.0) + pow(data[j], 1.0/3.0), 2.0l) * sqrt(1.0 / data[i] + 1.0 / data[j]);
    
}

double target_p(double x)
{ 
    return exp(-fabs(x))/2.0;;
/*    double sigma = 0.3;
    double ssigma = sigma * sigma;
    double mu = 2.0;
    double mu1 = -1.0;

    double sigma1 = 2.0;
    double ssigma1 = sigma1 * sigma1;
    return 1.0 / sigma / sqrt(2.0 * M_PI) * exp(-0.5 * (x - mu) * (x - mu) / ssigma) +
           1.0 / sigma1 /sqrt(2.0 * M_PI) * exp(-0.5 * (x - mu1) * (x - mu1) / ssigma1);
*/
}

double source_work(double tau, double power, double size)
{// const power source
    return power * size * tau;
}

double *DSMC_fast_step(double *samples, int *nn_part, double (*K)(int, int, double *), double *current_shift, double *source)
{
    int i, i_coll, j_coll;
    int rank = 30;
    double maxeps = 1e-6;
    double *new_samples;
    int n_part = nn_part[0];
    double max = K(0, n_part - 1, samples);// find max {K(i,j)}// particles are sorted, the last is the largest!
    
    for (i = 0; i < n_part; i++)
    {
        if (K(i, n_part - 1, samples) > max)
            max = K(i, n_part - 1, samples);
    }
    i_coll = rand() % n_part;
    j_coll = rand() % n_part;
    while (i_coll == j_coll)    
            j_coll = rand() % n_part;
    current_shift[0] = 0.0;
    double spec_const = 1.0 / n_part / (n_part - 1);
    current_shift[0] = 0.0;
    double renorm = 1.0 / (1.0 + lambda);
    double renorm2 = lambda * renorm;
    while (1)// sample collision
    {   // collision event, shift time
        //printf("sample collision\n");
        double p = my_random(0, 1);
        double p_coll = K(i_coll, j_coll, samples) / max; // 
        //printf("p_coll %E, p %E \n", p_coll, p);
        double ttau = 2.0 / (n_part - 1) / n_part / max * renorm;
        current_shift[0] = current_shift[0] + ttau;/*2.0 / (n_part - 1) / n_part / max;*/
        if (p <= p_coll) // accept event
        {
            double p = my_random(0, 1);
            if (p > renorm2)
            {//merge
                new_samples = (double *) malloc( (n_part - 1) * sizeof(double));
                delete_and_insert(samples, new_samples, i_coll, j_coll, n_part);
                nn_part[0] = n_part - 1;
                source[0] = source[0] + source_work(1.0, ttau, 1.0); 
                return new_samples;
            }        
            else
            {// break
                int number_of_fragments = (int) (samples[i_coll] + samples[j_coll]);
                //printf("fragmentation success\n s[i_coll] %lf s[j_coll] %lf num_frag %d\n", samples[i_coll], samples[j_coll], number_of_fragments);
                new_samples = (double *) malloc( (n_part + number_of_fragments - 2) * sizeof(double));
                add_fragments(samples, new_samples, i_coll, j_coll, n_part);
                nn_part[0] = n_part + number_of_fragments - 2;
                return new_samples;
            }
        }
        else // collision, no merge
        {            
            //printf("fail\ntry again\n");
            i_coll = rand() % n_part;
            j_coll = rand() % n_part;
            while (i_coll == j_coll)    
                j_coll = rand() % n_part;
        }
    }
    //return new_samples;
}

int main (int argc, char **argv) {
    int i, n;
    
    double proposal, verification;
    time_t t;
    double *samples;
    n = 5000;
    
    int if_checkpoint = 1;
    double info_time_step = 0.1, info_time = 0.0;
    double target_time = 2.0;
    if (argc < 6)
    {
        printf("Parameters expected: n_part Tfin lambda ptimestep ifcheckpoints\n");
        exit(1);
    }
    sscanf(argv[1], "%d", &n);
    sscanf(argv[2], "%lf", &target_time);
    sscanf(argv[3], "%lf", &lambda);
    sscanf(argv[4], "%lf", &info_time_step);
    sscanf(argv[5], "%d", &if_checkpoint);
    int tot_n = n;
    samples = (double *) malloc(n * sizeof(double));
    srand((unsigned) time(&t));
 
    i = 0;
    double x_t;
    double candidate;
    double a1, a2, a;
    
    x_t = my_random(0, 1);// init
 
    //samples = create_samples(n, target_p, 2.0, 0.0);
    //
    //for (i = 0; i < n; i++)
    //   samples[i] = fabs(samples[i]);

    for (i = 0; i < n; i++)
       samples[i] = 1.0;
    //qsort(samples, n, sizeof(double), cmpfunc);
    FILE *DATA_samples = fopen("res.dat", "w+");

    for (i = 0; i < n; i++)
       fprintf(DATA_samples, "%lf\n", (samples[i]));
    fclose(DATA_samples);


    //return 0;

    int rank, real_rank;
    rank = 40;
    double maxeps = 1e-6;
    double *U, *V;
    

    double current_time = 0.0;
    double current_shift = 0.0;
    int n_part = n;
    double *new_samples;

    printf("Init OK\n");

    int j;
    double mean_mass;
    double init_mass = 0.0;
    for (j = 0; j < n_part; j++)
        init_mass = init_mass + samples[j]; 
    int n_mc_steps = n / 2;

    mean_mass = 0.0;
    double cell_size = 1.0;
    for (i = 0; i < n_part; i++)
            mean_mass = mean_mass + samples[i]; 
    mean_mass = mean_mass / init_mass;
    int num_start = 1;
    double multip = 1.;
    char checkpoint_name[200];
    double source = 0.0;
    while ( (current_time < target_time))
    {
        while (n_part > n / 2)
        {
            new_samples = DSMC_fast_step(samples, &n_part, K1, &current_shift, &source);
            current_time = current_time + current_shift  * n * cell_size;
            if (current_shift < 0.0)
            {
                printf("FAILURE, negative time shift ... %E!!!\n", current_shift);
                exit(1);
            }
            free(samples); 
            samples = new_samples;
            
            mean_mass = 0.0;
            for (j = 0; j < n_part; j++)
                mean_mass = mean_mass + samples[j]; 
            //mean_mass = mean_mass;
            if ( current_time > info_time)
            {
                printf("%d %E %lf %lf\n", n_part, current_time, cell_size, ((double)n_part ) / tot_n);
                info_time = info_time + info_time_step;
                if (if_checkpoint)
                {
                    int stat;
                    stat = system("mkdir -p checkpoints");
                    sprintf(checkpoint_name, "checkpoints/res_time%.3lf.dat", current_time);
                    DATA_samples = fopen(checkpoint_name, "w+");
                    fprintf(DATA_samples, "%lf\n", mean_mass);
                    for (j = 0; j < n_part; j++)
                    {
                        fprintf(DATA_samples, "%lf\n", samples[i]);
                    }
                    fclose(DATA_samples);
                }
            }

            if ( (current_time) > target_time)
            {
                //printf("We Should Stop1\n");
                break;
            }
            //printf("MC step %d done, cur time %E\n", i+1, current_time);
        }
        // debug break, no cloning!
        //break;
        if ( (current_time) > target_time)
        {
            //printf("We Should Stop2\n");
            printf("step %d time = %E N = %lf\n", i+1, current_time, ((double)n_part ) / tot_n);
            printf("n_part = %d\n max_n = %d mass = %lf cell_size = %lf\n", n_part, n, mean_mass, cell_size);
            break;
        }
        //printf("CLONE ALL PARTICLES\n");// it means that we double the cell; hence tot numbers of particles virtually double
        new_samples = (double *) malloc (n * sizeof(double));// clone all particles
        for (i = 0; i < n_part; i++)// n_part = n / 2 after n_mc_steps
        {
            new_samples[2 * i] =  samples[i];
            new_samples[2 * i + 1] = samples[i];
        }
        free(samples);
        
        //mean_mass = mean_mass * 2.;
        num_start = num_start * 2; // 2^{restarts}
        samples = new_samples;        
        n_part = n;        
        tot_n = tot_n * 2.;
        cell_size = cell_size * 2.;
        current_shift = 0.0;
    }
    
    DATA_samples = fopen("fin.dat", "w+");
    for (i = 0; i < n_part; i++)
    {
       fprintf(DATA_samples, "%lf\n", (samples[i]));
    }
    fclose(DATA_samples);

    free(samples);

    return(0);
}
