#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "generate_samples.h"

double my_random(double left, double right)
{
    return left + ( (double)rand() / RAND_MAX) * (right - left);
}

double majorant(double x, double mu)
{// Gaussian majorant distribution
    double sigma = 0.01;
    double ssigma = sigma * sigma;
    return 1.0 / sqrt(2.0 * M_PI) / sigma * exp(-0.5 * (x - mu) * (x - mu) / ssigma);
}

double *create_samples(int n, double (*P)(double x), const double sigma_p, const double mu_p)
{// Metropolis-Hastings 
// n -- number of sampling points
// P -- target distribution
// sigma_p -- its assumed variance
// mu_p -- its assumed mean
   int i;
   double *samples;
   time_t t;
   // allocate memory
   samples = (double *) malloc(n * sizeof(double));

   //Intialize random number generator
   //srand((unsigned) time(&t));

   //srand(0);
   double x_t, candidate, a1, a2, a;
   
   x_t = my_random(0, 1);// init
   while (i < n)
   {
       candidate = x_t + (my_random(-2. * sigma_p, 2. * sigma_p));// uniform shift within 3*sigma_p
       a1 = P(candidate) / P(x_t); // target prob proportion
       a2 = majorant(x_t, candidate) / majorant(candidate, x_t); // majorant proportion; majorant is gaussian
       //printf("%lf \n", a2); // 1.0 in case of gaussian majorant btw due to its symmetry
       a  = a1 * a2; // check acceptance
       //printf("hello, %lf %lf %lf\n", a1, a2, a);
       double prob = my_random(0,1); // failure acceptance probability
       if (a >= 1 || a > prob ) // acceptance event
       {
           x_t = candidate;
           samples[i] = x_t;// point is accepted
           i = i + 1;
       }
   }
   //i = 0;
   //for (i = 0; i < n; i++)
   //    printf("%lf\n", samples[i]);
   return samples;
}
