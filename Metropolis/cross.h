//#include "mkl.h"
#include <cblas.h>
//#include <mkl.h>
//#include <mkl_lapacke.h>
#include <omp.h>

double* low_rank_matvec(double *U, double *V, int rank, int n, double* x, const char option);

int cross_mod(double (A(int i, int j, double *ddata)), int n, double *U, double *V, int decomposition_rank, double maxeps, double *data);

int cross(double (A(int i, int j)), int n, double *U, double *V, int decomposition_rank, double maxeps);


void reallocate_to_real_rank(double **U, double **V, int n, int rank, int real_rank);
