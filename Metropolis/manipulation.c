#include "manipulation.h"



int cmpfunc (const void * a, const void * b) 
{
   if ( *(double*)a > *(double*)b ) return 1;
   else if (*(double*)a < *(double*)b) return -1;
   else return 0;  
}

int max(int a, int b)
{
   if (a > b)
       return a;
   else
       return b;
}


void delete_and_insert(double *s, double *new_s, const int i, const int j, const int N)
{// generate new particle s[i] + s[j]
// save results in new ensemble in sorted manner
   double new_size = s[i] + s[j];
   int k, p;
   p = 1;
   for (k = 0; k < N; k++)
       if ( (k != i) && (k != j))
       {
           new_s[p] = s[k];
           p++;
       }

   new_s[0] = new_size;
   for (k = 0; k < N - 2; k++)
       if (new_s[k] > new_s[k+1])
       {
           new_size = new_s[k+1];
           new_s[k+1] = new_s[k];
           new_s[k] = new_size;
       }
}


void insert_particle(double *s, double *new_s, double particle, const int N)
{// generate new particle 
// save results in new ensemble in sorted manner
    int k, p;
    p = 0;
    for (k = 0; k < N; k++)
    {
       if (particle > s[p])
       {
           new_s[p] = s[k];
           p++;
       }
       else
       {
           new_s[p] = particle;
           p++;
       }
    }
}

void add_fragments(double *s, double *new_s, const int n1, const int n2, const int N)
{// delete particles n1 and n2 and add s[n1] + s[n2] unit fragments
// works only for discrete sizes
// double *new should contain N + number_of_fragments - 2 elements
    int number_of_fragments = (int)(s[n1] + s[n2]);
    int i, p, new_p;
    p = 0;
    i = 0;
    new_p = 0;
    for (i = 0; i < N + number_of_fragments; i++)
    {
        if (i < number_of_fragments)
        {
            new_s[new_p] = 1.0;
            new_p++;
        }
        else
        {
            if (p != n1 && p != n2)
            {
                new_s[new_p] = s[p];
                p++;
                new_p++;
            }
            else
                p++;
        }
    }
}
