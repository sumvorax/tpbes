# Makefile 

.SUFFIXES:
.SUFFIXES: .cpp .o

SHELL = /bin/bash

include Makefile.in

#==============================================================================#
#  Definitions 
#==============================================================================#
BIN_DIR = bin
BUILD_DIR = build
CONF_DIR = conf
SRC_DIR = src
TEST_DIR = test

UTILITY_DIR = $(SRC_DIR)/utility
TENSOR_DIR = $(SRC_DIR)/tensor
OPERATOR_DIR = $(SRC_DIR)/operator
SCHEME_DIR = $(SRC_DIR)/scheme
SOLVER_DIR = $(SRC_DIR)/solver

OPERATOR = $(wildcard $(OPERATOR_DIR)/*.cpp)
SCHEME = $(wildcard $(SCHEME_DIR)/*.cpp)

SOLVER = $(wildcard $(SOLVER_DIR)/*.cpp)
SRCS = $(filter-out $(SOLVER) $(OPERATOR) $(SCHEME), $(shell find $(SRC_DIR)/ -name '*.cpp'))
OBJS = $(patsubst %.cpp, build/%.o, $(notdir $(SRCS)))

LIBTT = $(BUILD_DIR)/libtt.a
 
#==============================================================================#
#   Default
#==============================================================================#
all: libtt

#==============================================================================#
#   Utility
#==============================================================================#
$(UTILITY_DIR)/definitions.h:
	touch $@

$(UTILITY_DIR)/blas.hpp:
	touch $@

$(UTILITY_DIR)/blas.h: $(UTILITY_DIR)/definitions.h $(UTILITY_DIR)/blas.hpp
	touch $@

# blas_complex.o, blas_double.o, blas_int.o
$(BUILD_DIR)/blas_%.o: $(UTILITY_DIR)/blas_%.cpp $(UTILITY_DIR)/blas.h \
	$(UTILITY_DIR)/memory.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

$(UTILITY_DIR)/memory.hpp: $(UTILITY_DIR)/blas.hpp
	touch $@

$(UTILITY_DIR)/memory.h: $(UTILITY_DIR)/memory.hpp
	touch $@

$(UTILITY_DIR)/utility.h:
	touch $@

# utility.o
$(BUILD_DIR)/utility.o: $(UTILITY_DIR)/utility.cpp $(UTILITY_DIR)/utility.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

#==============================================================================#
#   Tensor
#==============================================================================#
# tensor.o
$(TENSOR_DIR)/tensor.hpp: $(UTILITY_DIR)/blas.h $(UTILITY_DIR)/memory.h
	touch $@

$(TENSOR_DIR)/tensor.h: $(TENSOR_DIR)/tensor.hpp $(UTILITY_DIR)/definitions.h \
	$(UTILITY_DIR)/memory.h
	touch $@

$(BUILD_DIR)/tensor.o: $(TENSOR_DIR)/tensor.cpp $(TENSOR_DIR)/tensor.h \
	$(UTILITY_DIR)/definitions.h $(UTILITY_DIR)/memory.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

# cross.o
$(TENSOR_DIR)/cross.hpp:  $(UTILITY_DIR)/blas.h $(UTILITY_DIR)/memory.h
	touch $@

$(TENSOR_DIR)/cross.h: $(TENSOR_DIR)/cross.hpp
	touch $@

$(BUILD_DIR)/cross.o: $(TENSOR_DIR)/cross.cpp $(TENSOR_DIR)/cross.h \
	$(UTILITY_DIR)/memory.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

# tensor_train.o
$(TENSOR_DIR)/tensor_train.hpp: $(TENSOR_DIR)/cross.h $(TENSOR_DIR)/tensor.h \
	$(UTILITY_DIR)/blas.h $(UTILITY_DIR)/utility.h
	touch $@

$(TENSOR_DIR)/tensor_train.h: $(TENSOR_DIR)/tensor_train.hpp
	touch $@

$(BUILD_DIR)/tensor_train.o: $(TENSOR_DIR)/tensor_train.cpp \
	$(TENSOR_DIR)/tensor_train.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

#==============================================================================#
#   Operator
#==============================================================================#
# source.o
$(OPERATOR_DIR)/source.hpp: $(UTILITY_DIR)/blas.h
	touch $@

$(OPERATOR_DIR)/source.h: $(OPERATOR_DIR)/source.hpp
	touch $@

# unary_frag_operator.o
$(OPERATOR_DIR)/unary_frag_operator.hpp: $(TENSOR_DIR)/tensor.h
	touch $@

$(OPERATOR_DIR)/unary_frag_operator.h: $(OPERATOR_DIR)/unary_frag_operator.hpp \
	$(UTILITY_DIR)/definitions.h
	touch $@

$(BUILD_DIR)/unary_frag_operator.o: $(OPERATOR_DIR)/unary_frag_operator.cpp \
	$(OPERATOR_DIR)/unary_frag_operator.h $(UTILITY_DIR)/utility.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

# binary_frag_operator.o
$(OPERATOR_DIR)/binary_frag_operator.hpp: $(TENSOR_DIR)/tensor.h \
	$(TENSOR_DIR)/tensor_train.h $(UTILITY_DIR)/definitions.h
	touch $@

$(OPERATOR_DIR)/binary_frag_operator.h: $(OPERATOR_DIR)/binary_frag_operator.hpp
	touch $@

$(BUILD_DIR)/binary_frag_operator.o: $(OPERATOR_DIR)/binary_frag_operator.cpp \
	$(OPERATOR_DIR)/binary_frag_operator.h $(UTILITY_DIR)/blas.h \
	$(UTILITY_DIR)/utility.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

# aggr_operator.o
$(OPERATOR_DIR)/aggr_operator.hpp:
	touch $@

$(OPERATOR_DIR)/aggr_operator.h: $(OPERATOR_DIR)/aggr_operator.hpp \
	$(TENSOR_DIR)/tensor_train.h $(UTILITY_DIR)/definitions.h
	touch $@

$(BUILD_DIR)/aggr_operator.o: $(OPERATOR_DIR)/aggr_operator.cpp \
	$(OPERATOR_DIR)/aggr_operator.h $(TENSOR_DIR)/tensor.h \
	$(UTILITY_DIR)/blas.h $(UTILITY_DIR)/utility.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

#==============================================================================#
#   Scheme
#==============================================================================#
$(SCHEME_DIR)/scheme_parameters.h:
	touch $@

$(BUILD_DIR)/scheme_parameters.o: $(SCHEME_DIR)/scheme_parameters.cpp \
	$(SCHEME_DIR)/scheme_parameters.h $(UTILITY_DIR)/utility.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

$(SCHEME_DIR)/scheme.hpp: $(UTILITY_DIR)/blas.h $(UTILITY_DIR)/utility.h
	touch $@

$(SCHEME_DIR)/scheme.h: $(SCHEME_DIR)/scheme.hpp \
	$(SCHEME_DIR)/scheme_parameters.h $(OPERATOR_DIR)/aggr_operator.h \
	$(OPERATOR_DIR)/binary_frag_operator.h $(OPERATOR_DIR)/source.h \
	$(OPERATOR_DIR)/unary_frag_operator.h $(TENSOR_DIR)/tensor.h \
	$(UTILITY_DIR)/definitions.h
	touch $@

$(BUILD_DIR)/scheme_setters.o: $(SCHEME_DIR)/scheme_setters.cpp \
	$(SCHEME_DIR)/scheme.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

$(BUILD_DIR)/scheme_logging.o: $(SCHEME_DIR)/scheme_logging.cpp \
	$(SCHEME_DIR)/scheme.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

$(BUILD_DIR)/scheme_integration.o: $(SCHEME_DIR)/scheme_integration.cpp \
	$(SCHEME_DIR)/scheme.h
	$(CXX) $(CFLAGS) $(COPT) $< -o $@

#==============================================================================#
#	TPBES
#==============================================================================#
$(SRC_DIR)/tpbes.h: $(SCHEME_DIR)/scheme.h
	touch $@

#==============================================================================#
#	Libtt
#==============================================================================#
build:
	mkdir -p $(BUILD_DIR)

libtt: build $(OBJS)
	$(AR) rc $(LIBTT) $(OBJS)
	ranlib $(LIBTT)

#==============================================================================#
#	Test
#==============================================================================#
# test_cross.cpp
test_cross: libtt
	mkdir -p $(BIN_DIR)
	$(CXX) $(TEST_DIR)/test_cross.cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/test_cross.out

# test_tensor.cpp
test_tensor: libtt
	mkdir -p $(BIN_DIR)
	$(CXX) $(TEST_DIR)/test_tensor.cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/test_tensor.out

# test_tensor_train.cpp
test_tensor_train: libtt
	mkdir -p $(BIN_DIR)
	$(CXX) $(TEST_DIR)/test_tensor_train.cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/test_tensor_train.out

#==============================================================================#
#	Binary fragmentation test
#==============================================================================#
BINARY_FRAG_TEST = $(TEST_DIR)/binary_frag_test/binary_frag_test

$(BINARY_FRAG_TEST).h:
	touch $@

binary_frag_test: libtt $(BINARY_FRAG_TEST).h
	mkdir -p $(BIN_DIR)
	$(CXX) $(BINARY_FRAG_TEST).cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/binary_frag_test.out

#==============================================================================#
#	Integration test
#==============================================================================#
INTEGRATION_TEST = $(TEST_DIR)/integration_test/integration_test

$(INTEGRATION_TEST).h:
	touch $@

integration_test: libtt $(INTEGRATION_TEST).h
	mkdir -p $(BIN_DIR)
	$(CXX) $(INTEGRATION_TEST).cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/integration_test.out

#==============================================================================#
#	Solver
#==============================================================================#
$(CONF_DIR)/user_definitions.h:
	touch $@

solver: libtt $(CONF_DIR)/user_definitions.h
	mkdir -p $(BIN_DIR)
	$(CXX) $(SOLVER_DIR)/solver.cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/solver.out

#==============================================================================#
#	Brill Solver
#==============================================================================#
$(CONF_DIR)/brill_definitions.h:
	touch $@

brill_solver: libtt $(CONF_DIR)/brill_definitions.h
	mkdir -p $(BIN_DIR)
	$(CXX) $(SOLVER_DIR)/brill_solver.cpp $(LIBTT) $(LIBS) $(COPT) -o \
		$(BIN_DIR)/brill_solver.out

#==============================================================================#
#	Clean
#==============================================================================#
clean:
	rm -rf $(BUILD_DIR) $(BIN_DIR)

.PHONY: binary_frag_test brill_solver build clean integration_test libtt \
	solver test_cross test_tensor test_tensor_train

# Makefile 
