set terminal pdf lw 3
set output 'graphic.pdf'
set logscale y
set logscale x
set yrange [1e-12:1.0]
set key outside

m_list="0"
a_list="0.0 0.1"
lambda_list="5e-2"
cond="1e-2"

do for [a in a_list] {
   do for [lambda in lambda_list] {
       set title sprintf("N = 32768 a = %s, lambda = %s, cond = %s", a, lambda, cond)
       print a." ".lambda
       plot for [m in m_list] 'converg_'.m.'_'.a.'_'.lambda.'_'.cond.'.log' w l title ''.m
    }
}
