#!/bin/bash

program_name="anderson.out"
_m="0"
_a="0.0 0.1"
_lambda="5e-2"
_cond="1e-2"

# maximum number of iterations
max_iter=10000
eps=1e-12

for m in $_m
do
    for a in $_a
    do
	for lambda in $_lambda
	do
	    for cond in $_cond
	    do
		srun -N 1 -c 4 -J "m"$m"_a"$a"_"$lambda $program_name \
		    $max_iter $eps $m $a $lambda $cond \
		    "data_"$m"_"$a"_"$lambda"_"$cond > \
		    "converg_"$m"_"$a"_"$lambda"_"$cond".dat" &
	    done
        done
    done
done
