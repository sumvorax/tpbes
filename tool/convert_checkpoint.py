#!/bin/python
import sys
import struct
filename = sys.argv[1]
checkpoint = open(str(filename), "rb")
t = struct.unpack('d', checkpoint.read(8))[0]
N = struct.unpack('i', checkpoint.read(4))[0]
#print t, N
for i in xrange(N):
    element = struct.unpack('d', checkpoint.read(8))[0]
    print i+1, element
checkpoint.close()
