#ifndef DEFINITIONS_H 
#define DEFINITIONS_H

#include <complex>
#include <mkl_dfti.h>
#include <mkl_types.h>

namespace tpbes {

/*******************************************************************************
*
*   Type definitions
*
*******************************************************************************/

// Complex double type.
typedef std::complex<double> Complex;

// Allowed element function pointers.
typedef double (* VectorElementMapping)(const int);
typedef double (* MatrixElementMapping)(const int, const int);
typedef double (* TensorElementMapping)(const int, const int *);

typedef double (* VectorFunctionMapping)(const int, const double &);

} // namespace tpbes

#endif // DEFINITIONS_H
