#ifndef MEMORY_H
#define MEMORY_H

#include <cstdlib>
#include <iostream> // REMOVE

namespace tpbes {

/*******************************************************************************
*
*   Memory handling functions
*
*******************************************************************************/

template<typename Type>
Type * Allocate(const int, const Type &);

template<typename Type>
Type * Allocate(const int, const Type * = NULL);

template<typename Type>
void Reallocate(Type *&, const int, const Type &);

template<typename Type>
void Reallocate(Type *&, const int, const Type * = NULL);

template<typename Type>
void Free(Type *& arg) { if (arg) { free(arg); arg = NULL; } }
//void Free(Type *& arg) { if (arg) { std::cout << "START: FREE" << std::endl; free(arg); arg = NULL; std::cout << "FINISH: FREE" << std::endl;} }

/// template<typename Type>
/// void Free_elements(Type **, const unsigned);

} // namespace tpbes

#include "memory.hpp"

#endif // MEMORY_H
