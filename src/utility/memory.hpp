#ifndef MEMORY_HPP
#define MEMORY_HPP

#include <cstdlib>
#include <iostream> // REMOVE

#include "blas.hpp"

namespace tpbes {

template<typename Type>
Type * Allocate(const int size, const Type & arg)
{
    /// std::cout << "SINGLE" << std::endl;
    Type * res = NULL;

    if (size > 0)
    {
        /// std::cout << "<<<SIZE = " << size << std::endl;
        res = (Type *)realloc(res, size * sizeof(Type));
        if (res) { Copy<Type>(size, &arg, 0, res, 1); }
    }

    return res;
}

template<typename Type>
Type * Allocate(const int size, const Type * arg)
{
    /// std::cout << "MULTIPLE" << std::endl;
    Type * res = NULL;

    if (size > 0)
    {
       ///  std::cout << "<<<SIZE = " << size << std::endl;
        res = (Type *)realloc(res, size * sizeof(Type));
        /// std::cout << "<<<SIZE = " << size * sizeof(Type) << std::endl;
        if (arg && res) { Copy<Type>(size, arg, 1, res, 1); }
    }

    return res;
}

template<typename Type>
void Free(Type *& arg);

template<typename Type>
void Reallocate(Type *& res, const int size, const Type & arg)
{
    //std::cout << "SINGLE" << std::endl;
    if (size > 0)
    {
        res = (Type *)realloc(res, size * sizeof(Type));
        if (res) { Copy<Type>(size, &arg, 0, res, 1); }
    }
    else { Free<Type>(res); }
}

template<typename Type>
void Reallocate(Type *& res, const int size, const Type * arg)
{
    //std::cout << "MULTIPLE" << std::endl;
    if (size > 0)
    {
        res = (Type *)realloc(res, size * sizeof(Type));
        if (arg && res) { Copy<Type>(size, arg, 1, res, 1); }
    }
    else { Free<Type>(res); }
}

/// template<typename Type>
/// void Free_elements(Type ** arr, const unsigned size)
/// {
///     for (unsigned s = 0; s < size; ++s) { Free<Type>(arr[s]); }
/// }

} // namespace tpbes

#endif // MEMORY_HPP
