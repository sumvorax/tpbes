// src/utility/utility.cpp

#include "utility.h"

#include <mkl.h>
#include <omp.h>

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>

namespace tpbes {

//==============================================================================
//  Check_DFTI_status
//==============================================================================
void Check_DFTI_status(const MKL_LONG & status)
{
    if (status && !DftiErrorClass(status, DFTI_NO_ERROR))
    {
        std::cerr << "Error: MKL DFTI: " <<  DftiErrorMessage(status)
            << std::endl;

        exit(EXIT_FAILURE);
    }

    return;
}

//==============================================================================
//  Check_config_presence
//==============================================================================
void Check_config_presence(const int argc)
{
    if (argc < 2)
    {
        std::cerr << "Error: Configuration filename is not specified"
            << std::endl;

        exit(EXIT_FAILURE);
    }
}

//==============================================================================
//  Print_array
//==============================================================================
void Print_array(const int size, const double * cons, const int inc)
{
    for (int s = 0; s < size; ++s)
    {
        std::cout << s << " "<< std::scientific << std::setprecision(15)
            << std::fixed << cons[s * inc] << std::endl;
    } 
}

//==============================================================================
//  Zeroize_negative
//==============================================================================
void Zeroize_negative(const int size, double * cons)
{
#pragma omp parallel for
    for (int s = 0; s < size; ++s) { if (cons[s] < 0.) { cons[s] = 0.; } }
}

//==============================================================================
//  Save_array
//==============================================================================
void Save_array(
    const std::string & filename, const int size, const double * cons
)
{
    std::ofstream file(filename);

    if (file.is_open())
    {
        for (int s = 0; s < size; ++s)
        {
            file << s << " " << std::setprecision(15) << std::fixed << cons[s]
                << std::endl;
        }
    }
    else
    {
        std::cerr << "Error: Can not open file to save result concentrations"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    file.close();
}

//==============================================================================
//  Save_time
//==============================================================================
void Save_time(const std::string & filename, const double & time)
{
    std::ofstream file(filename);

    if (file.is_open())
    {
        file << std::setprecision(2) << std::scientific << std::fixed << time
            << std::endl;
    }
    else
    {
        std::cerr << "Error: Can not open file to save calculations time"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    file.close();
}

} // namespace tpbes

// src/utility/utility.cpp
