#ifndef BLAS_H
#define BLAS_H

#include <mkl.h>
#include <mkl_lapacke.h>

#include "definitions.h"

namespace tpbes {

/*******************************************************************************
*
*   Basic linear algebra functions (BLAS)
*
*******************************************************************************/

//==============================================================================
//  Generic arithmetics
//==============================================================================
template<typename Type>
void Copy(const int, const Type *, const int, Type *, const int);

//==============================================================================
//  Integer arithmetics
//==============================================================================
template<>
void Copy<int>(const int, const int *, const int, int *, const int);

double Dot(const int, const int *, const int, const int *, const int);

int Iamax(const int, const int *, const int);

//==============================================================================
//  Double-precision arithmetics
//==============================================================================
void Axpy(
    const int,
    const double &,
    const double *,
    const int,
          double *,
    const int
);

template<>
void Copy<double>(const int, const double *, const int, double *, const int);

double Dot(
    const int, const double *, const int, const double *, const int
);

int Iamax(const int, const double *, const int);

void Gemm(
    const CBLAS_ORDER,
    const CBLAS_TRANSPOSE,
    const CBLAS_TRANSPOSE,
    const int,
    const int,
    const int,
    const double &,
    const double *,
    const int,
    const double *,
    const int,
    const double &,
    double *,
    const int
);

void Gemv(
    const CBLAS_ORDER,
    const CBLAS_TRANSPOSE,
    const int,
    const int,
    const double &,
    const double *,
    const int,
    const double *,
    const int,
    const double &,
    double *,
    const int
);

int Gesv(const int, const int, const int, double *, int, int *, double *, int);

int Gesvd(
    const int,
    const char,
    const char,
    const int,
    const int,
    double *,
    const int,
    double *,
    double *,
    const int,
    double *,
    const int,
    double *
);

void Ger(
    const CBLAS_ORDER,
    const int,
    const int,
    const double &,
    const double *,
    const int,
    const double *,
    const int,
    double *,
    const int
);

// Computes maximal volume for a given submatrix.
void Maxvol(
    const int, const int, const double *, int *, const double &, double *
);

// Computes a moment of an arithmetic vector.
double Moment(const double &, const int, const double *);

double Norm(const int, const double *, const int);

// Computes pseudo-inverse matrix for a given submatrix.
double * Pseudoinverse(const double &, const int, const int, double *);

void Scal(const int, const double &, double *, const int);

//==============================================================================
//  Complex arithmetics
//==============================================================================
void Gemv(
    const CBLAS_ORDER,
    const CBLAS_TRANSPOSE,
    const int,
    const int,
    const Complex &,
    const Complex *,
    const int,
    const Complex *,
    const int,
    const Complex &,
    Complex *,
    const int
);

} // namespace tpbes

#include "blas.hpp"

#endif // BLAS_H
