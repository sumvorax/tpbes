#ifndef BLAS_HPP
#define BLAS_HPP

#include <iostream>

namespace tpbes {

template<typename Type>
void Copy(
    const int    size,
    const Type * arg,
    const int    inc_arg,
          Type * res,
    const int    inc_res
)
{
    // std::cout << "AAAAAAA" << std::endl;
    if (size > 0 && arg && res)
    {
        for (int s = 0; s < size; ++s) { res[s * inc_res] = arg[s * inc_arg]; }
    }
}

} // namespace tpbes

#endif // BLAS_HPP
