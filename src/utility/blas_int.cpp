// src/utility/blas_int.cpp

#include "blas.h"

#include <omp.h>

#include <iostream>

namespace tpbes {

template<>
void Copy<int>(
    const int   size,
    const int * arg,
    const int   arg_inc,
          int * res,
    const int   res_inc
)
{
    if (size > 0 && arg)
    {
        const int * ar = (arg_inc >= 0)? arg: arg + size - 1;

#pragma omp parallel for if (size > 20) // Magic constant
        for (int s = 0; s < size; ++s)
        {
            res[s * res_inc] = ar[s * arg_inc];
        }
    }
}

double Dot(
    const int   size,
    const int * left,
    const int   left_inc,
    const int * right,
    const int   right_inc
) {
    int res = 0;

#pragma omp parallel for reduction(+: res)
    for (int i = 0; i < size; ++i)
    {
        res += left[i * left_inc] * right[i * right_inc];
    }

    return res;
}

int Iamax(const int size, const int * left, const int left_inc)
{
    int res = left_inc? 0: size - 1;

    if (left_inc)
    {
        for (int i = left_inc; i < size; i += left_inc)
        {
            if (left[res] < left[i]) { res = i; }
        }
    }
    else
    {
        for (int i = size - 1 + left_inc; i >= 0; i += left_inc)
        {
            if (left[res] < left[i]) { res = i; }
        }
    }

    return res;
}

} // namespace tpbes

// src/utility/blas_int.cpp
