#ifndef UTILITY_H
#define UTILITY_H

#include <mkl.h>

// #include <cstdlib>
#include <string>

namespace tpbes {

/*******************************************************************************
*
*   Utility functions
*
*******************************************************************************/

//==============================================================================
//  Checking routines
//==============================================================================
/// TODO /// Move to aggregation operator
// Checks DFTI status.
void Check_DFTI_status(const MKL_LONG &);

/// TODO /// Move to wherever the new config will be
// Checks config presence.
void Check_config_presence(const int);

//==============================================================================
//  Array handling functions
//==============================================================================
/// TODO /// Move to operator folder
// Prints array (for debug purposes).
void Print_array(const int, const double *, const int = 1);

/// TODO /// Move to operator folder
// Zeroizes negative elements in the array.
void Zeroize_negative(const int, double *);

/// TODO /// Move to scheme
// Saves array to a specified file.
void Save_array(const std::string &, const int, const double *);
/// TODO /// Move to scheme
// Saves computation time to a specified file.
void Save_time(const std::string &, const double &);

} // namespace tpbes

#endif // UTILITY_H
