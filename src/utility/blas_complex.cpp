// src/utility/blas_complex.cpp

#include "blas.h"

namespace tpbes {

void Gemv(
    const CBLAS_ORDER     order,
    const CBLAS_TRANSPOSE mat_trans,
    const int             left_size,
    const int             right_size,
    const Complex &       factor,
    const Complex *       mat,
    const int             mat_ld,
    const Complex *       vec,
    const int             vec_inc,
    const Complex &       upd_factor,
          Complex *       upd,
    const int             upd_inc
)
{
    cblas_zgemv(
        order, mat_trans, left_size, right_size, &factor, mat, mat_ld, vec,
        vec_inc, &upd_factor, upd, upd_inc
    );
}

} // namespace tpbes

// src/utility/blas_complex.cpp
