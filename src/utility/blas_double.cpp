// src/utility/blas_double.cpp

#include "blas.h"

#include <omp.h>

#include "memory.h"

namespace tpbes {

void Axpy(
    const int      size,
    const double & factor,
    const double * arg,
    const int      arg_inc,
          double * upd,
    const int      upd_inc
)
{
    if (size > 0 && arg)
    {
        cblas_daxpy(size, factor, arg, arg_inc, upd, upd_inc);
    }
}

template<>
void Copy<double>(
    const int      size,
    const double * arg,
    const int      arg_inc,
          double * res,
    const int      res_inc
)
{
    if (size > 0 && arg) { cblas_dcopy(size, arg, arg_inc, res, res_inc); }
}

double Dot(
    const int      size,
    const double * left,
    const int      left_inc,
    const double * right,
    const int      right_inc
)
{
    return cblas_ddot(size, left, left_inc, right, right_inc);
}

void Gemm(
    const CBLAS_ORDER     order,
    const CBLAS_TRANSPOSE left_trans,
    const CBLAS_TRANSPOSE right_trans,
    const int             left_size,
    const int             right_size,
    const int             med_size,
    const double &        factor,
    const double *        left,
    const int             left_ld,
    const double *        right,
    const int             right_ld,
    const double &        upd_factor,
          double *        upd,
    const int             upd_ld
)
{
    cblas_dgemm(
        order, left_trans, right_trans, left_size, right_size, med_size, factor,
        left, left_ld, right, right_ld, upd_factor, upd, upd_ld
    );
}

void Gemv(
    const CBLAS_ORDER     order,
    const CBLAS_TRANSPOSE mat_trans,
    const int             left_size,
    const int             right_size,
    const double &        factor,
    const double *        mat,
    const int             mat_ld,
    const double *        vec,
    const int             vec_inc,
    const double &        upd_factor,
          double *        upd,
    const int             upd_inc
)
{
    cblas_dgemv(
        order, mat_trans, left_size, right_size, factor, mat, mat_ld, vec,
        vec_inc, upd_factor, upd, upd_inc
    );
}

int Gesv(
    const int      order,
    const int      mat_size,
    const int      rhs_size,
          double * mat,
    const int      mat_ld,
          int    * ipiv,
          double * rhs,
    const int      rhs_ld
)
{
    return
        LAPACKE_dgesv(
            order, mat_size, rhs_size, mat, mat_ld, ipiv, rhs, rhs_ld
        );
}

int Gesvd(
    const int      order,
    const char     U_job,
    const char     VT_job,
    const int      left_size,
    const int      right_size,
          double * mat,
    const int      mat_ld,
          double * S,
          double * U,
    const int      U_ld,
          double * VT,
    const int      VT_ld,
          double * superb
)
{
    return
        LAPACKE_dgesvd(
            order, U_job, VT_job, left_size, right_size, mat, mat_ld, S, U,
            U_ld, VT, VT_ld, superb
        );
}

void Ger(
    const CBLAS_ORDER order,
    const int         left_count,
    const int         right_count,
    const double &    factor,
    const double *    left,
    const int         left_inc,
    const double *    right,
    const int         right_inc,
          double *    upd,
    const int         upd_ld
)
{
    cblas_dger(
        order, left_count, right_count, factor, left, left_inc, right,
        right_inc, upd, upd_ld
    );
}

int Iamax(const int size, const double * arg, const int arg_inc)
{
    return cblas_idamax(size, arg, arg_inc);
}

void Maxvol(
    const int      size,
    const int      rank,
    const double * factor,
          int    * start_index,
    const double & eps,
          double * coef
)
{
	double * matrix = Allocate<double>(std::max(rank * rank, size + rank));

	for (int r = 0; r < rank; ++r)
    {
        Copy<double>(size, factor + r * size, 1, coef + r, rank);
		Copy<double>(rank, factor + start_index[r], size, matrix + r * rank, 1);
	}

	int * ipiv = Allocate<int>(rank);

	Gesv(LAPACK_COL_MAJOR, rank, size, matrix, rank, ipiv, coef, rank);

	Free<int>(ipiv);

    double * row = matrix;
    double * col = row + size;

	double alpha = 1.;
    double maxf = eps + 2.;

	int i;
    int j;

	while (maxf > eps + 1.)
	{
		i    =  Iamax(size * rank, coef, 1);
		maxf =  coef[i];
		j    =  i / rank;
		i    -= j * rank;

		if (std::fabs(maxf) > eps + 1.)
		{
			Copy<double>(size, coef + i, rank, row, 1);
			Copy<double>(rank, coef + j * rank, 1, col, 1);

			col[i]         -= 1.;
			start_index[i] =  j;
			alpha          = -1. / maxf;

			Ger(CblasColMajor, rank, size, alpha, col, 1, row, 1, coef, rank);
		}
	}

	Free<double>(matrix);
}

double Moment(const double & order, const int size, const double * arg)
{
    double res = 0.;

#pragma omp parallel for reduction(+: res)
    for (int s = 0; s < size; ++s) { res += std::pow(s + 1., order) * arg[s]; }

    return res;
}

double Norm(const int size, const double * arg, const int arg_inc)
{
    return cblas_dnrm2(size, arg, arg_inc);
}

double * Pseudoinverse(
    const double & tol, const int row_size, const int col_size, double * mat
)
{
    const int min_size = std::min(row_size, col_size);
    if (!min_size) { return NULL; }

    double * inv_mat = Allocate<double>(col_size * row_size);
    double * U       = Allocate<double>((row_size + col_size + 1) * min_size);
    double * VT      = U + row_size * min_size;
    double * sigma   = VT + min_size * col_size;
    double * superb  = inv_mat; // no need to save superb after execution

    Gesvd(
        LAPACK_COL_MAJOR, 'S', 'S', row_size, col_size, mat, row_size, sigma, U,
        row_size, VT, min_size, superb
    );

    const double s = Dot(min_size, sigma, 1, sigma, 1);

    int    k = 0;
    double r = 0.;

    for (int i = min_size; i; --i)
    {
        r += sigma[i - 1] * sigma[i - 1];

        if (s * tol * tol <= r * (1. + tol * tol)) { k = i; break; }
    }

#pragma omp parallel for
    for (int i = 0; i < k; ++i)
    {
        Scal(col_size, 1. / sigma[i], VT + i, min_size);
    }

    Gemm(
        CblasColMajor, CblasTrans, CblasTrans, col_size, row_size, k, 1., VT,
        min_size, U, row_size, 0., inv_mat, col_size
    );

    Free<double>(U);

    return inv_mat;
}

void Scal(
    const int size, const double & factor, double * upd, const int upd_inc
)
{
    if (size > 0) { cblas_dscal(size, factor, upd, upd_inc); }
}

} // namespace tpbes

// src/utility/blas_double.cpp
