// src/solver/solver.cpp

#include "../../conf/user_definitions.h"
#include "../scheme/scheme.h"

//============================================================================//
//  Unified solver
//============================================================================//
int main(int argc, char ** argv)
{
    //========================================================================//
    //  Read configuration file
    //========================================================================//
    // check if configuration filename is specified
    tpbes::Check_config_presence(argc);

    tpbes::SchemeParameters params;

    // read configuration parameters
    tpbes::Read_config(argv[1], params);

    //========================================================================//
    //  Initialize scheme with read parameters and user-defined functions
    //========================================================================//
    // initialize integration scheme, disable warnings
    tpbes::Scheme scheme(params, false);

    // set initial condition
    scheme.Set_initial_condition_data(&Initial_condition);
    scheme.Set_initial_temperature(Initial_temperature());

    // set source
    scheme.Set_source_data(&Source);
    scheme.Set_thermal_source_data(&Thermal_source);

    // set aggregation term
    scheme.Set_aggr_data(&Aggr_coefficient);

    // set thermal dependence
    scheme.Set_kinetic_factor(&Kinetic_factor);
    scheme.Set_thermal_factor(&Thermal_factor);

    // set unary fragmentation term
    scheme.Set_unary_frag_data(&Unary_frag_coefficient);
    scheme.Set_unary_frag_distr(&Unary_frag_mass_distribution);

    // set binary fragmentation term
    scheme.Set_binary_frag_data(&Binary_frag_coefficient);
    scheme.Set_binary_frag_distr(&Binary_frag_mass_distribution);

    //========================================================================//
    //  Integration process
    //========================================================================//
    const int size = params.mode + params.thermal_flag;
    double * init_cons = (double *)malloc(size * sizeof(double));
    double * res_cons = (double *)malloc(size * sizeof(double));

    // initialize concentrations
    scheme.Initial_conditions(init_cons);

    // integration
    scheme.Integrate(init_cons, res_cons);

    if (init_cons) { free(init_cons); }
    if (res_cons) { free(res_cons); }

    return 0;
}

// src/solver/solver.cpp
