// src/solver/brill_solver.cpp

#include "../../conf/brill_definitions.h"
#include "../scheme/scheme.h"

//============================================================================//
//  Unified solver
//============================================================================//
int main(int argc, char ** argv)
{
    //========================================================================//
    //  Read config
    //========================================================================//
    // check if configuration filename is specified
    tpbes::Check_config_presence(argc);

    tpbes::SchemeParameters params;

    // read configuration parameters
    tpbes::Read_config(argv[1], params);

    //========================================================================//
    //  Initialize scheme
    //========================================================================//
    // initialize integration scheme
    tpbes::Scheme scheme(params);

    // set initial condition
    scheme.Set_initial_condition_data(&Initial_condition);
    scheme.Set_initial_temperature(Initial_temperature());

    // set source
    scheme.Set_source_data(&Source);
    scheme.Set_thermal_source_data(&Thermal_source);

    // brill parameters
    double brill_params[2] = { 2.5, 1. };

    // brill factor
    double brill_factor = Factor(brill_params[0], brill_params[1]);

    // this is not to be changed
    void * aggr_params[2] = { NULL, (void *)&brill_factor };

    // set aggregation term
    scheme.Set_aggr_data(&Aggr_coefficient, aggr_params);

    // set thermal dependence
    scheme.Set_kinetic_factor(&Kinetic_factor);
    scheme.Set_thermal_factor(&Thermal_factor);

    //========================================================================//
    //  Integration process
    //========================================================================//
    const int size = params.mode + params.thermal_flag;
    double * init_cons = (double *)malloc(size * sizeof(double));
    double * res_cons = (double *)malloc(size * sizeof(double));

    // initialize concentrations
    scheme.Initial_conditions(init_cons);

    // integration
    scheme.Integrate(init_cons, res_cons);

    if (init_cons) { free(init_cons); }
    if (res_cons) { free(res_cons); }

    return 0;
}

// src/solver/brill_solver.cpp
