#ifndef BINARY_FRAG_OPERATOR_HPP
#define BINARY_FRAG_OPERATOR_HPP

#include <iostream>
#include <iomanip>

#include "../tensor/tensor.h"
#include "../tensor/tensor_train.h"
#include "../utility/definitions.h"

namespace tpbes {

/*******************************************************************************
*
*   BinaryFragOperator
*
*******************************************************************************/

//==============================================================================
//  Set income kernel
//==============================================================================
// element of two tensors product
template<typename DataTensor, typename DistrTensor>
static double Product_element(const int, const int * left_ind, void * pars)
{
    const int right_ind[2] = { left_ind[0], left_ind[2] };

    return
        ((DataTensor *)(((void **)pars)[0]))[left_ind]
        * ((DistrTensor *)(((void **)pars)[1]))[right_ind];
}

template<typename Tensor>
void Set_outcome_kernel(Tensor & ten)
{
    double approx_time = omp_get_wtime();

    out_ker.Approximate<Tensor>(data, approx_params);
    out_ker.Compress(approx_params.comp_tolerance);

    approx_time = omp_get_wtime() - approx_time;

    std::cout << "Binary fragmentation kernel approximation:\n" << "    "
        << "Mode = " << mode << ", Dim = 2, Time = " << std::setprecision(2)
        << std::scientific << std::fixed << approx_time << " (s)"
        << std::endl;
}

template<typename DataTensor>
void Set_income_kernel(DataTensor & data, DistrTensor & distr)
{
    double approx_time = omp_get_wtime();

    out_ker.Approximate(data, approx_params);
    out_ker.Compress(approx_params.comp_tolerance);

    approx_time = omp_get_wtime() - approx_time;

    std::cout << "Binary fragmentation kernel approximation:\n" << "    "
        << "Mode = " << mode << ", Dim = 2, Time = " << std::setprecision(2)
        << std::scientific << std::fixed << approx_time << " (s)"
        << std::endl;
}

template<typename DataTensor, typename DistrTensor>
void BinaryFragOperator<DataTensor, DistrTensor>::Set_data(
    DataTensor & data, DistrTensor & distr
)
{
    if (data.Get_dimension() != 2)
    {
        std::cerr << "Error: " << "A tensor of the requested dimension \""
            << data.Get_dimension() << "\"\n" << "    "
            << "can not be set as a binary fragmentation kernel" << std::endl;

        exit(EXIT_FAILURE);
    }

    if (distr.Get_dimension() != 2)
    {
        std::cerr << "Error: " << "A tensor of the requested dimension \""
            << distr.Get_dimension() << "\"\n" << "    " << "can not be set "
            << "as a binary fragmentation particle distribution kernel"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    //==========================================================================
    //  TensorProduct
    //==========================================================================
    // Represents the general tensor product of two tensors.
    template<typename LeftTensor, typename RightTensor>
    class TensorProduct
    {
        private:

            // Dimensionality.
            int dimension;

            // Mode sizes along each dimension.
            int * modes;

            // The argument.
            LeftTensor * left;
            RightTensor * right;

            int * left_indices;
            int * right_indices;

        public:

            TensorProduct(LeftTensor &, RightTensor &, int *, int *);
            ~TensorProduct() { if (modes) { free(modes); } }

            inline int Get_dimension() const { return dimension; }
            int Get_mode(const int) const;
            inline const int * Export_modes() const { return modes; }

            // Checks whether the wrapped function depends on a continuous argument.
            inline const bool Is_empty() { left != NULL && right != NULL; }
            const bool Is_continuous() const;

            // Computes tensor element for a given multiindex (and argument).
            double operator()(const int *, const double & = 0.) { return 0.; }

            // Computes tensor element for a given lexicographical index
            // (and argument).
            double operator()(const int, const double & = 0.) { return 0.; }
    };

    void * pars[2] = { (void *)data, (void *)distr };

    WrapTensor<element::GenIntParam> ten(3, mode);
    ten.Set_element(
        GenIntParamElement(Product_element<DataTensor, DistrTensor>), pars
    );

    double approx_time = omp_get_wtime();

    in_ker.Approximate< WrapTensor<element::GenIntParam> >(ten, approx_params);
    in_ker.Compress(approx_params.comp_tolerance);

    approx_time = omp_get_wtime() - approx_time;

    std::cout << "Binary fragmentation data-distribution product kernel "
        << "approximation:\n" << "    " << "Mode = " << mode
        << ", Dim = 3, Time = " << std::setprecision(2) << std::scientific
        << std::fixed << approx_time << " (s)" << std::endl;

    workspace
        = (double *)realloc(
            workspace,
            (
                mode * (out_ker.Get_rank(1) + in_ker.Get_rank(2))
                + std::max(
                    out_ker.Get_rank(1), in_ker.Get_rank(1) * in_ker.Get_rank(2)
                )
            ) * sizeof(double)
        );
}

} // namespace tpbes

#endif // BINARY_FRAG_OPERATOR_HPP
