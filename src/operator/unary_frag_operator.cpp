// src/operator/unary_frag_operator.cpp

#include "unary_frag_operator.h"

#include <cstdlib>

#include "../utility/utility.h"

namespace tpbes {

/*******************************************************************************
*
*   UnaryFragOperator
*
*******************************************************************************/

//==============================================================================
//  Constructor
//==============================================================================
UnaryFragOperator::UnaryFragOperator(const int mod, const int act_mod):
    mode(mod), actual_mode(act_mod? act_mod: mod), data(NULL), distr(NULL)
{}

//==============================================================================
//  Destructor
//==============================================================================
UnaryFragOperator::~UnaryFragOperator()
{
    if (data) { free(data); data = NULL; }
    if (distr) { free(distr); distr = NULL; }
}

//==============================================================================
//  Set_actual_mode
//==============================================================================
void UnaryFragOperator::Set_actual_mode(const int act_mod)
{
    actual_mode = std::min(act_mod, mode);
}

//==============================================================================
//  Action
//==============================================================================
// res += UnaryFragOperator(arg)
void UnaryFragOperator::operator()(const double * arg, double * res)
{
    if (data && distr)
    {
        double income = 0.;
        double outcome = 0.;

        for (int m = actual_mode - 1; m >= 0; --m)
        {
            outcome = data[m] * arg[m];
            res[m] += income - outcome;
            income += outcome * distr[m];
        }
    }
}

} // namespace tpbes

// src/operator/unary_frag_operator.cpp
