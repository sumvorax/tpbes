#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <cstdlib>
#include <iostream>

#include "../tensor/tensor.h"
#include "../utility/blas.h"

namespace tpbes {

/*******************************************************************************
*
*   Source
*
*******************************************************************************/

template<typename Tensor>
static double Dot(
    double * arr, Tensor & ten, const double & arg = 0., const int len = -1
)
{
    if (!len) { return; }

    int size = Get_size<Tensor>(ten);

    if (len > 0) { size = std::min(size, len); }

    double dot = 0.;

#pragma omp parallel for reduction(+: dot)
    for (int s = 0; s < size; ++s) { dot += arr[s] * ten(s, arg); }

    return dot;
}

//==============================================================================
//  Set_thermal_data
//==============================================================================
template<typename KineticTensor, typename ThermalTensor>
void Source<KineticTensor, ThermalTensor>::Set_thermal_data()
{
    data[mode] = Dot<ThermalTensor>(*thermal_element, data);
}

//==============================================================================
//  Constructor
//==============================================================================
template<typename KineticTensor, typename ThermalTensor>
Source<KineticTensor, ThermalTensor>::Source(
    const int mod, const int act_mod, const bool therm_flag
):
    thermal_flag(therm_flag),
    thermal_element(NULL),
    mode(mod),
    actual_mode(act_mod? act_mod: mod),
    kinetic_element(NULL),
    data(NULL),
    data_moment(0.)
{}

//==============================================================================
//  Destructor
//==============================================================================
template<typename KineticTensor, typename ThermalTensor>
Source<KineticTensor, ThermalTensor>::~Source()
{
    if (data) { free(data); data = NULL; }

    thermal_flag = false;
    thermal_element = NULL;
    mode = 0;
    actual_mode = 0;
    kinetic_element = NULL;
    data_moment = 0.;
}

//==============================================================================
//  Set_actual_mode
//==============================================================================
template<typename KineticTensor, typename ThermalTensor>
void Source<KineticTensor, ThermalTensor>::Set_actual_mode(const int act_mod)
{
    actual_mode = std::min(act_mod, mode);
}

//==============================================================================
//  Set_kinetic_element
//==============================================================================
template<typename KineticTensor, typename ThermalTensor>
void Source<KineticTensor, ThermalTensor>::Set_kinetic_element(
    KineticTensor & ten
)
{
    if (ten.Get_dimension() != 1) 
    {
        std::cerr << "Error: A tensor of the requested dimension \""
            << ten.Get_dimension() << "\"" << "    "
            << "can not be set as a source vector" << std::endl;

        exit(EXIT_FAILURE);
    }

    kinetic_element = &ten;

    if (!ten.Is_continuous())
    {
        data = (double *)realloc(data, (mode + thermal_flag) * sizeof(double));

        Vectorize<KineticTensor>(ten, data);

        data_moment = Moment(0, mode, data);

        // Preset thermal data if a thermal kinetic_element is set
        // and is not time dependent.
        if (thermal_element && !thermal_element->Is_continuous())
        {
            Set_thermal_data();
        }
    }
}

//==============================================================================
//  Set_thermal_element
//==============================================================================
template<typename KineticTensor, typename ThermalTensor>
void Source<KineticTensor, ThermalTensor>::Set_thermal_element(
    ThermalTensor & ten
)
{
    if (ten.Get_dimension() != 1) 
    {
        std::cerr << "Error: A tensor of the requested dimension \""
            << ten.Get_dimension() << "\"" << "    "
            << "can not be used for a thermal source" << std::endl;

        exit(EXIT_FAILURE);
    }

    thermal_element = &ten;

    // Preset thermal data if a kinetic element is set
    // and is not time dependent.
    if (data) { Set_thermal_data(); }
}

//==============================================================================
//  Action
//==============================================================================
// res = Source(time, temperature)
template<typename KineticTensor, typename ThermalTensor>
void Source<KineticTensor, ThermalTensor>::operator()(
    const double & time, const double & temp, double * res
)
{
    if (kinetic_element->Is_continuous())
    {
        Vectorize<KineticTensor>(*kinetic_element, res, time, actual_mode);
    }
    else { copy(actual_mode, data, 1, res, 1); }

    // Set temperature in case of thermal dependence.
    if (thermal_flag)
    {
        res[mode] = 
            (
                (thermal_element->Is_continuous())?
                    Dot<ThermalTensor>(
                        data, *thermal_element, time, actual_mode
                    ):
                    data[mode]
            )
            - temp * (
                (kinetic_element->Is_continuous())?
                    Moment(0, actual_mode, res):
                    data_moment
            );
    }
}
} // namespace tpbes

#endif // SOURCE_HPP
