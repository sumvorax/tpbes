#ifndef AGGR_OPERATOR_HPP
#define AGGR_OPERATOR_HPP

#include <omp.h>
#include <iostream>
#include <iomanip>

#include "../blas/blas.h"
#include "../tensor/tensor.h"
#include "../utility/utility.h"

namespace tpbes {

/*******************************************************************************

    AggrOperator -- Tensor Train based aggregation operator

*******************************************************************************/

//============================================================================//
//  Initialize data layout
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_layout(const int cnt)
{
    count = cnt;

    dimensions = (int *)realloc(dimensions, count * sizeof(int));
    ranks = (int **)realloc(ranks, count * sizeof(int *));
    factor_cols = (int **)realloc(factor_cols, count * sizeof(int *));
}

//============================================================================//
//  Initialize workspace
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_workspace()
{
    // set fourier parameters
    fourier_count = omp_get_max_threads();

    // allocate data and mode bounds
    row_bounds = (int *)malloc((fourier_count + 1) * sizeof(int));
    col_bounds = (int *)malloc((fourier_count + 1) * sizeof(int));

    col_bounds[fourier_count] = col_size;

    // allocate workspace 
    workspace_size = col_size + 2 * (max_rank + (fourier_count + 1) * mode);
    workspace = (double *)malloc((workspace_size + mode) * sizeof(double));

    // allocate fourier image workspace
    fourier_workspace
        = (Complex *)mkl_malloc(
            ((col_size + 1) * (mode + 1) + 2 * fourier_count * max_rank)
                * sizeof(Complex),
            64 // MAGIC (see Intel MKL documentation)
        );

    fourier_forwards
        = (DFTI_DESCRIPTOR_HANDLE *)malloc(
            fourier_count * sizeof(DFTI_DESCRIPTOR_HANDLE)
        );

#pragma omp parallel for
    for (int f = 0; f < fourier_count; ++f)
    {
        // set data bounds
        col_bounds[f]
            = std::min(((col_size - 1) / fourier_count + 1) * f, col_size);

        fourier_forwards[f] = NULL;
    }

    Set_actual_mode(actual_mode);
}

//============================================================================//
//  Append data with given tensor
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
template<typename Tensor>
void AggrOperator<KineticFactor, ThermalFactor>::Append_data(
    const int ind, Tensor & ten
)
{
    TensorTrain ker;

    double approx_time = omp_get_wtime();

    ker.Approximate(ten, approx_params);

    approx_time = omp_get_wtime() - approx_time;

    std::cout << "Aggregation kernel approximation: Mode = " << mode
        << ", Dim = " << dimensions[ind] << ", Time = "
        << std::setprecision(2) << std::scientific << std::fixed
        << approx_time << " (s)" << std::endl;

    Append_data(ind, ker);
}

// append data with given tensor in tensor train format
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Append_data(
    const int ind, const TensorTrain & ker
)
{
    const int dim = ker.Get_dimension();

    // set dimension
    dimensions[ind] = dim;

    // set ranks
    ranks[ind] = NULL;
    Set_array(dim + 1, ker.Export_ranks(), ranks[ind]);

    // update maximal rank
    max_rank = std::max(max_rank, ranks[ind][iamax(dim, ranks[ind], 1)]);

    // set factor columns
    factor_cols[ind] = (int *)malloc((dim + 1) * sizeof(int));

    for (int d = 0; d < dim; ++d)
    {
        factor_cols[ind][d] = col_size;
        col_size += ranks[ind][d] * ranks[ind][d + 1];
    }

    factor_cols[ind][dim] = col_size;

    // update data
    data = (double *)realloc(data, col_size * mode * sizeof(double));

    for (int d = 0; d < dim; ++d)
    {
        copy(
            ranks[ind][d] * ranks[ind][d + 1] * mode, ker.Export_carriage(d), 1,
            data + factor_cols[ind][d] * mode, 1
        );
    }
}

//============================================================================//
//  Initialization
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
AggrOperator<KineticFactor, ThermalFactor>::AggrOperator(
    const int mod, const int act_mod, const bool therm_flag
):
    thermal_flag(therm_flag),
    kinetic_factor(NULL),
    thermal_factor(NULL),
    mode(mod),
    actual_mode(act_mod? act_mod: mod),
    approx_params(),
    count(0),
    dimensions(NULL),
    ranks(NULL),
    max_rank(0),
    factor_cols(NULL),
    col_size(0),
    data(NULL),
    row_bounds(NULL),
    col_bounds(NULL),
    fourier_size(0),
    fourier_workspace(NULL),
    fourier_count(0),
    fourier_forwards(NULL),
    fourier_backward(NULL),
    workspace_size(0),
    workspace(NULL)
{}

//============================================================================//
//  Deallocation 
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
AggrOperator<KineticFactor, ThermalFactor>::~AggrOperator()
{
    if (dimensions) { free(dimensions); dimensions = NULL; }

    if (ranks)
    {
        for (int k = 0; k < count; ++k) { if (ranks[k]) { free(ranks[k]); } }

        free(ranks);
        ranks = NULL;
    }

    if (factor_cols)
    {
        for (int k = 0; k < count; ++k)
        {
            if (factor_cols[k]) { free(factor_cols[k]); }
        }

        free(factor_cols);
        factor_cols = NULL;
    }

    if (data) { free(data); data = NULL; }
    if (row_bounds) { free(row_bounds); row_bounds = NULL; }
    if (col_bounds) { free(col_bounds); col_bounds = NULL; }

    if (fourier_workspace)
    {
        mkl_free(fourier_workspace);
        fourier_workspace = NULL;
    }

    if (fourier_forwards)
    { 
        for (int f = 0; f < fourier_count; ++f)
        {
            if (fourier_forwards[f])
            {
                DftiFreeDescriptor(fourier_forwards + f);
            }
        }

        free(fourier_forwards);
        fourier_forwards = NULL;
    }

    if (fourier_backward)
    {
        DftiFreeDescriptor(&fourier_backward);
        fourier_backward = NULL;
    }

    if (workspace) { free(workspace); workspace = NULL; }

    kinetic_factor = NULL;
    thermal_factor = NULL;

    count = 0;
    max_rank = 0;
    col_size = 0;
    fourier_size = 0;
    fourier_count = 0;
    workspace_size = 0;
}

//============================================================================//
//  Set thermal dependence
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_kinetic_factor(
    KineticFactor & ten
)
{
    if (ten.Get_dimension() != 1)
    {
        std::cerr << "Error: A tensor of the requested dimension \""
            << ten.Get_dimension() << "\"" << "    "
            << "can not be set as a kinetic factor" << std::endl;

        exit(EXIT_FAILURE);
    }

    kinetic_factor = &ten;
}

template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_thermal_factor(
    ThermalFactor & ten
)
{
    if (ten.Get_dimension() != 1)
    {
        std::cerr << "Error: A tensor of the requested dimension \""
            << ten.Get_dimension() << "\"" << "    "
            << "can not be set as a thermal factor" << std::endl;

        exit(EXIT_FAILURE);
    }

    thermal_factor = &ten;
}

//============================================================================//
//  Set actual mode
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_actual_mode(
    const int act_mod
)
{
    double mod = std::min(act_mod, mode);
    bool fourier_reset = (mod != actual_mode || !fourier_size);

    actual_mode = mod;

    if (fourier_reset)
    {
        const MKL_LONG is[2] = { 0, 1 };
        const MKL_LONG os[2] = { 0, col_size };

        // set fourier parameters
        fourier_size = 2 * actual_mode;

        // allocate data and mode bounds
        row_bounds[fourier_count] = actual_mode + 1;

        const double dzero = 0.;

#pragma omp parallel for
        for (int f = 0; f < fourier_count; ++f)
        {
            // set mode bounds
            row_bounds[f]
                = std::min(
                    (actual_mode / fourier_count + 1) * f, actual_mode + 1
                );

            // set required zeros for fourier preimage
            copy(
                actual_mode, &dzero, 0,
                workspace + col_size + 2 * max_rank + (2 * f + 1) * actual_mode,
                1
            );

            // DFTI forward descriptor handler initialization
            if (fourier_forwards[f])
            {
                DftiFreeDescriptor(fourier_forwards + f);
                fourier_forwards[f] = NULL;
            }

            Check_DFTI_status(
                DftiCreateDescriptor(
                    fourier_forwards + f, DFTI_DOUBLE, DFTI_REAL, 1,
                    fourier_size
                )
            );

            Check_DFTI_status(
                DftiSetValue(
                    fourier_forwards[f], DFTI_CONJUGATE_EVEN_STORAGE,
                    DFTI_COMPLEX_COMPLEX
                )
            );

            Check_DFTI_status(
                DftiSetValue(
                    fourier_forwards[f], DFTI_PLACEMENT, DFTI_NOT_INPLACE
                )
            );

            Check_DFTI_status(
                DftiSetValue(fourier_forwards[f], DFTI_INPUT_STRIDES, is)
            );

            Check_DFTI_status(
                DftiSetValue(fourier_forwards[f], DFTI_OUTPUT_STRIDES, os)
            );

            Check_DFTI_status(
                DftiSetValue(
                    fourier_forwards[f], DFTI_BACKWARD_SCALE,
                    1. / double(fourier_size)
                )
            );

            Check_DFTI_status(DftiCommitDescriptor(fourier_forwards[f]));

            Check_DFTI_status(
                DftiComputeForward(
                    fourier_forwards[f], workspace + f * fourier_size,
                    fourier_workspace + f
                )
            );
        }

        // DFTI backward descriptor handler initialization
        if (fourier_backward)
        {
            DftiFreeDescriptor(&fourier_backward);
            fourier_backward = NULL;
        }

        Check_DFTI_status(
            DftiCopyDescriptor(fourier_forwards[0], &fourier_backward)
        );

        Check_DFTI_status(
            DftiSetValue(fourier_backward, DFTI_OUTPUT_STRIDES, is)
        );

        Check_DFTI_status(DftiCommitDescriptor(fourier_backward));

        Check_DFTI_status(
            DftiComputeBackward(
                fourier_backward, fourier_workspace,
                workspace + workspace_size - fourier_size
            )
        );
    }
}

//============================================================================//
//  Set approximation parameters
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_approx_params(
    const TensorTrainParameters & pars
)
{
    approx_params = pars;
}

//============================================================================//
//  Set kinetic coefficients representation data
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
template<typename Tensor>
void AggrOperator<KineticFactor, ThermalFactor>::Set_data(
    const int cnt, Tensor * tens
)
{
    for (int k = 0; k < cnt; ++k)
    {
        if (tens[k].Get_dimension() < 2)
        {
            std::cerr << "Error: A tensor of the requested dimension \""
                << tens[k].Get_dimension() << "\"" << "    "
                << "can not be set as an aggregation kernel" << std::endl;

            exit(EXIT_FAILURE);
        }
    }

    Set_layout(cnt);

    for (int k = 0; k < count; ++k) { Append_data<Tensor>(k, tens[k]); }

    Set_workspace();
}

//============================================================================//
//  Get dimension
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
int AggrOperator<KineticFactor, ThermalFactor>::Get_dimension(
    const int ind
) const
{
    if (dimensions && ind >= 0 && ind < count) { return dimensions[ind]; }
    else { return 0; }
}

//============================================================================//
//  Export ranks
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
const int * AggrOperator<KineticFactor, ThermalFactor>::Export_ranks(
    const int ind
) const
{
    if (ranks && ind >= 0 && ind < count) { return ranks[ind]; }
    else { return NULL; }
}

//============================================================================//
//  Compute convolutional term of operator action 
//============================================================================//
// res += Conv(arg)
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Conv(
    const double * arg, double * res
)
{
    int dim;

    // thermal dependence factor
    double factor;

    // storage for intermediate data
    double * updated_data = workspace + col_size + 2 * max_rank;
    double * part_res  = workspace + workspace_size - fourier_size;

    Complex * fourier_res
        = fourier_workspace + col_size * (mode + 1)
            + 2 * fourier_count * max_rank;

    //========================================================================//
    //  Update factors and perform fourier transformations
    //========================================================================//
#pragma omp parallel
    {
        const int f = omp_get_thread_num();

        for (int c = col_bounds[f]; c < col_bounds[f + 1]; ++c)
        {
            for (int m = 0; m < actual_mode; ++m)
            {
                updated_data[f * fourier_size + m]
                    = data[c * mode + m] * arg[m];
            }

            Check_DFTI_status(
                DftiComputeForward(
                    fourier_forwards[f], updated_data + f * fourier_size,
                    fourier_workspace + c
                )
            );
        }
    }

    //========================================================================//
    //  Multiply updated factor images elementwise
    //========================================================================//
    for (int k = 0; k < count; ++k)
    {
        dim = dimensions[k];

#pragma omp parallel
        {
            const int f = omp_get_thread_num();

            Complex * prod
                = fourier_workspace + col_size * (mode + 1) + max_rank * f;

            Complex * tmp = prod + max_rank * fourier_count;
            Complex * right_mult;

            for (int m = row_bounds[f]; m < row_bounds[f + 1]; ++m)
            {
                right_mult
                    = fourier_workspace + factor_cols[k][dim - 1]
                        + m * col_size;

                for (int d = dim - 2; d >= 0; --d)
                {
                    //========================================================//
                    //  Multiply along right rank dimension  
                    //========================================================//
                    // prod = left_mult · right_mult
                    gemv(
                        CblasColMajor, CblasTrans, ranks[k][d + 1], ranks[k][d],
                        Complex(1., 0.), fourier_workspace + factor_cols[k][d]
                            + m * col_size,
                        ranks[k][d + 1], right_mult, 1, Complex(0., 0.), prod, 1
                    );

                    right_mult = prod;
                    std::swap(prod, tmp);
                }

                fourier_res[m] = right_mult[0];
            }
        }

        //====================================================================//
        //  Perform inverse fourier transformation
        //====================================================================//
        Check_DFTI_status(
            DftiComputeBackward(fourier_backward, fourier_res, part_res)
        );

        //====================================================================//
        //  Zeroize negative values
        //====================================================================//
        Zeroize_negative(actual_mode, part_res);

        //====================================================================//
        //  Take thermal dependence into account
        //====================================================================//
        factor = 1.;

        if (thermal_flag)
        {
            factor = (*kinetic_factor)(&dim, arg[mode]);

            res[mode]
                -= factor * arg[mode]
                    * Moment(0, actual_mode - dim + 1, part_res);
        }

        //====================================================================//
        //  Update resulting vector
        //====================================================================//
        axpy(actual_mode - dim + 1, factor, part_res, 1, res + dim - 1, 1);
    }
}

//============================================================================//
//  Matvec term of operator action
//============================================================================//
// res -= Matvec(arg)
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::Matvec(
    const double * arg, double * res
)
{
    int dim;

    // thermal dependence factor
    double factor;

    // workspace[col_size]
    // prod[max_rank * actual_mode]
    // tmp[max_rank * actual_mode]
    double * prod = workspace + col_size;
    double * tmp = prod + max_rank;
    double * right_mult;

    // partial result storage
    double * part_res = workspace + workspace_size;

    //========================================================================//
    //  Perform matrix by vector products
    //========================================================================//
    for (int k = 0; k < count; ++k)
    {
        dim = dimensions[k];

        //====================================================================//
        //  Update factors
        //====================================================================//
        // workspace = data · arg
        gemv(
            CblasColMajor, CblasTrans, actual_mode,
            factor_cols[k][dim] - factor_cols[k][1], 1.,
            data + factor_cols[k][1] * mode, mode, arg, 1, 0.,
            workspace + factor_cols[k][1], 1
        );

        right_mult = workspace + factor_cols[k][dim - 1];

        for (int d = dim - 2; d > 0; --d)
        {
            //================================================================//
            //  Multiply along right rank dimension
            //================================================================//
            // prod = left_mult · right_mult
            gemv(
                CblasColMajor, CblasTrans, ranks[k][d + 1], ranks[k][d], 1.,
                workspace + factor_cols[k][d], ranks[k][d + 1], right_mult, 1,
                0., prod, 1
            );

            right_mult = prod;
            std::swap(prod, tmp);
        }

        //====================================================================//
        //  Multiply by leftmost factor to obtain partial result
        //====================================================================//
        gemv(
            CblasColMajor, CblasNoTrans, actual_mode, ranks[k][1], 1.,
            data + factor_cols[k][0] * mode, mode, right_mult, 1, 0., part_res,
            1
        );

#pragma omp parallel for
        for (int m = 0; m < actual_mode; ++m) { part_res[m] *= arg[m]; }
        
        //====================================================================//
        //  Zeroize negative values
        //====================================================================//
        Zeroize_negative(actual_mode, part_res);

        //====================================================================//
        //  Take thermal dependence into account
        //====================================================================//
        factor = double(dim);

        if (thermal_flag)
        {
            factor *= (*kinetic_factor)(&dim, arg[mode]);

            res[mode]
                += (factor * arg[mode] - (*thermal_factor)(&dim, arg[mode]))
                    * Moment(0, actual_mode, part_res);
        }

        //====================================================================//
        //  Update resulting vector
        //====================================================================//
        axpy(actual_mode, -factor, part_res, 1, res, 1);
    }
}

//============================================================================//
//  Action    
//============================================================================//
// res += Conv(arg) - Matvec(arg)
template<typename KineticFactor, typename ThermalFactor>
void AggrOperator<KineticFactor, ThermalFactor>::operator()(
    const double * arg, double * res
)
{
    if (data) { Conv(arg, res); Matvec(arg, res); }
}

} // namespace tpbes

#endif // AGGR_OPERATOR_HPP
