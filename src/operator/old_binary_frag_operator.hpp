#ifndef BINARY_FRAG_OPERATOR_HPP
#define BINARY_FRAG_OPERATOR_HPP

#include <iostream>
#include <iomanip>

namespace tpbes {

/*******************************************************************************

    BinaryFragOperator -- Tensor train based binary fragmentation operator

*******************************************************************************/

// unified set method for data initialization
template<typename Tensor>
void BinaryFragOperator::Set_data(Tensor & ten, void * pars)
{
    ten.Set_modes(2, mode);
    ten.Set_params(pars);

    TensorTrain ker;

    double approx_time = omp_get_wtime();

    ker.Approximate(ten, approx_params);
    ker.Rearrange();
    ker.Compress(approx_params.comp_tolerance);

    approx_time = omp_get_wtime() - approx_time;

    std::cout << "Fragmentation kernel approximation: Mode = " << mode
        << ", Dim = 2, Time = " << std::setprecision(2) << std::scientific
        << std::fixed << approx_time << " (s)" << std::endl;

    Set_data(ker);
}

template<typename Tensor>
void BinaryFragOperator::Set_data(void * pars)
{
    Tensor ten;

    Set_data<Tensor>(ten, pars);
}

} // namespace tpbes

#endif // BINARY_FRAG_OPERATOR_HPP
