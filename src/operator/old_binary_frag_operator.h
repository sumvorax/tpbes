#ifndef BINARY_FRAG_OPERATOR_H
#define BINARY_FRAG_OPERATOR_H

#include "../tensor/tensor_train.h"
#include "../utility/definitions.h"

namespace tpbes {

//============================================================================//
//  BinaryFragOperator -- Tensor train based binary fragmentation operator
//============================================================================//
class BinaryFragOperator
{
    private:

        // total number of equations
        int mode;

        // number of actually used equations 
        int actual_mode;

        // approximation parameters
        TensorTrainParameters approx_params;

        // kernel rank
        int rank;

        // kernel factor size
        int factor_size;

        // kernel coefficients representation
        double * data;

        // mass distribution
        double * distr;

        // auxiliary memory
        double * workspace;

        // set kinetic coefficients representation
        template<typename Tensor>
        void Set_data(Tensor &, void *);

    public:

        BinaryFragOperator(const int, const int = 0);
        ~BinaryFragOperator();

        // set actual mode
        void Set_actual_mode(const int);

        // set approximation parameters
        void Set_approx_params(const TensorTrainParameters &);

        // set kinetic coefficients representation
        template<typename Tensor>
        void Set_data(void * = NULL);

        void Set_data(const TensorTrain &);
        void Set_data(BinIntElement);
        void Set_data(BinIntParamElement, void *);

        // set particle mass distribution
        void Set_distr(UnIntElement);
        void Set_distr(UnIntParamElement, void *);

        // setting indicators
        inline bool Is_data_set() const { return data != NULL; } 
        inline bool Is_distr_set() const { return distr != NULL; } 

        // access field
        inline int Get_mode() const { return mode; }
        inline int Get_actual_mode() const { return actual_mode; }
        inline int Get_rank() const { return rank; }

        // action
        void operator()(const double *, double *);
};

} // namespace tpbes

#include "binary_frag_operator.hpp"

#endif // BINARY_FRAG_OPERATOR_H
