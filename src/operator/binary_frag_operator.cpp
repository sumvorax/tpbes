// src/operator/binary_frag_operator.cpp

#include <iostream>
#include <iomanip>

#include "../tensor/tensor.h"
#include "../tensor/tensor_train.h"
#include "../utility/definitions.h"

namespace tpbes {

/*******************************************************************************

    BinaryFragOperator -- Tensor train based binary fragmentation operator

*******************************************************************************/

//============================================================================//
//  Initialization
//============================================================================//
BinaryFragOperator::BinaryFragOperator(const int mod, const int act_mod):
    mode(mod),
    actual_mode(act_mod? act_mod: mod),
    approx_params(),
    out_ker(2, mod),
    in_ker(3, mod),
    workspace(NULL)
{}

//============================================================================//
//  Deallocation
//============================================================================//
BinaryFragOperator::~BinaryFragOperator()
{
    if (workspace) { free(workspace); workspace = NULL; }

    mode = 0;
    actual_mode = 0;
}

//============================================================================//
//  Set income kernel
//============================================================================//

//============================================================================//
//  Set actual mode
//============================================================================//
void BinaryFragOperator::Set_actual_mode(const int act_mod)
{
    actual_mode = std::min(act_mod, mode);
}

//============================================================================//
//  Set approximation parameters
//============================================================================//
void BinaryFragOperator::Set_approx_params(const TensorTrainParameters & pars)
{
    approx_params = pars;
}

//============================================================================//
//  Export ranks
//============================================================================//
const int * BinaryFragOperator::Export_ranks(const int ind) const
{
    if (!ind) { return out_ker.Export_ranks(); }
    else if (ind == 1) { return in_ker.Export_ranks(); }
    else { return NULL; }
}

//============================================================================//
//  Action
//============================================================================//
// res += BinaryFragOperator<DataTensor, DistrTensor>(arg)
void BinaryFragOperator::operator()(const double * arg, double * res)
{
    const int rank = out_ker.Get_rank(1);
    const int * ranks = in_ker.Export_ranks();

    // workspace - for holding N times the product of two carriages of size Rr 
    // for holding N times quasi-matvec result of size Rl of left carriage
    double * left = workspace + mode * ranks[2];
    // for holding matvec result of size Rr x Rl of middle carriage
    double * right = left + mode * ranks[1];

    const double dzero = 0.;

    if (workspace)
    {
        // calculate outcome term
        gemv(
            CblasColMajor, CblasTrans, actual_mode, rank, 1.,
            out_ker.Export_carriage(0), mode, arg, 1, 0., left, 1
        );

        gemv(
            CblasColMajor, CblasNoTrans, actual_mode - 1, rank, 1.,
            out_ker.Export_carriage(1), mode, left + 1, 1, 0., workspace + 1, 1
        );

        // res -= outcome
#pragma omp parallel for
        for (int m = 1; m < actual_mode; ++m) { res -= workspace[m] * arg[m]; }

    // calculate income term
        copy(ranks[1], &dzero, 0, left + (actual_mode - 1) * ranks[1], 1);

        // calculate N times quasi-matvec of size Rl of left carriage
        for (int m = actual_mode - 2; m >= 0; --m)
        {
            copy(
                ranks[1], left + (m + 1) * ranks[1], 1, left + m * ranks[1], 1
            );

            axpy(
                ranks[1], arg[m + 1], in_ker.Export_carriage(0), 1,
                left + m * ranks[1], 1
            );
        }

        // calculate matvec of size Rr x Rl of middle carriage
        gemv(
            CblasColMajor, CblasTrans, actual_mode, ranks[1] * ranks[2], 1.,
            in_ker.Export_carriage(1), mode, arg, 1, 0., right, 1
        );

        for (int m = 0; m < actual_mode; ++m)
        {
            // calculate N times the product of two carriages of size Rr 
            gemv(
                CblasColMajor, CblasNoTrans, ranks[2], ranks[1], 1., right,
                ranks[2], left + m * ranks[1], 1, 0., workspace + m * ranks[2],
                1
            );

            // res += income
            res[m]
                += dot(
                    ranks[2], workspace + m * ranks[2], 1,
                    in_ker.Export_carriage(2), mode
                );
        }
    }
}

} // namespace tpbes

// src/operator/binary_frag_operator.cpp
