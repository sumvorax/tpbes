#ifndef SOURCE_H
#define SOURCE_H

namespace tpbes {

//==============================================================================
//  Source
//==============================================================================
// Source operator.
template<typename KineticTensor, typename ThermalTensor = void>
class Source
{
    private: 

    // Thermal fields:

        // Temperature dependence indicator.
        bool thermal_flag;

        // Thermal source function wrapper.
        ThermalTensor * thermal_element;

    // Nonthermal fields:

        // Total number of equations.
        int mode;

        // Number of actually used equations.
        int actual_mode;

        // Concentration source wrapper.
        KineticTensor * kinetic_element;

        // Fixed concentration sources.
        double * data;

        // Fixed concentration sources moment.
        double data_moment;

        // Precalculates thermal source.
        void Set_thermal_data();

    public:

        ~Source();
        Source(const int, const int = 0, const bool = false);

        void Set_actual_mode(const int);

        // Sets concentration sources.
        void Set_kinetic_element(KineticTensor &);

        // Sets thermal sources.
        void Set_thermal_element(ThermalTensor &);

        inline int Get_mode() const { return mode; }
        inline int Get_actual_mode() const { return actual_mode; }

        // Compute the source vector of concentration.
        void operator()(const double &, const double &, double *);
};

#include "source.hpp"

} // namespace tpbes

#endif // SOURCE_H
