// src/operator/binary_frag_operator.cpp

#include "binary_frag_operator.h"

#include <cstdlib>
#include <cstring>

#include "../blas/blas.h"
#include "../tensor/tensor.h"
#include "../utility/utility.h"

namespace tpbes {

/*******************************************************************************

    BinaryFragOperator -- Binary fragmentation operator

*******************************************************************************/

//============================================================================//
//  Initialization
//============================================================================//
BinaryFragOperator::BinaryFragOperator(const int mod, const int act_mod):
    mode(mod),
    actual_mode(act_mod? act_mod: mod),
    approx_params(),
    rank(0),
    factor_size(0),
    data(NULL),
    distr(NULL),
    workspace(NULL)
{}

//============================================================================//
//  Deallocation
//============================================================================//
BinaryFragOperator::~BinaryFragOperator()
{
    if (data) { free(data); data = NULL; }
    if (distr) { free(distr); distr = NULL; }
    if (workspace) { free(workspace); workspace = NULL; }

    rank = 0;
    factor_size = 0;
    mode = 0;
    actual_mode = 0;
}

//============================================================================//
//  Set actual mode
//============================================================================//
void BinaryFragOperator::Set_actual_mode(const int act_mod)
{
    actual_mode = std::min(act_mod, mode);
}

//============================================================================//
//  Set approximation parameters
//============================================================================//
void BinaryFragOperator::Set_approx_params(const TensorTrainParameters & pars)
{
    approx_params = pars;
}

//============================================================================//
//  Set kinetic coefficients representation data
//============================================================================//
void BinaryFragOperator::Set_data(const TensorTrain & ker)
{
    if (ker.Get_dimension() != 2)
    {
        std::cerr << "Error: "
            << "Tensor train decomposition of requested dimension \""
            << ker.Get_dimension() << "\"\n" << "    "
            << "can not be set as binary fragmentation kernel" << std::endl;

        exit(EXIT_FAILURE);
    }

    rank = ker.Get_rank(1);
    factor_size = rank * mode;

    data = (double *)realloc(data, 2 * factor_size * sizeof(double));

    if (data)
    {
        copy(mode * rank, ker.Export_carriage(0), 1, data, 1);
        copy(mode * rank, ker.Export_carriage(1), 1, data + factor_size, 1);
    }

    workspace
        = (double *)realloc(
            workspace, (std::max(mode, rank) + rank) * sizeof(double)
        );
}

void BinaryFragOperator::Set_data(BinIntElement Elem)
{
    WrapTensor ten;
    ten.Set_element(Elem);

    Set_data<WrapTensor>(ten, NULL);
}

void BinaryFragOperator::Set_data(BinIntParamElement Elem, void * pars)
{
    WrapTensor ten;
    ten.Set_element(Elem);

    Set_data<WrapTensor>(ten, pars);
}

//============================================================================//
//  Set particle mass distribution
//============================================================================//
void BinaryFragOperator::Set_distr(UnIntElement Elem)
{
    WrapTensor ten(1, mode);
    ten.Set_element(Elem);

    if (!distr) { distr = (double *)malloc(mode * sizeof(double)); }
    ten.Get_vectorization(distr);
}

void BinaryFragOperator::Set_distr(UnIntParamElement Elem, void * pars)
{
    WrapTensor ten(1, mode);
    ten.Set_element(Elem, pars);

    if (!distr) { distr = (double *)malloc(mode * sizeof(double)); }
    ten.Get_vectorization(distr);
}

//============================================================================//
//  Action
//============================================================================//
// res += BinaryFragOperator(arg)
void BinaryFragOperator::operator()(const double * arg, double * res)
{
    if (data && distr)
    {
        double frag_mass = 0.;

        // workspace[max(rank, mode) + rank]
        // left_mult[rank]
        // updated_right_factor[rank]
        double * updated_arg = workspace + rank;
        double * updated_right_factor = workspace + std::max(rank, mode);

        // initialize updated_arg
#pragma omp parallel for
        for (int m = 1; m < actual_mode; ++m)
        {
            updated_arg[m] = (m + 1) * arg[m];
        }

        // workspace[0, rank) = factor[0][1, mode) · updated_arg
        gemv(
            CblasColMajor, CblasTrans, actual_mode - 1, rank, 1., data + 1,
            mode, updated_arg + 1, 1, 0., workspace, 1
        );

        // updated_right_factor = factor[1] · arg
        gemv(
            CblasColMajor, CblasTrans, actual_mode, rank, 1.,
            data + factor_size, mode, arg, 1, 0., updated_right_factor, 1
        );

        // frag_mass = workspace[0, rank) · updated_right_factor
        frag_mass = 2. * dot(rank, workspace, 1, updated_right_factor, 1);

        // res += frag_mass · distr
        axpy(actual_mode, frag_mass, distr, 1, res, 1);

        // workspace[1, mode) = -2 * factor[0][1, mode) · updated_right_factor
        gemv(
            CblasColMajor, CblasNoTrans, actual_mode - 1, rank, -2., data + 1,
            mode, updated_right_factor, 1, 0., workspace, 1
        );

        // res += workspace ∘ arg
#pragma omp parallel for
        for (int m = 1; m < actual_mode; ++m)
        {
            res[m] += workspace[m - 1] * arg[m];
        }
    }
}

} // namespace tpbes

// src/operator/binary_frag_operator.cpp
