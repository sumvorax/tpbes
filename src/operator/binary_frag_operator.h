#ifndef BINARY_FRAG_OPERATOR_H
#define BINARY_FRAG_OPERATOR_H

#include <cstdlib>

namespace tpbes {

//==============================================================================
//  BinaryFragOperator
//==============================================================================
// Tensor train based binary fragmentation operator.
class TensorTrainParameters;
class TensorTrain;

class BinaryFragOperator
{
    private:

        // Total number of equations.
        int mode;

        // Number of actually used equations.
        int actual_mode;

        // TT-approximation parameters.
        TensorTrainParameters approx_params;

        // Kernel TT-decomposition for the outcome term.
        TensorTrain out_ker;

        // Kernel TT-decomposition for the income term.
        TensorTrain in_ker;

        // Auxiliary memory.
        double * workspace;

    public:

        BinaryFragOperator(const int, const int = 0);
        ~BinaryFragOperator();

        void Set_actual_mode(const int);
        void Set_approx_params(const TensorTrainParameters &);

        // set method for outcome and income kernels
        template<typename DataTensor, typename DistrTensor>
        void Set_data(DataTensor &, DistrTensor &);

        inline bool Is_data_set() const { return workspace != NULL; } 

        inline int Get_mode() const { return mode; }
        inline int Get_actual_mode() const { return actual_mode; }
        const int * Export_ranks(const int) const;

        // Adds the binary fragmentation term to the resulting concentrations.
        void operator()(const double *, double *);
};

} // namespace tpbes

#include "binary_frag_operator.hpp"

#endif // BINARY_FRAG_OPERATOR_H
