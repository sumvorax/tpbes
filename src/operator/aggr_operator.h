#ifndef AGGR_OPERATOR_H
#define AGGR_OPERATOR_H

#include "../tensor/tensor_train.h"

namespace tpbes {

//============================================================================//
//  AggrOperator -- Tensor Train based aggregation operator
//============================================================================//
template<typename KineticFactor, typename ThermalFactor>
class AggrOperator
{
    private:

    // thermal fields

        // temperature dependence indicator
        bool thermal_flag;

        // temperature-dependent prefactor for kinetic equations
        KineticFactor * kinetic_factor;

        // temperature-dependent prefactor for temperature equation
        ThermalFactor * thermal_factor;

    // non-thermal fields

        // total number of equations
        int mode;

        // number of actually used equations 
        int actual_mode;

        // approximation parameters
        TensorTrainParameters approx_params;

        // quantity of kernels
        int count;

        // kernel dimensions
        int * dimensions;

        // kernel factor ranks
        int ** ranks;

        // maximal rank
        int max_rank;

        // data column numbers for each kernel factor
        int ** factor_cols;

        // total kernel column size
        int col_size;

        // data
        double * data;

        // row bounds for OMP threads
        int * row_bounds;

        // column bounds for OMP threads
        int * col_bounds;

        // DFT size
        int fourier_size;

        // complex auxiliary memory
        Complex * fourier_workspace;

        // quantity of DFTI forward descriptor handlers
        int fourier_count; 

        // forward DFTI descriptor handlers
        DFTI_DESCRIPTOR_HANDLE * fourier_forwards;

        // backward DFTI descriptor handler
        DFTI_DESCRIPTOR_HANDLE fourier_backward;

        // auxiliary memory capacity
        int workspace_size;

        // auxiliary memory
        double * workspace;

    // initialization routines

        // initialize data layout
        void Set_layout(const int);

        // initialize workspace
        void Set_workspace();

        // append data with given tensor
        template<typename Tensor>
        void Append_data(const int, Tensor &);

        void Append_data(const int, const TensorTrain &);

    public:

        AggrOperator(const int, const int = 0, const bool = false);
        ~AggrOperator();

        // set thermal dependence
        void Set_kinetic_factor(KineticFactor &);
        void Set_thermal_factor(ThermalFactor &);

        // set actual mode
        void Set_actual_mode(const int);

        // set approximation parameters
        void Set_approx_params(const TensorTrainParameters &);

        // set kinetic coefficients representation data
        template<typename Tensor>
        void Set_data(const int, Tensor *);

        // get dimensional parameters
        inline int Get_mode() const { return mode; }
        inline int Get_actual_mode() const { return actual_mode; }
        inline int Get_count() const { return count; }
        int Get_dimension(const int) const;

        const int * Export_ranks(const int) const;

        // action terms
        void Conv(const double *, double *);
        void Matvec(const double *, double *);

        // action
        void operator()(const double *, double *);
};

} // namespace tpbes

#include "aggr_operator.hpp"

#endif // AGGR_OPERATOR_H
