#ifndef UNARY_FRAG_OPERATOR_H
#define UNARY_FRAG_OPERATOR_H

namespace tpbes {

//==============================================================================
//  UnaryFragOperator
//==============================================================================
// Unary fragmentation operator.
class UnaryFragOperator
{
    private: 

        // Total number of equations.
        int mode;

        // Number of actually used equations.
        int actual_mode;

        // Fragmentaion coefficients.
        double * data;

        // Particle mass distribution.
        double * distr;

    public:

        UnaryFragOperator(const int, const int = 0);
        ~UnaryFragOperator();

        void Set_actual_mode(const int);

        // Sets fragmentation coefficients representation.
        template<typename Tensor>
        void Set_data(Tensor &);

        // Sets particle mass distribution
        template<typename Tensor>
        void Set_distr(Tensor &);

        inline bool Is_data_set() const { return data != NULL; } 
        inline bool Is_distr_set() const { return data != NULL; } 

        inline int Get_mode() const { return mode; }
        inline int Get_actual_mode() const { return actual_mode; }

        // Adds the unary fragmentation term to the resulting concentrations.
        void operator()(const double *, double *);
};

#include "unary_frag_operator.hpp"

} // namespace tpbes

#endif // UNARY_FRAG_OPERATOR_H
