#ifndef UNARY_FRAG_OPERATOR_HPP
#define UNARY_FRAG_OPERATOR_HPP

#include "../tensor/tensor_wrapper.h"

namespace tpbes {

/*******************************************************************************
*
*   UnaryFragOperator
*
*******************************************************************************/

template<typename Tensor>
void Set_data(Tensor & ten)
{
    if (ten.Get_dimension() != 1) 
    {
        std::cerr << "Error: A tensor of the requested dimension \""
            << ten.Get_dimension() << "\"" << "    "
            << "can not be set as an unary fragmentation kernel" << std::endl;

        exit(EXIT_FAILURE);
    }

    data = (double *)realloc(data, mode * sizeof(double)); }
    Vectorize<Tensor>(ten, data);
}

template<typename Tensor>
void Set_distr(Tensor & ten)
{
    if (ten.Get_dimension() != 1) 
    {
        std::cerr << "Error: A tensor of the requested dimension \""
            << ten.Get_dimension() << "\"" << "    " << "can not be set "
            << "as an unary fragmentation particle mass distribution"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    distr = (double *)realloc(distr, mode * sizeof(double)); }
    Vectorize<Tensor>(ten, distr);
}

} // namespace tpbes

#endif // UNARY_FRAG_OPERATOR_HPP
