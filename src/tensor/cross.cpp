// src/tensor/cross.cpp

#include "cross.h"

#include <cstdlib>
#include <iomanip>
#include <ostream>

#include "../utility/memory.h"

namespace tpbes {

/*******************************************************************************
*
*   CrossParams
*
*******************************************************************************/

CrossParams::CrossParams():
    tolerance(0.),
    iters_max_count(1),
    max_rank(1),
    rank_inc(1),
    stop_rank(0),
    memory_strategy(true),
    start_from_col(true)
{}

/*******************************************************************************
*
*   Cross
*
*******************************************************************************/

//==============================================================================
//  Destructor
//==============================================================================
Cross::~Cross()
{
    Free<int>(max_vol_inds[0]);
    Free<int>(max_vol_inds[1]);

    Free<double>(U);
    Free<double>(V);
    Free<double>(C);
    Free<double>(AR);
    Free<double>(CAT);
    Free<double>(RT);
    Free<double>(hat_A_inv);
}

//==============================================================================
//  Constructor
//==============================================================================
Cross::Cross():
    modes{ 0, 0 },
    rank(0),
    max_vol_inds{ NULL, NULL },
    U(NULL),
    V(NULL),
    C(NULL),
    AR(NULL),
    CAT(NULL),
    RT(NULL),
    hat_A_inv(NULL)
{}

//==============================================================================
//  Get maximal volume indices
//==============================================================================
const int * Cross::Get_max_vol_inds(const int d) const
{
    return (!d || d == 1)? max_vol_inds[d]: NULL;
}

int Cross::Get_max_vol_ind(const int d, const int ind) const
{
    return ((!d || d == 1) && max_vol_inds[d])? max_vol_inds[d][ind]: -1;
}

int Cross::Get_max_vol_row(const int ind) const
{
    return  max_vol_inds[0]? max_vol_inds[0][ind]: -1;
}

int Cross::Get_max_vol_col(const int ind) const
{
    return max_vol_inds[1]? max_vol_inds[1][ind]: -1;
}

//==============================================================================
//  Element access
//==============================================================================
double Cross::operator()(const int row, const int col) const
{
    return (U && V)? Dot(rank, U + row, modes[0], V + col, modes[1]): 0.;
}

//==============================================================================
//  Stream writing operator
//==============================================================================
std::ostream & operator<<(std::ostream & os, const tpbes::Cross & cross)
{
    os << std::scientific << std::setprecision(15);

    os << cross.modes[0]  << std::endl;
    os << cross.modes[1]  << std::endl;
    os << cross.rank      << std::endl;
    os << cross.norm      << std::endl;
    os << cross.tolerance << std::endl;

    for (int r = 0; r < cross.rank; ++r)
    {
        os << cross.max_vol_inds[0][r] << " ";
    }

    os << std::endl;

    for (int r = 0; r < cross.rank; ++r)
    {
        os << cross.max_vol_inds[1][r] << " ";
    }

    os << std::endl << std::endl;

    return os;
}

//==============================================================================
//  C, RT, CAT, AR, hat_A_inv and max_vol_inds derivation
//==============================================================================
void Set_cross(Cross & cross)
{
    if (!(cross.U && cross.V)) { return; }

    const int rank    = cross.rank;
    const int * modes = cross.modes;

    Reallocate<double>(cross.C,   rank * modes[0]);
    Reallocate<double>(cross.AR,  rank * modes[1]);
    Reallocate<double>(cross.CAT, rank * modes[0]);
    Reallocate<double>(cross.RT,  rank * modes[1]);

    // U -> CAT -> RT.
    Maxvol(
        modes[0], rank, cross.U, cross.max_vol_inds[0], cross.tolerance,
        cross.CAT
    );

    for (int r = 0; r < rank; ++r)
    {
        Gemv(
            CblasColMajor, CblasNoTrans, modes[1], rank, 1., cross.V, modes[1],
            cross.U + cross.max_vol_inds[0][r], modes[0], 0.,
            cross.RT + r * modes[1], 1
        );
    }

    // V -> AR -> C.
    Maxvol(
        modes[1], rank, cross.V, cross.max_vol_inds[1], cross.tolerance,
        cross.AR
    );

    for (int r = 0; r < rank; ++r)
    {
        Gemv(
            CblasColMajor, CblasNoTrans, modes[0], rank, 1., cross.U, modes[0],
            cross.V + cross.max_vol_inds[1][r], modes[1], 0.,
            cross.C + r * modes[0], 1
        );
    }

    int    * ipiv = Allocate<int>(rank);
    double * mat  = Allocate<double>(rank * rank);

    // C -> CAT.
    for (int r = 0; r < rank; ++r)
    {
        Copy<double>(modes[0], cross.C + r * modes[0], 1, cross.CAT + r, rank);

        Copy<double>(
            rank, cross.C + cross.max_vol_inds[0][r], modes[0], mat + r * rank,
            1
        );
    }

    Gesv(LAPACK_COL_MAJOR, rank, modes[0], mat, rank, ipiv, cross.CAT, rank);

    // RT -> AR.
    for (int r = 0; r < rank; ++r)
    {
        Copy<double>(modes[1], cross.RT + r * modes[1], 1, cross.AR + r, rank);

        Copy<double>(
            rank, cross.RT + cross.max_vol_inds[1][r], modes[1], mat + r * rank,
            1
        );
    }

    Gesv(LAPACK_COL_MAJOR, rank, modes[1], mat, rank, ipiv, cross.AR, rank);

    Free<int>(ipiv);

    // RT -> hat_A_inv.
    for (int r = 0; r < rank; ++r)
    {
        Copy<double>(
            rank, cross.RT + cross.max_vol_inds[1][r], modes[1], mat + r * rank,
            1
        );
    }

    // NOTE: Magic constant 1e-8
    // is the square of the machine double precision,
    // which is the maximal available one for this operation.
    cross.hat_A_inv = Pseudoinverse(1e-8, rank, rank, mat);

    Free<double>(mat);
}

} // namespace tpbes

// src/tensor/cross.cpp
