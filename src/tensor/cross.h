#ifndef CROSS_H
#define CROSS_H

#include <cstdlib>
#include <ostream>

namespace tpbes {

/*******************************************************************************
*
*   CrossParams
*
*******************************************************************************/

// Stores matrix cross-approximation parameters.
struct CrossParams
{
    double tolerance;

    int iters_max_count;
    int max_rank;
    int rank_inc;
    int stop_rank;

    bool memory_strategy;
    bool start_from_col;

    CrossParams();
};

/*******************************************************************************
*
*   Cross
*
*******************************************************************************/

// Stores cross-approximation for a matrix.
class Cross
{
    template<typename Matrix>
    friend void Approximate(const Matrix &, const CrossParams &, Cross &);

    friend void Set_cross(Cross &);

    friend std::ostream & operator<<(std::ostream &, const Cross &);

    private:

    // Description data:
        int    modes[2];
        int    rank;
        double norm;
        double tolerance;

        // Row / column indices corresponding to maximal volume submatrices. 
        int * max_vol_inds[2];

    // Decomposition data:
        double * U;
        double * V;
        double * C; 
        double * AR; 
        double * CAT;
        double * RT;
        double * hat_A_inv;

    public:

        ~Cross();
        Cross();

    // Matrix cross approximation.
        template<typename Matrix>
        Cross(const Matrix &, const CrossParams &, const bool = true);

        template<typename Matrix>
        void Approximate(
            const Matrix &, const CrossParams &, const bool = true
        );

    // Get description data routines:
        static inline int Get_dimension() { return 2; }

        int Get_mode(const int d) const { return (!d || d == 1)? modes[d]: 0; }
        inline int Get_row_size() const { return modes[0]; }
        inline int Get_col_size() const { return modes[1]; }

        inline const int * Get_modes() const { return modes; }
        inline int Get_rank() const { return rank; }
        inline double Get_norm() const { return norm; }
        inline double Get_tolerance() const { return tolerance; }

        const int * Get_max_vol_inds(const int) const;
        int Get_max_vol_ind(const int, const int) const;
        int Get_max_vol_row(const int) const;
        int Get_max_vol_col(const int) const;

    // Get decomposition data routines:
        inline const double * Get_U() const { return U; }
        inline const double * Get_V() const { return V; }
        inline const double * Get_C() const { return C; }
        inline const double * Get_AR() const { return AR; }
        inline const double * Get_CAT() const { return CAT; }
        inline const double * Get_RT() const { return RT; }
        inline const double * Get_hat_A_inv() const { return hat_A_inv; }

        inline double * Detach_U() { double * r = U; U = NULL; return r; }
        inline double * Detach_V() { double * r = V; V = NULL; return r; }
        inline double * Detach_CAT() { double * r = CAT; CAT = NULL; return r; }

    // Element access:
        double operator()(const int, const int) const;
        double operator[](const int * i) const { return (*this)(i[0], i[1]); }
};

// Stream writing operator
std::ostream & operator<<(std::ostream & os, const tpbes::Cross & cross);

/*******************************************************************************
*
*   Matrix cross-approximation
*
*******************************************************************************/

template<typename Matrix>
void Approximate(const Matrix &, const CrossParams &, Cross &);

void Set_cross(Cross &);

} // namespace tpbes

#include "cross.hpp"

#endif // CROSS_H
