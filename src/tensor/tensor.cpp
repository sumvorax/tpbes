// src/tensor/tensor.cpp

#include "tensor.h"

#include <omp.h>

#include "../utility/definitions.h"
#include "../utility/memory.h"

namespace tpbes {

/*******************************************************************************
*
*   TensorWrapper
*
*******************************************************************************/

//==============================================================================
//  operator[]
//==============================================================================
template<>
double TensorWrapper<VectorElementMapping>::operator[](const int * inds) const
{
    return Mapping? Mapping(inds[0]): 0.;
}

template<>
double TensorWrapper<MatrixElementMapping>::operator[](const int * inds) const
{
    return Mapping? Mapping(inds[0], inds[1]): 0.;
}

template<>
double TensorWrapper<TensorElementMapping>::operator[](const int * inds) const
{
    return Mapping? Mapping(dimension, inds): 0.;
}

/*******************************************************************************
*
*   Predefined tensor element functions
*
*******************************************************************************/

//============================================================================//
//  Factorial
//============================================================================//
static int Factorial(const int num)
{
    if (num == 1 || !num) { return 1; }
    else { return num * Factorial(num - 1); }
}

//==============================================================================
//  Const_element
//==============================================================================
double Const_element(const int dim, const int *) { return 1. / Factorial(dim); }

//==============================================================================
//  Gen_sum_element
//==============================================================================
double Gen_sum_element(const int dim, const int * inds)
{
    double sum = 0.;

    for (int d = 0; d < dim; ++d) { sum += inds[d] + 1.; }

    return std::sqrt(sum) / Factorial(dim);
}

//==============================================================================
//  Next_permutation
//==============================================================================
// Implements next permutation transposition algorithm (see Wikipedia).
// Returns: false if perm is last permutation, true otherwise
static bool Next_permutation(const int size, int * perm)
{
    int i;
    int j;

    // j = argmax {perm[j] < perm[j + 1]}
    for (j = size - 2; j >= 0; --j) { if (perm[j] < perm[j + 1]) { break; } }

    // Last permutation check.
    if (j < 0) { return false; }

    // i = argmax {i > j and perm[i] > perm[j]}
    for (i = size - 1; i > j; --i) { if (perm[j] < perm[i]) { break; } }

    std::swap(perm[i], perm[j]);

    // Reverse sequence {perm[j + 1], ..., perm[d - 1]}.
    for (i = j + 1, j = size - 1; i < j; ++i, --j)
    {
        std::swap(perm[i], perm[j]);
    }

    return true;
}

//==============================================================================
//  Gen_prod_element
//==============================================================================
double Gen_prod_element(const int dim, const int * inds)
{
    int indices[dim];

    for (int d = 0; d < dim; ++d) { indices[d] = d + 1; }

    double value = 0.;
    double prod  = 1.;

    while (int(prod))
    {
        for (int d = 0; d < dim; ++d)
        {
            prod *= std::pow(inds[d] + 1., 0.45 - 0.1 * indices[d]);
        }

        value += prod;

        prod = double(Next_permutation(dim, indices));
    }

    return value / Factorial(dim);
}

} // namespace tpbes

// src/tensor/tensor.cpp
