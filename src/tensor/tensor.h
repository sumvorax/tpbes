#ifndef TENSOR_H
#define TENSOR_H

#include "../utility/definitions.h"
#include "../utility/memory.h"

namespace tpbes {

/*******************************************************************************
*
*   Generic functions with tensors
*
*******************************************************************************/

//==============================================================================
//  Get_size
//==============================================================================
template<typename Tensor>
int Size(const Tensor & ten);

//==============================================================================
//  Element
//==============================================================================
template<typename Tensor>
double Element(const Tensor & ten, const int ind);

template<typename Tensor>
double Element(const Tensor & ten, const int ind, int * buf);

//==============================================================================
//  Element
//==============================================================================
template<typename Tensor>
double Norm(const Tensor &);

//==============================================================================
//  Vectorize
//==============================================================================
// Fills in the lexicographically ordered vectorized representation
// of a given tensor.
//
// If [res_size] is -1 then it is set to the result of [Get_size]
// for a given tensor.
template<typename Tensor>
void Vectorize(const Tensor & ten, double * res, const int res_size = -1);

/*******************************************************************************
*
*   TensorWrapper
*
*******************************************************************************/

// Tensor wrapper for an element function of a given signature.
template<typename Type>
class TensorWrapper
{
    private:

        int dimension;

        int * modes;

        int size;

        // Wrapped element function.
        Type Mapping;

    public:

        ~TensorWrapper() { Free<int>(modes); }

        TensorWrapper() = delete;
        TensorWrapper(const TensorWrapper &) = delete;
        TensorWrapper(const int, const int, Type);
        TensorWrapper(const int, const int *, Type);

        inline int Get_dimension() const { return dimension; }

        int Get_mode(const int) const;
        inline const int * Get_modes() const { return modes; }

        inline int Get_size() const { return size; }

        inline const Type Get_mapping() const { return Mapping; }

        // Computes tensor element for a given multiindex.
        double operator[](const int *) const { return 0.; }

        // Computes tensor element for a vectorized index.
        double operator[](const int ind) const { return Element(*this, ind); }

        // Computes tensor element for a given multiindex.
        void Vectorize(double * v) const { tpbes::Vectorize(*this, v, size); }
};

template<>
double TensorWrapper<VectorElementMapping>::operator[](const int *) const;

template<>
double TensorWrapper<MatrixElementMapping>::operator[](const int *) const;

template<>
double TensorWrapper<TensorElementMapping>::operator[](const int *) const;

/*******************************************************************************
*
*   Predefined tensor element functions
*
*******************************************************************************/

// Constant tensor element.
double Const_element(const int, const int *);

// Generalized sum tensor element.
double Gen_sum_element(const int, const int *); 

// Generalized product tensor element.
double Gen_prod_element(const int, const int *); 

} // namespace tpbes

#include "tensor.hpp"

#endif // TENSOR_H
