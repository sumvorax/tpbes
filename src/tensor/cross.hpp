#ifndef CROSS_HPP
#define CROSS_HPP

#include <omp.h>

#include <cstdlib>

#include "../utility/blas.h"
#include "../utility/memory.h"

namespace tpbes {

/*******************************************************************************
*
*   Cross
*
*******************************************************************************/

//==============================================================================
//  Constructor
//==============================================================================
template<typename Matrix>
Cross::Cross(const Matrix & mat, const CrossParams & pars, const bool set_flag):
    modes{ 0, 0 },
    rank(0),
    max_vol_inds{ NULL, NULL },
    U(NULL),
    V(NULL),
    C(NULL),
    AR(NULL),
    CAT(NULL),
    RT(NULL),
    hat_A_inv(NULL)
{
    tpbes::Approximate<Matrix>(mat, pars, *this);
    if (set_flag) { Set_cross(*this); }
}

//==============================================================================
//  Approximate
//==============================================================================
template<typename Matrix>
void Cross::Approximate(
    const Matrix & mat, const CrossParams & pars, const bool set_flag
)
{
    tpbes::Approximate<Matrix>(mat, pars, *this);
    if (set_flag) { Set_cross(*this); }
}

/*******************************************************************************
*
*   Matrix cross-approximation
*
*******************************************************************************/

//==============================================================================
//  Search_new_ind
//==============================================================================
template<typename Matrix, typename Data, int d>
static void Search_new_ind(const Matrix & mat, Data & data, Cross & cross)
{
    const int    * modes      = cross.Get_modes();
    const double * factors[2] = { cross.Get_U(), cross.Get_V() };

    double * vec = data.curr_vecs[d];

#pragma omp parallel for
    for (int m = 0; m < modes[!d]; ++m)
    {
        if (!data.approx_inds[!d][m])
        {
            int mat_inds[2];

            mat_inds[d]  = data.max_vol[d];
            mat_inds[!d] = m;

            vec[m] = mat(mat_inds[0], mat_inds[1]);
        }
    }

    Gemv(
        CblasColMajor, CblasNoTrans, modes[!d], cross.Get_rank(), -1.,
        factors[!d], modes[!d], factors[d] + data.max_vol[d], modes[d], 1., vec,
        1
    );

    int inds[data.threads[d]];

#pragma omp parallel for
    for (int t = 0; t < data.threads[d]; ++t)
    {
        inds[t] = data.max_vol[!d];

        for (int b = data.bounds[d][t]; b < data.bounds[d][t + 1]; ++b)
        {
            if (
                !data.approx_inds[!d][b]
                && std::fabs(vec[inds[t]]) < std::fabs(vec[b])
            ) { inds[t] = b; }
        }
    }

    int i = inds[0];

    for (int t = 1; t < data.threads[d]; ++t)
    {
        if (std::fabs(vec[i]) < std::fabs(vec[inds[t]])) { i = inds[t]; }
    }

    data.max_vol.Set(vec[i], !d, i);
}

//==============================================================================
//  Search_max_vol
//==============================================================================
template<typename Matrix, typename Data, int d>
void Search_max_vol(const Matrix & mat, Data & data, Cross & cross)
{
    const int   rank  = cross.Get_rank();
    const int * modes = cross.Get_modes();

    if (rank == std::min(modes[0], modes[1])) { return; }

    int max_inds[2] = { -1, -1 };

    for (int dir: { 0, 1 })
    {
        int k = rand() % (modes[dir] - rank);

        for (int r = 0; r <= k; )
        {
            if (!data.approx_inds[dir][max_inds[dir] + 1]) { ++r; }
            ++max_inds[dir];
        }
    }

    data.max_vol.Set(
        mat(max_inds[0], max_inds[1]) - cross(max_inds[0], max_inds[1]),
        max_inds
    );

    int prev_inds[2] = { -1, -1 };

    for (
        int i = 0;
        !data.params.iters_max_count || (i < data.params.iters_max_count);
        ++i
    )
    {
        if (prev_inds[d] != data.max_vol[d])
        {
            Search_new_ind<Matrix, Data, d>(mat, data, cross);
            prev_inds[d] = data.max_vol[d];
        }

        if (prev_inds[!d] != data.max_vol[!d])
        {
            Search_new_ind<Matrix, Data, !d>(mat, data, cross);
            prev_inds[!d] = data.max_vol[!d];
        }
        else { break; }
    }
}

//==============================================================================
//  Approx_loop
//==============================================================================
template<typename Matrix, typename Data, int d>
static void Approx_loop(const Matrix & mat, Data & data, Cross & cross)
{
    while (true)
    {
        Search_max_vol<Matrix, Data, d>(mat, data, cross);

        if (data.Stopping_criteria(cross)) { break; }
        else { data.Update(cross); }
    }
}

//==============================================================================
//  Approximate
//==============================================================================
template<typename Matrix>
void Approximate(const Matrix & mat, const CrossParams & pars, Cross & cross)
{
    //==========================================================================
    //  Data
    //==========================================================================
    // Stores matrix cross approximation working data.
    struct Data
    {
        //======================================================================
        //  Volume
        //======================================================================
        // Stores the determinant of a submatrix and corresponding row, column.
        struct Volume
        {
            int inds[2];
    
            double vol;
    
            Volume(): inds{ 0, 0 }, vol(0.) {}
    
            void Set(const double & v, const int * i)
            {
                Copy<int>(2, i, 1, inds, 1);
                vol = v;
            }
    
            inline void Set(const double & v, const int d, const int i)
            {
                inds[d] = i;
                vol = v;
            }
    
            // Returns row / column index corresponding to the volume.
            inline int operator[](const int d) const { return inds[d]; }
    
            // Returns the volume.
            inline double operator()() const { return vol; }
        };
    
        //======================================================================
        //  Fields
        //======================================================================
        CrossParams params;
    
        Volume max_vol;

        bool   * approx_inds[2];
        double * curr_vecs[2];
        int      threads[2];
        int    * bounds[2];
    
        //======================================================================
        //  Destructor
        //======================================================================
        ~Data()
        {
            for (int d: { 0, 1 })
            {
                Free<bool>(approx_inds[d]);
                Free<int>(bounds[d]);
            }
        }

        //======================================================================
        //  Constructor
        //======================================================================
        Data(const CrossParams & pars, Cross & cross): params(pars), max_vol()
        {
            if (params.max_rank <= 0) { params.max_rank = 1; }

            if (params.rank_inc <= 0 && params.memory_strategy)
            {
                params.rank_inc = 1;
            }
            else if (params.rank_inc <= 1 && !params.memory_strategy)
            {
                params.rank_inc = 2;
            }

            cross.rank      = 0;
            cross.norm      = 0.;
            cross.tolerance = params.tolerance;

            Reallocate<int>(cross.max_vol_inds[0], params.max_rank);
            Reallocate<int>(cross.max_vol_inds[1], params.max_rank);

            Reallocate<double>(cross.U, params.max_rank * cross.modes[0]);
            Reallocate<double>(cross.V, params.max_rank * cross.modes[1]);

            for (int d: { 0, 1 })
            {
                approx_inds[d] = Allocate<bool>(cross.modes[d], false);

                curr_vecs[d] = (d? cross.U: cross.V);

                const int avg =
                    (cross.modes[!d] - 1) / omp_get_max_threads() + 1;

                threads[d] = (cross.modes[!d] - 1) / avg + 1;
                bounds[d]  = Allocate<int>(threads[d] + 1);

#pragma omp parallel for
                for (int t = 0; t < threads[d]; ++t) { bounds[d][t] = t * avg; }

                bounds[d][threads[d]] = cross.modes[!d];
            }
        }

        //======================================================================
        //  Stopping_criteria
        //======================================================================
        bool Stopping_criteria(Cross & cross)
        {
            const int rank    = cross.rank;
            const int * modes = cross.modes;
                
            // Trim-to-fit U and V if criteria are satisfied.
            if (
                (params.stop_rank > 0 && rank >= params.stop_rank)
                || (
                    (std::sqrt(cross.norm) * params.tolerance)
                    >= (
                        std::fabs(max_vol())
                        * std::sqrt((modes[0] - rank) * (modes[1] - rank))
                    )
                )
            )
            {
                Reallocate<int>(cross.max_vol_inds[0], rank);
                Reallocate<int>(cross.max_vol_inds[1], rank);

                Reallocate<double>(cross.U, rank * modes[0]);
                Reallocate<double>(cross.V, rank * modes[1]);

                return true;
            }
            else { return false; }
        }
        
        //======================================================================
        //  Update
        //======================================================================
        void Update(Cross & cross)
        {
            int & rank = cross.rank;

            double mults[2]; 
            mults[1] = 1. / std::sqrt(std::fabs(max_vol()));
            mults[0] = 1. / (max_vol() * mults[1]);

            double * vecs[2];
            vecs[0] = Allocate<double>(2 * (rank + 1));
            vecs[1] = vecs[0] + rank + 1;

            const int * modes = cross.modes;

            for (int d: { 0, 1 })
            {
#pragma omp parallel for
                for (int m = 0; m < modes[d]; ++m)
                {
                    if (!approx_inds[d][m]) { curr_vecs[!d][m] *= mults[!d]; }
                    else { curr_vecs[!d][m] = 0.; }
                }

                approx_inds[d][max_vol[d]]  = true;
                cross.max_vol_inds[d][rank] = max_vol[d];

                Gemv(
                    CblasColMajor, CblasConjTrans, modes[d], rank + 1, 1.,
                    d? cross.V: cross.U, modes[d], curr_vecs[!d], 1, 0.,
                    vecs[d], 1
                );
            }

            cross.norm +=
                2. * Dot(rank, vecs[0], 1, vecs[1], 1)
                + vecs[0][rank] * vecs[1][rank];

            Free<double>(vecs[0]);

            ++rank;

            if (rank != std::min(modes[0], modes[1]))
            {
                if (rank >= params.max_rank)
                {
                    if (params.memory_strategy)
                    {
                        params.max_rank += params.rank_inc;
                    }
                    else { params.max_rank *= params.rank_inc; }

                    Reallocate<int>(cross.max_vol_inds[0], params.max_rank);
                    Reallocate<int>(cross.max_vol_inds[1], params.max_rank);

                    Reallocate<double>(cross.U, params.max_rank * modes[0]);
                    Reallocate<double>(cross.V, params.max_rank * modes[1]);
                }

                curr_vecs[0] = cross.V + rank * modes[1];
                curr_vecs[1] = cross.U + rank * modes[0];
            }
        }
    };

    cross.modes[0] = mat.Get_row_size();
    cross.modes[1] = mat.Get_col_size();

    Data data(pars, cross);

    if (pars.start_from_col) { Approx_loop<Matrix, Data, 1>(mat, data, cross); }
    else { Approx_loop<Matrix, Data, 0>(mat, data, cross); }
}

} // namespace tpbes

#endif // CROSS_HPP
