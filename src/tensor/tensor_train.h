#ifndef TENSOR_TRAIN_H
#define TENSOR_TRAIN_H

#include <cstdlib>

namespace tpbes {

struct CrossParams;

/*******************************************************************************
*
*   TensorTrainParams
*
*******************************************************************************/

// Stores parameters for the tensor train approximation.
struct TensorTrainParams
{
    double cross_tolerance;
    double tolerance;
    double comp_tolerance;

    int iters_max_count;
    int stop_rank;
    int cross_iters_max_count;

    TensorTrainParams();

    TensorTrainParams(
        const double & cross_tol,
        const double & tol,
        const double & comp_tol,
        const int      iter_max,
        const int      stop_rk,
        const int      cross_iter_max
    );

    operator const CrossParams() const;
};

/*******************************************************************************
*
*   TensorTrain
*
*******************************************************************************/

class Cross;

// Tensor train decomposition for a tensor (multidimensional array).
class TensorTrain
{
    template<typename Tensor>
    friend TensorTrain Approximate(const Tensor &, const TensorTrainParams &);

    private:

        // Links count.
        int * count;

        // Dimensionality.
        int dimension;

        // Mode sizes along each dimension.
        int * modes;

        // TT ranks.
        int * ranks;

        // TT factors (aka. carriages).
        double ** carriages;

    // Auxiliary fields:
        int max_rank;

        // Workspace for operator[].
        double * workspace;

    // Auxiliary routines:
        /// TODO /// Move to separate class
            /// // Rearranges storage order of carriages.
            /// void Rearrange();

            /// // Executes SVD-compression.
            /// void Compress(const double &);

        // Resets the object data (and deallocates it if necessary),
        // leaves the object in a nonvalid state.
        void Reset();

    public:

        ~TensorTrain() { Reset(); }

        TensorTrain();
        TensorTrain(Cross &);
        TensorTrain(const Cross &);
        TensorTrain(const TensorTrain &);

        // Performs TT cross approximation of a given tensor.
        template<typename Tensor>
        TensorTrain(const Tensor &, const TensorTrainParams &);

        // Performs TT cross approximation of a given tensor.
        template<typename Tensor>
        void Approximate(const Tensor &, const TensorTrainParams &);

        // Sets the object to the default (empty) state.
        void Empty();

        void Shallow_copy(const TensorTrain &);
        void Deep_copy(const TensorTrain &);

        const TensorTrain & operator=(const TensorTrain &);

        // Computes the Euclid norm of the TT decomposition.
        double Norm() const;

        inline int Get_dimension() const { return dimension; }

        int Get_mode(const int) const;
        inline const int * Get_modes() const { return modes; }

        int Get_rank(const int) const;
        inline const int * Get_ranks() const { return ranks; }

        const double * Get_carriage(const int) const;

        inline int Get_max_rank() const { return max_rank; }

        // Computes TT decomposition element for a given multiindex.
        double operator[](const int * inds) const;
};

// Computes TT cross-approximation
template<typename Tensor>
TensorTrain Approximate(const Tensor &, const TensorTrainParams &);

// Computes dot product of two TT decompositions.
double Dot(const TensorTrain &, const TensorTrain &, double * = NULL);

} // namespace tpbes

#include "tensor_train.hpp"

#endif // TENSOR_TRAIN_H
