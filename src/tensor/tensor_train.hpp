#ifndef TENSOR_TRAIN_HPP
#define TENSOR_TRAIN_HPP

#include <omp.h>

#include <cstdlib>
#include <iostream>

#include "../utility/blas.h"
#include "cross.h"
#include "tensor.h"

namespace tpbes {

/*******************************************************************************
*
*   TensorTrain
*
*******************************************************************************/

//==============================================================================
//  Set_carriage
//==============================================================================
static void Set_carriage(
    const int            d,
    const TensorTrain  & train,
    const bool           dir,
          Cross        & cross,
          double      *& car
)
{
    const int dim = train.Get_dimension();

    const int * modes = train.Get_modes();
    const int * ranks = train.Get_ranks();

    if (dir)
    {
        if (d < dim - 1) { Free<double>(car); car = cross.Detach_CAT(); }
        else
        {
            Reallocate<double>(car, ranks[dim - 1] * modes[dim - 1]);

            for (int r = 0; r < ranks[dim - 1]; ++r)
            {
                Copy<double>(
                    modes[dim - 1], cross.Get_RT() + r * modes[dim - 1], 1,
                    car + r, ranks[dim - 1]
                );
            }
        }
    }
    else
    {
        if (d)
        {
            Reallocate<double>(car, ranks[d + 1] * ranks[d] * modes[d]);

            for (int m = 0; m < modes[d]; ++m)
            {
                for (int r = 0; r < ranks[d + 1]; ++r)
                {
                    Copy<double>(
                        ranks[d],
                        cross.Get_AR() + ranks[d] * (ranks[d + 1] * m + r), 1,
                        car + ranks[d] * ranks[d + 1] * m + r, ranks[d + 1]
                    );
                }
            }
        }
        else
        {
            Reallocate<double>(car, ranks[1] * modes[0]);

            for (int r = 0; r < ranks[1]; ++r)
            {
                Copy<double>(
                    modes[0], cross.Get_C() + r * modes[0], 1, car + r, ranks[1]
                );
            }
        }
    }
}

//==============================================================================
//  Search_tensor_train
//==============================================================================
template<typename Tensor, typename Index, typename UnfoldSubMatrix, bool dir>
void Search_tensor_train(
    const Tensor      &  ten,
          CrossParams &  cross_pars,
          Cross       &  cross,
    const TensorTrain &  tt,
          int         *  ranks,
          double      ** cars,
          int         &  max_rank,
          Index       *  inds
)
{
    cross_pars.start_from_col = dir;

    const int dim  = ten.Get_dimension();
    const int last = (dir? dim - 1: 0);
    const int inc  = (dir? 1: -1);

    for (int d = (dir? 0: dim - 1); d != last; d += inc)
    {
        if (dir) { cross_pars.max_rank = ranks[d + 1]; }

        inds[d].Set(ranks[d + !dir]);

        cross.Approximate(UnfoldSubMatrix(d + dir, ten, inds[d]), cross_pars);

        ranks[d + dir] = cross.Get_rank();

        Set_carriage(d, tt, dir, cross, cars[d]);

        inds[d].Shuffle(cross);
    }

    Set_carriage(last, tt, dir, cross, cars[last]);

    max_rank = ranks[Iamax(dim, ranks, 1)];
}

//==============================================================================
//  Stopping_criteria
//==============================================================================
static bool Stopping_criteria(
    const TensorTrain & prev, const TensorTrain & curr, const double & tol
)
{
    const int max_rank = std::max(prev.Get_max_rank(), curr.Get_max_rank());
    const int size     = max_rank * max_rank;
    double * workspace = Allocate<double>(3 * size);

    const double prev_norm = Dot(prev, prev, workspace);

    const double diff_norm =
        prev_norm + Dot(curr, curr, workspace)
        - 2. * Dot(prev, curr, workspace);

    Free<double>(workspace);

    return diff_norm <= tol * tol * prev_norm;
}

//==============================================================================
//  Approximate
//==============================================================================
// Unified approximation function.
//
// One-dimensional case:
//   retrieves exact decomposition.
//
// Two-dimensional case:
//   performs matrix cross approximation,
//   with final columnwise storage of the factors: mode x rank.
//
// High-dimensional case:
//   performs TT-cross approximation,
//   with final columnwise storage of the factors:
//   mode x right-rank x left-rank.
template<typename Tensor>
TensorTrain Approximate(const Tensor & ten, const TensorTrainParams & pars)
{
    TensorTrain tt;

    const int dim = ten.Get_dimension();

    if (!dim) { return tt; }

    tt.dimension = dim;
    tt.modes     = Allocate<int>(dim);
    tt.ranks     = Allocate<int>(dim + 1, 1);
    tt.carriages = Allocate<double *>(dim, (double *)NULL);

    for (int d = 0; d < dim; ++d) { tt.modes[d] = ten.Get_mode(d); }

    //==========================================================================
    //  One-dimensional case
    //==========================================================================
    if (dim == 1) { Vectorize<Tensor>(ten, tt.carriages[0]); tt.max_rank = 1; }
    //==========================================================================
    //  Two-dimensional case
    //==========================================================================
    else if (dim == 2)
    { 
        // Matrix wrapper for a two-dimensional tensor,
        // works only for two-dimensional tensors.
        struct MatrixWrapper
        {
            const Tensor * tensor;

            MatrixWrapper(const Tensor & ten): tensor(&ten) {}

            inline int Get_row_size() const { return tensor->Get_mode(0); }
            inline int Get_col_size() const { return tensor->Get_mode(1); }

            // Computes matrix element for given row and column indices.
            double operator()(const int row, const int col) const
            {
                const int inds[2] = { row, col };

                return (*tensor)[inds];
            }
        };

        Cross cross(MatrixWrapper(ten), pars, false);

        tt.ranks[1] = cross.Get_rank();

        tt.carriages[0] = cross.Detach_U();
        tt.carriages[1] = cross.Detach_V();

        tt.max_rank = tt.ranks[1];
    }
    //==========================================================================
    //  High-dimensional case
    //==========================================================================
    else
    { 
        //======================================================================
        //  Index of unfolding submatrix
        //======================================================================
        struct Index
        {
            bool w;
            int  order;

            int  dim;
            const int * modes;

            bool last_flags[2];
            int  sizes[2];
            int  counts[2];
            int  capacities[2];

            int ** indices[2];

            const Index * previous;
            const Index * opposite;

            ~Index()
            {
                for (int l: { 0, 1 })
                { 
                    for (int c = 0; c < capacities[l]; ++c)
                    {
                        Free<int>(indices[l][c]);
                    }

                    Free<int *>(indices[l]);
                }
            }

            Index():
                w(0),
                order(0),
                dim(0),
                modes(NULL),
                last_flags{ false, false },
                sizes{ 0, 0 },
                counts{ 0, 0 },
                capacities{ 0, 0 },
                indices{ NULL, NULL },
                previous(NULL),
                opposite(NULL)
            {}

            // Sets index.
            void Set(
                const TensorTrain & train,
                const bool          way,
                const int           ord,
                const Index       * prev  = NULL,
                const Index       * oppos = NULL
            )
            {
                w     = way;
                order = ord + w;

                dim   = train.dimension;
                modes = train.modes;

                last_flags[0] = (order == 1);
                last_flags[1] = (order == dim - 1);

                sizes[0] = order;
                sizes[1] = dim - order;

                previous = prev;
                opposite = oppos;
            }

            // Extends the index to a given capacity.
            void Reserve(const int l, const int cap)
            {
                if (cap > capacities[l])
                {
                    Reallocate<int *>(indices[l], cap);

                    for (int c = capacities[l]; c < cap; ++c)
                    {
                        indices[l][c] = Allocate<int>(sizes[l]);
                    }

                    capacities[l] = cap;
                }
            }

            // Returns element.
            int * operator()(const int l, const int c)
            {
                return indices[l]? indices[l][c]: NULL;
            }

            const int * operator()(const int l, const int c) const
            {
                return indices[l]? indices[l][c]: NULL;
            }

            // Sets the index.
            void Set_previous(const int rank)
            {
                const int l    = !w;
                const int mode = modes[order - 1 + l];

                counts[l] = mode * rank;
                Reserve(l, counts[l]);

#pragma omp parallel for collapse(2)
                for (int m = 0; m < mode; ++m)
                {
                    for (int r = 0; r < rank; ++r)
                    {
                        int * ind = (*this)(l, m * rank + r);

                        if (!last_flags[l])
                        {
                            Copy<int>(
                                sizes[l] - 1, (*previous)(l, r), 1, ind, 1
                            );
                        }

                        ind[sizes[l] - 1] = m;
                    }
                }
            }

            // Appends the index with another one.
            void Append(const Index & inds)
            {
                for (int c = 0; c < inds.counts[w]; ++c)
                {
                    Copy<int>(
                        sizes[w], inds(w, c), 1, (*this)(w, counts[w] + c), 1
                    );
                }

                counts[w] += inds.counts[w];
            }

            // Appends the index with a randomized tail.
            void Randomize_tail()
            {   
                if (counts[w] < capacities[w])
                {
                    for (int c = counts[w]; c < capacities[w]; ++c)
                    {
                        int * ind = (*this)(w, c);

                        if (w)
                        {
                            for (int s = 0; s < sizes[w]; ++s)
                            {
                                ind[s] = rand() % modes[dim - s - 1];
                            }
                        }
                        else
                        {
                            for (int s = 0; s < sizes[w]; ++s)
                            {
                                ind[s] = rand() % modes[s];
                            }
                        }
                    }

                    counts[w] = capacities[w];
                }
            }

            // Sets the index.
            void Set_last()
            {
                counts[w] = modes[w? dim - 1: 0];
                Reserve(w, counts[w]);

                for (int c = 0; c < counts[w]; ++c) { *((*this)(w, c)) = c; }
            }

            // Sets the index.
            void Set_both()
            {
                Reserve(
                    w,
                    std::max(
                        counts[w] + (!last_flags[w]? opposite->counts[w]: 0)
                            + (!last_flags[!w]? previous->counts[w]: 0),
                        counts[!w]
                    )
                );

                if (!last_flags[w])  { Append(*opposite); }
                if (!last_flags[!w]) { Append(*previous); }

                Randomize_tail();
            }

            // Sets the index.
            void Set(const int rank)
            {
                Set_previous(rank);

                if (last_flags[w]) { Set_last(); }
                else { Set_both(); }
            }

            // Shuffles the index.
            void Shuffle(const Cross & cross)
            {
                counts[0] = counts[1] = cross.Get_rank();

                for (int l: { 0, 1 })
                {
                    if (counts[l] > capacities[l])
                    {
                        capacities[l] = counts[l];
                    }

                    int ** inds = Allocate<int *>(capacities[l], (int *)NULL);

                    for (int c = 0; c < counts[l]; ++c)
                    {
                        std::swap(
                            inds[c], indices[l][cross.Get_max_vol_ind(l, c)]
                        );
                    }

                    int m = counts[l];

                    for (int c = 0; c < capacities[l]; ++c)
                    {
                        if (indices[l][c]) { inds[m++] = indices[l][c]; } 
                    }

                    Free<int *>(indices[l]);
                    indices[l] = inds;
                }
            }
        };

        //======================================================================
        //  Unfolding matrix of a tensor
        //======================================================================
        struct UnfoldSubMatrix
        {
            // The delimiting index for grouping modes of the tensor.
            int delimiter;

            const Tensor * tensor;
            const Index  * indices;

            UnfoldSubMatrix(
                const int del, const Tensor & ten, const Index & inds
            ):
                delimiter(del), tensor(&ten), indices(&inds)
            {}

            inline int Get_row_size() const { return indices->counts[0]; }
            inline int Get_col_size() const { return indices->counts[1]; }

            // Computes matrix element for given row and column indices.
            double operator()(const int row, const int col) const
            {
                const int dim = tensor->Get_dimension();

                int inds[dim];

                Copy<int>(delimiter, (*indices)(0, row), 1, inds, 1);

                Copy<int>(
                    dim - delimiter, (*indices)(1, col), -1, inds + delimiter, 1
                );

                return (*tensor)[inds];
            }
        };

        TensorTrain b_tt;
        b_tt.Deep_copy(tt);

        Cross       cross;
        CrossParams cross_pars = pars;

        cross_pars.tolerance = pars.tolerance / (10. * sqrt(double(dim - 1)));

        Index * inds   = new Index[2 * dim];
        Index * b_inds = inds + dim;

        // Set indices.
        for (int d = 0; d < dim; ++d)
        {
            inds[d].Set(
                tt, true, d, d? inds + d - 1: NULL,
                (d < dim - 1)? b_inds + d + 1: NULL
            );

            b_inds[d].Set(
                b_tt, false, d, (d < dim - 1)? b_inds + d + 1: NULL,
                d? inds + d - 1: NULL
            );
        }

        // Iterate up to maximum count of iterations or convergence (if zero).
        for (int i = 0; !pars.iters_max_count || i < pars.iters_max_count; ++i)
        {
            Search_tensor_train<Tensor, Index, UnfoldSubMatrix, true>(
                ten, cross_pars, cross, tt, tt.ranks, tt.carriages, tt.max_rank,
                inds
            );

            if (i && Stopping_criteria(tt, b_tt, pars.tolerance)) { break; }

            Search_tensor_train<Tensor, Index, UnfoldSubMatrix, false>(
                ten, cross_pars, cross, b_tt, b_tt.ranks, b_tt.carriages,
                b_tt.max_rank, b_inds
            );

            if (Stopping_criteria(b_tt, tt, pars.tolerance)) { break; }
        }

        if (!cross_pars.start_from_col) { tt = b_tt; }

        delete[] inds;
    }

    tt.workspace = Allocate<double>(2 * tt.max_rank);

    return tt;
}

//==============================================================================
//  Approximate
//==============================================================================
template<typename Tensor>
TensorTrain::TensorTrain(const Tensor & ten, const TensorTrainParams & pars):
    count(NULL),
    dimension(0),
    modes(NULL),
    ranks(NULL),
    carriages(NULL),
    max_rank(0), 
    workspace(NULL)
{
    *this = tpbes::Approximate<Tensor>(ten, pars);
}

template<typename Tensor>
void TensorTrain::Approximate(
    const Tensor & ten, const TensorTrainParams & pars
)
{
    *this = tpbes::Approximate<Tensor>(ten, pars);
}

} // namespace tpbes

#endif // TENSOR_TRAIN_HPP
