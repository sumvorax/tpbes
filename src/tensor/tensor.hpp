#ifndef TENSOR_HPP
#define TENSOR_HPP

#include <omp.h>

#include "../utility/blas.h"
#include "../utility/memory.h"

namespace tpbes {

/*******************************************************************************
*
*   Generic functions with tensors
*
*******************************************************************************/

//==============================================================================
//  Size
//==============================================================================
template<typename Tensor>
int Size(const Tensor & ten)
{
    int res = 1;

    for (int d = 0; d < ten.Get_dimension(); ++d) { res *= ten.Get_mode(d); }

    return res;
}

//==============================================================================
//  Element
//==============================================================================
template<typename Tensor>
double Element(const Tensor & ten, const int ind)
{
    int buf[ten.Get_dimension()];

    return Element<Tensor>(ten, ind, buf);
}

template<typename Tensor>
double Element(const Tensor & ten, const int ind, int * buf)
{
    int i = ind;

    for (int d = ten.Get_dimension() - 1; d >= 0; --d)
    {
        const int mode = ten.Get_mode(d);

        buf[d] = i % mode;
        i /= mode;
    }
    
    return ten[buf];
}

//==============================================================================
//  Norm
//==============================================================================
template<typename Tensor>
double Norm(const Tensor & ten)
{
    double res = 0.;

#pragma omp parallel for reduction(+: res)
    for (int s = 0; s < Size<Tensor>(ten); ++s)
    {
        int buf[ten.Get_dimension()];

        res += std::pow(Element<Tensor>(ten, s, buf), 2.);
    }

    return std::sqrt(res);
}

//==============================================================================
//  Vectorize
//==============================================================================
template<typename Tensor>
void Vectorize(const Tensor & ten, double * res, const int res_size)
{
    const int size = (res_size < 0)? Size<Tensor>(ten): res_size;

#pragma omp parallel for
    for (int s = 0; s < size; ++s)
    {
        int buf[ten.Get_dimension()];

        res[s] = Element<Tensor>(ten, s, buf);
    }
}

/*******************************************************************************
*
*   TensorWrapper
*
*******************************************************************************/

//==============================================================================
//  Constructor
//==============================================================================
template<typename Type>
TensorWrapper<Type>::TensorWrapper(const int dim, const int mode, Type Map):
    dimension(dim), modes(NULL), size(0), Mapping(Map)
{
    modes = Allocate<int>(dimension, mode);
    size  = Size(*this);
}

template<typename Type>
TensorWrapper<Type>::TensorWrapper(const int dim, const int * mods, Type Map):
    dimension(dim), modes(NULL), size(0), Mapping(Map)
{
    modes = Allocate<int>(dimension, mods);
    size  = Size(*this);
}

//==============================================================================
//  Get_mode
//==============================================================================
template<typename Type>
int TensorWrapper<Type>::Get_mode(const int ind) const
{
    return (ind >= 0 && ind < dimension)? modes[ind]: 0;
}

} // namespace tpbes

#endif // TENSOR_HPP
