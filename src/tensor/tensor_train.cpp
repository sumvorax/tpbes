// src/tensor/tensor_train.cpp

#include "tensor_train.h"

#include <cstdlib>

namespace tpbes {

/*******************************************************************************
*
*   TensorTrainParams
*
*******************************************************************************/

TensorTrainParams::TensorTrainParams():
    cross_tolerance(1e-6),
    tolerance(1e-6),
    comp_tolerance(1e-6),
    iters_max_count(0),
    stop_rank(0),
    cross_iters_max_count(0)
{}

TensorTrainParams::TensorTrainParams(
    const double & cross_tol,
    const double & tol,
    const double & comp_tol,
    const int      iter_max,
    const int      stop_rk,
    const int      cross_iter_max
):
    cross_tolerance(cross_tol),
    tolerance(tol),
    comp_tolerance(comp_tol),
    iters_max_count(iter_max),
    stop_rank(stop_rk),
    cross_iters_max_count(cross_iter_max)
{}

TensorTrainParams::operator const CrossParams() const
{
    CrossParams cross_pars;

    cross_pars.tolerance       = cross_tolerance;
    cross_pars.iters_max_count = cross_iters_max_count;
    cross_pars.stop_rank       = stop_rank;
    cross_pars.max_rank        = 10;

    return cross_pars;
}

/*******************************************************************************
*
*   TensorTrain
*
*******************************************************************************/

/// //==============================================================================
/// //  Rearrange
/// //==============================================================================
/// void TensorTrain::Rearrange()
/// {
///     if (dimension > 2)
///     {
///         for (int d = 0; d < dimension; ++d)
///         {
///             double * car = Allocate<double>(ranks[d] * modes[d] * ranks[d + 1]);
/// 
/// #pragma omp parallel for collapse(2)
///             for (int r = 0; r < ranks[d] * ranks[d + 1]; ++r)
///             {
///                 for (int m = 0; m < modes[d]; ++m)
///                 {
///                     car[r * modes[d] + m]
///                         = carriages[d][r + m * ranks[d] * ranks[d + 1]];
///                 }
///             }
/// 
///             Free<double>(carriages[d]);
///             carriages[d] = car;
///         }
///     }
/// }
/// 
/// //==============================================================================
/// //  SVD Compress
/// //==============================================================================
/// void TensorTrain::Compress(const double & tol)
/// {
///     if (!carriages || tol < 1e-16) { return; }
/// 
///     const double max_rank = ranks[Iamax(dimension, ranks, 1)];
///     const double max_mode = modes[Iamax(dimension, modes, 1)];
/// 
///     // Will contain R-matrix.
///     double * buf = Allocate<double>(max_rank * max_rank);
///     double * car = Allocate<double>(max_rank * max_mode * max_rank); 
/// 
///     //==========================================================================
///     //  QR decompositions
///     //==========================================================================
///     for (int d = dimension - 1; d; --d)
///     {
///         const int k = std::min(modes[d] * ranks[d + 1], ranks[d]);
/// 
///         /// TODO /// Create envelope in blas
///         LAPACKE_dgeqrf(
///             CblasColMajor, modes[d] * ranks[d + 1], ranks[d], carriages[d],
///             modes[d] * ranks[d + 1], car
///         );
///         
///         // cars[d - 1] ~ modes[d - 1] x ranks[d] x ranks[d - 1].
///         // Multiply carriage[d - 1] by upper-triangular part (R-matrix).
///         for (int r = 0; r < ranks[d - 1]; ++r)
///         {
///             /// TODO /// Create envelope in blas
///             cblas_dtrmm(
///                 CblasColMajor, CblasRight, CblasUpper, CblasTrans, CblasNonUnit,
///                 modes[d - 1], ranks[d], 1., carriages[d],
///                 modes[d] * ranks[d + 1],
///                 carriages[d - 1] + r * modes[d - 1] * ranks[d], modes[d - 1]
///             );
///         }
///         
///         // For current carriage[d] genetate orthogonal part (Q-matrix).
///         /// TODO /// Create envelope in blas
///         LAPACKE_dorgqr(
///             CblasColMajor, modes[d] * ranks[d + 1], k, k, carriages[d],
///             modes[d] * ranks[d + 1], car
///         );
///     }
/// 
///     //==========================================================================
///     //  SVD decompositions
///     //==========================================================================
///     for (int d = 1; d < dimension; ++d)
///     {
///         int rank = ranks[d];
/// 
///         double sigma[rank];
///         double superb[rank - 1];
/// 
///         // car = U
///         // buf = V^T
///         Gesvd(
///             LAPACK_COL_MAJOR, 'S', 'S', ranks[d - 1] * modes[d - 1], ranks[d], 
///             carriages[d - 1], ranks[d - 1] * modes[d - 1], sigma, car,
///             ranks[d - 1] * modes[d - 1], buf, ranks[d], superb
///         );
/// 
///         const double sum = Dot(ranks[d], sigma, 1, sigma, 1);
///         double approx = sigma[rank - 1] * sigma[rank - 1];
///         
///         while (approx < sum * tol * tol / double(dimension - 1))
///         {
///             --rank;
///             approx += sigma[rank - 1] * sigma[rank - 1];
///         }
/// 
///         // buf = trunc(S V^T)
///         for (int r = 0; r < rank; ++r)
///         {
///             Scal(ranks[d], sigma[r], buf + r, ranks[d]);
///         }
/// 
///         // carriage[d - 1] = trunc(U)
///         for (int rr = 0; rr < rank; ++rr)
///         {
///             for (int rl = 0; rl < ranks[d - 1]; ++rl)
///             {
///                 Copy(
///                     modes[d - 1], car + modes[d - 1] * (rl + ranks[d - 1] * rr),
///                     1, carriages[d - 1] + modes[d - 1] * (rr + rank * rl), 1
///                 );
///             }
///         }
/// 
///         // Multiply carriage[d] by trunc(S V^T):
///         for (int r = 0; r < ranks[d + 1]; ++r)
///         {
///             Gemm(
///                 CblasColMajor, CblasNoTrans, CblasTrans, modes[d], rank,
///                 ranks[d], 1., carriages[d] + r * modes[d],
///                 modes[d] * ranks[d + 1], buf, ranks[d], 0.,
///                 car + r * modes[d] * rank, modes[d]
///             );
///         }
/// 
///         Copy(modes[d] * ranks[d + 1] * rank, car, 1, carriages[d], 1);
/// 
///         ranks[d] = rank;
///     }
/// 
///     // Shrink to fit.
///     for (int d = 0; d < dimension; ++d)
///     {
///         Reallocate<double>(carriages[d], modes[d] * ranks[d + 1] * ranks[d]);
///     }
/// 
///     Free<double>(buf);
///     Free<double>(car);
/// }

//==============================================================================
//  Reset
//==============================================================================
void TensorTrain::Reset()
{
    if (count)
    {
        if (*count == 1)
        {
            Free<int>(count);
            Free<int>(modes);
            Free<int>(ranks);

            if (carriages)
            {
                for (int d = 0; d < dimension; ++d)
                {
                    Free<double>(carriages[d]);
                }

                Free<double *>(carriages);
            }

            dimension = 0;

            max_rank  = 0;
            Free<double>(workspace);
        }
        else { --(*count); }
    }
}

//==============================================================================
//  Empty
//==============================================================================
void TensorTrain::Empty()
{
    Reset();

    count     = Allocate<int>(1, 1);
    dimension = 0;
    modes     = NULL;
    ranks     = NULL;
    carriages = NULL;

    max_rank  = 0;
    workspace = NULL;
}

//==============================================================================
//  Shallow copy
//==============================================================================
void TensorTrain::Shallow_copy(const TensorTrain & train)
{
    if (this != &train)
    {
        Reset();

        count     = train.count;
        dimension = train.dimension;
        modes     = train.modes;
        ranks     = train.ranks;
        carriages = train.carriages;

        max_rank  = train.max_rank;
        workspace = train.workspace;

        ++(*count);
    }
}

//==============================================================================
//  Deep copy
//==============================================================================
void TensorTrain::Deep_copy(const TensorTrain & train)
{
    if (this != &train)
    {
        Empty();

        dimension = train.dimension;
        modes     = Allocate<int>(dimension, train.modes);
        ranks     = Allocate<int>(dimension + 1, train.ranks);

        if (train.carriages)
        {
            carriages = Allocate<double *>(dimension);

            for (int d = 0; d < dimension; ++d)
            {
                carriages[d]
                    = Allocate<double>(
                        ranks[d] * modes[d] * ranks[d + 1], train.carriages[d]
                    );
            }
        }

        max_rank  = train.max_rank;
        workspace = Allocate<double>(2 * max_rank, train.workspace);
    }
}

//==============================================================================
//  Assignment operator
//==============================================================================
const TensorTrain & TensorTrain::operator=(const TensorTrain & train)
{
    Shallow_copy(train);
    return *this;
}

//==============================================================================
//  Constructor
//==============================================================================
TensorTrain::TensorTrain():
    count(NULL),
    dimension(0),
    modes(NULL),
    ranks(NULL),
    carriages(NULL),
    max_rank(0),
    workspace(NULL)
{
    count = Allocate<int>(1, 1);
}

TensorTrain::TensorTrain(Cross & cross):
    count(NULL),
    dimension(2),
    modes(NULL),
    ranks(NULL),
    carriages(NULL),
    max_rank(0),
    workspace(NULL)
{
    count = Allocate<int>(1, 1);
    modes = Allocate<int>(2, cross.Get_modes());

    ranks    = Allocate<int>(3, 1);
    ranks[1] = cross.Get_rank();

    carriages    = Allocate<double *>(2, (double *)NULL);
    carriages[0] = cross.Detach_U();
    carriages[1] = cross.Detach_V();

    max_rank = ranks[1];
}

TensorTrain::TensorTrain(const Cross & cross):
    count(NULL),
    dimension(2),
    modes(NULL),
    ranks(NULL),
    carriages(NULL),
    max_rank(0),
    workspace(NULL)
{
    count = Allocate<int>(1, 1);
    modes = Allocate<int>(2, cross.Get_modes());

    ranks    = Allocate<int>(3, 1);
    ranks[1] = cross.Get_rank();

    carriages    = Allocate<double *>(2, (double *)NULL);
    carriages[0] = Allocate<double>(modes[0] * ranks[1], cross.Get_U());
    carriages[1] = Allocate<double>(modes[1] * ranks[1], cross.Get_V());

    max_rank = ranks[1];
}

TensorTrain::TensorTrain(const TensorTrain & train):
    count(NULL),
    dimension(0),
    modes(NULL),
    ranks(NULL),
    carriages(NULL),
    max_rank(0),
    workspace(NULL)
{
    Shallow_copy(train);
}

//==============================================================================
//  Euclid norm
//==============================================================================
double TensorTrain::Norm() const
{
    return std::sqrt(std::fabs(Dot(*this, *this)));
}

//==============================================================================
//  Get mode
//==============================================================================
int TensorTrain::Get_mode(const int ind) const 
{
    return (modes && ind >= 0 && ind < dimension)? modes[ind]: 0;
}

//==============================================================================
//  Get rank
//==============================================================================
int TensorTrain::Get_rank(const int ind) const
{
    return (ranks && ind >= 0 && ind <= dimension)? ranks[ind]: 0;
}

//==============================================================================
//  Get carriage
//==============================================================================
const double * TensorTrain::Get_carriage(const int ind) const
{
    return (carriages && ind >= 0 && ind < dimension)? carriages[ind]: NULL;
}

//==============================================================================
//  Element access
//==============================================================================
double TensorTrain::operator[](const int * ind) const
{
    double res;

    if (dimension == 1) { res = carriages[0][*ind]; }
    else if (dimension == 2)
    {
        Gemm(
            CblasColMajor, CblasNoTrans, CblasTrans, 1, 1, ranks[1], 1.,
            carriages[0] + ind[0], modes[0], carriages[1] + ind[1], modes[1],
            0., &res, 1
        );
    }
    else
    {
        double * v = workspace;
        double * w = workspace + max_rank;

        v[0] = 1.;

        for (int d = 0; d < dimension; ++d)
        {
            // v -> w.
            Gemv(
                CblasColMajor, CblasNoTrans, ranks[d + 1], ranks[d], 1.,
                carriages[d] + ind[d] * ranks[d] * ranks[d + 1], ranks[d + 1],
                v, 1, 0., w, 1
            );

            std::swap(v, w);
        }

        res = v[0];
    }

    return res;
}

/*******************************************************************************
*
*   Dot
*
*******************************************************************************/

double Dot(
    const TensorTrain & left, const TensorTrain & right, double * workspace
)
{
    const int dim = left.Get_dimension();

    if (dim != right.Get_dimension())
    {
        std::cerr <<
            "ERROR: Dimensions of tensor trains in 'Dot' function do not match."
            << std::endl;

        exit(EXIT_FAILURE);
    }

    const int * modes = left.Get_modes();

    for (int d = 0; d < dim; ++d)
    {
        if (modes[d] != right.Get_mode(d))
        {
            std::cerr << "ERROR: Some mode sizes of tensor trains in 'Dot'"
                << " function do not match." << std::endl;

            exit(EXIT_FAILURE);
        }
    }

    const int * left_ranks  = left.Get_ranks();
    const int * right_ranks = right.Get_ranks();

    const int max_rank = std::max(left.Get_max_rank(), right.Get_max_rank());
    const int size     = max_rank * max_rank;

    double * v = workspace? workspace: Allocate<double>(3 * size);
    double * u = v + size;
    double * w = u + size;

    u[0] = 1.;

    for (int d = 0; d < dim; ++d)
    {
        w[0] = 0.;

        Copy<double>(left_ranks[d + 1] * right_ranks[d + 1], w, 0, w, 1);

        for (int m = 0; m < modes[d]; ++m)
        {
            // u -> v.
            Gemm(
                CblasColMajor, CblasNoTrans, CblasConjTrans, left_ranks[d],
                right_ranks[d + 1], right_ranks[d], 1., u, left_ranks[d],
                right.Get_carriage(d) + m * right_ranks[d] * right_ranks[d + 1],
                right_ranks[d + 1], 0., v, left_ranks[d]
            );

            // v -> w.
            Gemm(
                CblasColMajor, CblasNoTrans, CblasNoTrans, left_ranks[d + 1],
                right_ranks[d + 1], left_ranks[d], 1.,
                left.Get_carriage(d) + m * left_ranks[d] * left_ranks[d + 1],
                left_ranks[d + 1], v, left_ranks[d], 1., w, left_ranks[d + 1]
            );
        }

        std::swap(u, w);
    }

    double res = u[0];

    if (!workspace) { Free<double>(v); }

    return res;
}

} // namespace tpbes

// src/tensor/tensor_train.cpp
