// src/scheme/scheme_parameters.cpp

#include "scheme_parameters.h"

#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>

#include "../utility/utility.h"

namespace tpbes {

/*******************************************************************************

    SchemeParameters -- Finite difference integration scheme parameters 
  
*******************************************************************************/

//============================================================================//
//  Initialization
//============================================================================//
SchemeParameters::SchemeParameters():
    scheme_name(),
    thermal_flag(false),
    current_time(0.),
    final_time(0.),
    time_step(0.),
    mode(0),
    actual_mode(0),
    thresh(0.),
    tolerance(0.),
    comp_tolerance(0.),
    aggr_name(),
    aggr_count(0),
    aggr_dimensions(NULL),
    unary_frag_name(),
    binary_frag_name(),
    dir_name(),
    result_flag(false),
    time_flag(false),
    rank_flag(false),
    checkpoint_period(0),
    moment_log_period(0),
    moment_count(0),
    moment_orders(NULL)
{}

SchemeParameters::SchemeParameters(const SchemeParameters & pars):
    scheme_name(pars.scheme_name),
    thermal_flag(pars.thermal_flag),
    current_time(pars.current_time),
    final_time(pars.final_time),
    time_step(pars.time_step),
    mode(pars.mode),
    actual_mode(pars.actual_mode),
    thresh(pars.thresh),
    tolerance(pars.tolerance),
    comp_tolerance(pars.comp_tolerance),
    aggr_name(pars.aggr_name),
    aggr_count(pars.aggr_count),
    aggr_dimensions(NULL),
    unary_frag_name(pars.unary_frag_name),
    binary_frag_name(pars.binary_frag_name),
    dir_name(pars.dir_name),
    result_flag(pars.result_flag),
    time_flag(pars.time_flag),
    rank_flag(pars.rank_flag),
    checkpoint_period(pars.checkpoint_period),
    moment_log_period(pars.moment_log_period),
    moment_count(pars.moment_count),
    moment_orders(NULL)
{
    Init_array(aggr_count, pars.aggr_dimensions, aggr_dimensions);
    Init_array(moment_count, pars.moment_orders, moment_orders);
}

//============================================================================//
//  Deallocation 
//============================================================================//
SchemeParameters::~SchemeParameters()
{
    if (aggr_dimensions) { free(aggr_dimensions); aggr_dimensions = NULL; }
    if (moment_count) { free(moment_orders); moment_orders = NULL; }
}

/*******************************************************************************

    Read_config -- Read configuration file function
  
*******************************************************************************/

// extract string
static void Extract_string(std::ifstream & file, std::string & res)
{
    std::string buf;
    std::getline(file, buf);

    std::istringstream line(buf);

    std::getline(line, res, ' ');
}

// extract boolean
static bool Extract_bool(std::ifstream & file)
{
    std::string buf;
    std::getline(file, buf);

    std::istringstream line(buf);

    std::getline(line, buf, ' ');

    if (!buf.compare("yes") || !buf.compare("true")) { return true; }
    else if (!buf.compare("no") || !buf.compare("false")) { return false; }
    else
    {
        std::cerr << "Error: Some of result writing indicators "
            << "are not recognized" << std::endl;

        exit(EXIT_FAILURE);
    }
}

// extract integer
static int Extract_int(std::ifstream & file)
{
    std::string buf;
    std::getline(file, buf);

    std::istringstream line(buf);

    std::getline(line, buf, ' ');
    return std::stoi(buf);
}

// extract integer array
static void Extract_int_array(std::ifstream & file, const int count, int * res)
{
    std::string buf;
    std::getline(file, buf);

    std::istringstream line(buf);

    for (int c = 0; c < count; ++c)
    {
        std::getline(line, buf, ' ');
        res[c] = std::stoi(buf);
    }
}

// extract double
static double Extract_double(std::ifstream & file)
{
    std::string buf;
    std::getline(file, buf);

    std::istringstream line(buf);

    std::getline(line, buf, ' ');
    return std::stod(buf);
}

// read configuration file
void Read_config(const std::string & config_name, SchemeParameters & pars)
{
    std::ifstream config(config_name);

    if (config.is_open())
    {
        try
        {
            // scheme parameters
            Extract_string(config, pars.scheme_name);

            // thermal dependence
            pars.thermal_flag = Extract_bool(config);

            // where we are
            pars.current_time = Extract_double(config);
            pars.final_time = Extract_double(config); 
            pars.time_step = Extract_double(config); 

            // number of equations
            pars.mode = Extract_int(config);
            pars.actual_mode = Extract_int(config);

            pars.thresh = Extract_double(config);

            // approximation parameters
            pars.tolerance = Extract_double(config);
            pars.comp_tolerance = Extract_double(config);

            // aggregation process parameters
            Extract_string(config, pars.aggr_name);

            pars.aggr_count = Extract_int(config);
            pars.aggr_dimensions = (int *)malloc(pars.aggr_count * sizeof(int));

            Extract_int_array(config, pars.aggr_count, pars.aggr_dimensions);

            // read unary fragmentation kernel name
            Extract_string(config, pars.unary_frag_name);

            // read binary fragmentation kernel name
            Extract_string(config, pars.binary_frag_name);

            // result directory filename
            Extract_string(config, pars.dir_name);

            // result writing indicators
            pars.result_flag = Extract_bool(config);
            pars.time_flag = Extract_bool(config);
            pars.rank_flag = Extract_bool(config);

            // logging
            pars.checkpoint_period = Extract_int(config);
            pars.moment_log_period = Extract_int(config);

            pars.moment_count = Extract_int(config);
            pars.moment_orders = (int *)malloc(pars.moment_count * sizeof(int));

            Extract_int_array(config, pars.moment_count, pars.moment_orders);
        }
        catch (...)
        {
            std::cerr << "Error: "
                << "Some configuration parameters are not recognised"
                << std::endl;

            exit(EXIT_FAILURE);
        }
    }
    else
    {
        std::cerr << "Error: Can not open configuration file" << std::endl;

        exit(EXIT_FAILURE);
    }

    config.close();
}

} // namespace tpbes

// src/scheme/scheme_parameters.cpp
