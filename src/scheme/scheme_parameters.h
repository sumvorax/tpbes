#ifndef SCHEME_PARAMETERS_H
#define SCHEME_PARAMETERS_H

#include <string>

namespace tpbes {

//============================================================================//
//  SchemeParameters -- Finite difference integration scheme parameters 
//============================================================================//
struct SchemeParameters
{
    // scheme parameters
    std::string scheme_name;

    // thermal dependence indicator
    bool thermal_flag;

    double current_time;
    double final_time;
    double time_step;

    // number of equations
    int mode;
    int actual_mode;

    double thresh;

    // approximation parameters
    double tolerance;
    double comp_tolerance;

    // aggregation process parameters
    std::string aggr_name;

    int aggr_count;
    int * aggr_dimensions;

    // fragmentation parameters
    std::string unary_frag_name;
    std::string binary_frag_name;

    // result directory name
    std::string dir_name;

    // result writing flags
    bool result_flag;
    bool time_flag;
    bool rank_flag;

    // logging
    int checkpoint_period;
    int moment_log_period;

    int moment_count;
    int * moment_orders;

    SchemeParameters();
    SchemeParameters(const SchemeParameters &);

    ~SchemeParameters();
};

//============================================================================//
//  Read configuration file function 
//============================================================================//
void Read_config(const std::string &, SchemeParameters &);

} // namespace tpbes

#endif // SCHEME_PARAMETERS_H
