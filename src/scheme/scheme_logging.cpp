// src/scheme/scheme.cpp

#include "scheme.h"

#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

namespace tpbes {

/*******************************************************************************

    Scheme -- Finite difference integration scheme for TT-based operator

*******************************************************************************/

//============================================================================//
//  Save checkpoint
//============================================================================//
void Scheme::Save_checkpoint(
    const std::string & filename, const double * cons
) const
{
    std::ofstream file(filename, std::ios::out | std::ios::binary);

    if (file.is_open())
    {
        file.write((char *)&params.current_time, sizeof(double));
        file.write((char *)&params.mode, sizeof(int));
        file.write((char *)cons, params.mode * sizeof(double));
        file.write((char *)&cons[params.mode], sizeof(double));
    }
    else
    {
        std::cerr << "Error: Can not open checkpoint file on write"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    file.close();
}

//============================================================================//
//  Load checkpoint
//============================================================================//
void Scheme::Load_checkpoint(const std::string & filename, double * cons)
{
    std::ifstream file(filename, std::ios::in | std::ios::binary);

    if (file.is_open())
    {
        int tmp;

        file.read((char *)&params.current_time, sizeof(double));
        file.read((char *)&tmp, sizeof(int));
        file.read((char *)cons, params.mode * sizeof(double));
        file.read((char *)&current_temp, sizeof(double));
        cons[params.mode] = current_temp;

        if (tmp != params.mode)
        {
            std::cerr << "Error: Mode read from checkpoint file "
                << "does not coincide with the expected one" << std::endl;

            exit(EXIT_FAILURE);
        }
    }
    else
    {
        std::cerr << "Error: Can not open checkpoint file on read" << std::endl;

        exit(EXIT_FAILURE);
    }

    file.close();
}

//============================================================================//
//  Checkpoint criteria
//============================================================================//
bool Scheme::Checkpoint_criteria(const int iter) const
{
    return params.checkpoint_period && !(iter % params.checkpoint_period);
}

//============================================================================//
//  Moment logging criteria
//============================================================================//
bool Scheme::Moment_log_criteria(const int iter) const
{
    return params.moment_log_period && !(iter % params.moment_log_period);
}

//============================================================================//
//  Adaptation criteria
//============================================================================//
bool Scheme::Mode_adaptation_criteria(const double * cons) const
{
    return params.actual_mode < params.mode
        && cons[params.actual_mode - 1] > params.thresh;
}

//============================================================================//
//  Save moments
//============================================================================//
void Scheme::Save_moments(
    const std::string & filename, const double * cons
) const
{
    std::ofstream file(filename, std::ios::app);

    if (file.is_open())
    {
        file << std::setprecision(15) << std::scientific << std::fixed
            << params.current_time;
        
        int order;

        for (int c = 0; c < params.moment_count; ++c)
        {
            order = params.moment_orders[c];

            file << " "
                << (
                    (order == 1)?
                    current_mass:
                    (order? Moment(order, params.mode, cons): current_con)
                );
        }

        if (params.thermal_flag) { file << " " << current_temp; }

        file << std::endl;
    }
    else
    {
        std::cerr << "Error: Can not open moment log file" << std::endl;

        exit(EXIT_FAILURE);
    }

    file.close();
}

//============================================================================//
//  Save ranks
//============================================================================//
void Scheme::Save_ranks(const std::string & filename) const
{
    std::ofstream file(filename);

    if (file.is_open())
    {
        file << params.mode << "\nAggregation kernels TT-ranks:";

        for (int k = 0; k < aggr_operator.Get_count(); ++k)
        {
            const int dim = aggr_operator.Get_dimension(k);
            const int * ranks = aggr_operator.Export_ranks(k);

            file << "\n" << dim << ":";

            for (int d = 0; d <= dim; ++d) { file << " " << ranks[d]; }
        }

        file << "\nBinary fragmentation kernel rank:\n2: "
            << binary_frag_operator.Get_rank() << std::endl;
    }

    file.close();
}

} // namespace tpbes

// src/scheme/scheme.cpp
