// src/scheme/scheme.cpp

#include "scheme.h"

#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>

namespace tpbes {

/*******************************************************************************

    Scheme -- Finite difference integration scheme for TT-based operator

*******************************************************************************/

//============================================================================//
//  Initial conditions 
//============================================================================//
void Scheme::Initial_conditions(double * cons)
{
    initial_conditions.Get_vectorization(cons);

    initial_con = Moment(0, params.mode, cons);
    initial_mass = Moment(1, params.mode, cons);
    cons[params.mode] = initial_temp;
}

//============================================================================//
//  Right hand-sight calculation
//============================================================================//
void Scheme::operator()(const double & time, const double * arg, double * res)
{
    // res = Source(time, temp)
    source(time, params.thermal_flag? arg[params.mode]: 0., res);
    // res += Aggr_operator(arg)
    aggr_operator(arg, res);
    // res += Unary_frag_operator(arg)
    unary_frag_operator(arg, res);
    // res += Binary_frag_operator(arg)
    binary_frag_operator(arg, res);

    // final temperature update in case of thermal dependence
    if (params.thermal_flag)
    {
        res[params.mode] /= Moment(0, params.actual_mode, arg);
    }
}

//============================================================================//
//  Explicit first order Runge-Kutta integration step routine
//============================================================================//
void Scheme::Step_RK1_explicit(
    const double & time, const double & step, double * arr
)
{
    // workspace = Operator(time, arr)
    operator()(time, arr, workspace);

    // arr += step * workspace
    axpy(params.mode + params.thermal_flag, step, workspace, 1, arr, 1);
    Zeroize_negative(params.mode + params.thermal_flag, arr);
}

//============================================================================//
//  Explicit second order Runge-Kutta integration step routine
//============================================================================//
void Scheme::Step_RK2_explicit(
    const double & time, const double & step, double * arr
)
{
    const int size = params.mode + params.thermal_flag;

    double * half_res = workspace + size;

    // workspace = Operator(time, arr)
    operator()(time, arr, workspace);

    // half_res = arr
    copy(size, arr, 1, half_res, 1);

    // half_res += 0.5 * step * workspace
    axpy(size, .5 * step, workspace, 1, half_res, 1);
    Zeroize_negative(size, half_res);

    // workspace = Operator(time + 0.5 * step, half_res)
    operator()(time + .5 * step, half_res, workspace);

    // arr += step * workspace
    axpy(size, step, workspace, 1, arr, 1);
    Zeroize_negative(size, arr);
}

//============================================================================//
//  Explicit fourth order Runge-Kutta integration step routine
//============================================================================//
void Scheme::Step_RK4_explicit(
    const double & time, const double & step, double * arr
)
{
    const int size = params.mode + params.thermal_flag;

    const double b[3] = { 2., 2., 1. };
    const double c[3] = { 0.5, 0.5, 1. };

    double * inc_res = workspace + size;
    double * tmp_res = inc_res + size;

    // inc_res = Operator(time, arr)
    operator()(time, arr, inc_res);

    for (int s = 0; s < 3; ++s)
    {
        // workspace = arr
        copy(size, arr, 1, workspace, 1);

        // workspace += c[s] * step * tmp_res
        axpy(size, c[s] * step, s? tmp_res: inc_res, 1, workspace, 1);

        Zeroize_negative(size, workspace);

        // tmp_res = Operator(time + c[s] * step, workspace)
        operator()(time + c[s] * step, workspace, tmp_res);

        // inc_res += b[s] * tmp_res
        axpy(size, b[s], tmp_res, 1, inc_res, 1);
    }

    // arr += step / 6. * inc_res
    axpy(size, step / 6., inc_res, 1, arr, 1);
    Zeroize_negative(size, arr);
}

//============================================================================//
//  Wrapper for selecting integration scheme step routine
//============================================================================//
void Scheme::Integrate(const double * arg, double * res)
{
    //========================================================================//
    //  Check user settings
    //========================================================================//
    if (
        warning_flag && unary_frag_operator.Is_data_set()
        && !unary_frag_operator.Is_distr_set()
    )
    {
        std::cerr << "Warning: "
            << "Unary fragmentation term is set to trivial (\"zero\")\n"
            << "    "
            << "since mass distribution function has not been specified"
            << std::endl;
    }

    if (
        warning_flag && !unary_frag_operator.Is_data_set()
        && unary_frag_operator.Is_distr_set()
    )
    {
        std::cerr << "Warning: "
            << "Unary fragmentation term is set to trivial (\"zero\")\n"
            << "    " << "since element function has not been specified"
            << std::endl;
    }

    if (
        warning_flag && binary_frag_operator.Is_data_set()
        && !binary_frag_operator.Is_distr_set()
    )
    {
        std::cerr << "Warning: "
            << "Binary fragmentation term is set to trivial (\"zero\")\n"
            << "    "
            << "since mass distribution function has not been specified"
            << std::endl;
    }

    if (
        warning_flag && !binary_frag_operator.Is_data_set()
        && binary_frag_operator.Is_distr_set()
    )
    {
        std::cerr << "Warning: "
            << "Binary fragmentation term is set to trivial (\"zero\")\n"
            << "    " << "since element function has not been specified"
            << std::endl;
    }

    //========================================================================//
    //  Save ranks if needed
    //========================================================================//
    if (params.rank_flag) { Save_ranks("./" + params.dir_name + "/ranks"); }

    //========================================================================//
    //  Integration
    //========================================================================//
    double calculation_time = omp_get_wtime();

    if (!params.scheme_name.compare("RK1"))
    {
        Integrate<&Scheme::Step_RK1_explicit>(arg, res);
    }
    else if (!params.scheme_name.compare("RK2"))
    {
        Integrate<&Scheme::Step_RK2_explicit>(arg, res);
    }
    // params.scheme_name == "RK4"
    else { Integrate<&Scheme::Step_RK4_explicit>(arg, res); }

    calculation_time = omp_get_wtime() - calculation_time;

    //========================================================================//
    //  Save result if needed
    //========================================================================//
    if (params.result_flag)
    {
        Save_array(
            "./" + params.dir_name + "/result",
            params.mode + params.thermal_flag, res
        );
    }

    //========================================================================//
    //  Save time if needed
    //========================================================================//
    if (params.time_flag)
    {
        Save_time("./" + params.dir_name + "/time", calculation_time);
    }
}

} // namespace tpbes

// src/scheme/scheme.cpp
