// src/scheme/scheme.cpp

#include "scheme.h"

#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>

namespace tpbes {

/*******************************************************************************

    Scheme -- Finite difference integration scheme for TT-based operator

*******************************************************************************/

//============================================================================//
//  Initialization 
//============================================================================//
Scheme::Scheme(
    const SchemeParameters & scheme_pars,
    const bool warn_flag,
    const TensorTrainParameters * approx_pars
):
    warning_flag(warn_flag),
    params(scheme_pars),
    decomp_params(NULL),
    initial_con(0.),
    initial_mass(0.),
    initial_temp(0.),
    current_con(0.),
    current_mass(0.),
    current_temp(0.),
    initial_conditions(1, scheme_pars.mode),
    source(scheme_pars.mode, scheme_pars.actual_mode, scheme_pars.thermal_flag),
    aggr_operator(
        scheme_pars.mode, scheme_pars.actual_mode, scheme_pars.thermal_flag
    ),
    unary_frag_operator(scheme_pars.mode, scheme_pars.actual_mode),
    binary_frag_operator(scheme_pars.mode, scheme_pars.actual_mode),
    workspace(NULL)
{
    std::string buf;

    // check scheme name
    if (
        params.scheme_name.compare("RK1") && params.scheme_name.compare("RK2")
        && params.scheme_name.compare("RK4")
    )
    {
        std::cerr << "Error: Scheme name is not recognized" << std::endl;

        exit(EXIT_FAILURE);
    }

    // check integration time limits
    if (params.current_time > params.final_time)
    {
        std::cerr << "Error: Requested integration time limits are invalid"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    // check time step
    if (params.time_step < 0.)
    {
        std::cerr << "Error: Requested scheme time step \"" << params.time_step
            << "\" is invalid" << std::endl;

        exit(EXIT_FAILURE);
    }

    // check mode
    if (params.mode < 0)
    {
        std::cerr << "Error: Requested maximal number of equations \""
            << params.mode << "\" is invalid" << std::endl;

        exit(EXIT_FAILURE);
    }

    // check actual mode
    if (params.actual_mode <= 0 || params.actual_mode > params.mode)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested actual number of equations \""
                << params.actual_mode << "\" is treated as \"" << params.mode
                << "\"" << std::endl;
        }

        params.actual_mode = params.mode;
    }

    // check adaptation threshold
    if (params.thresh < 1e-16)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested mode adaptation threshold \""
                << params.thresh << "\" is treated as \"1e-16\"" << std::endl;
        }

        params.thresh = 1e-16;
    }

    if (params.tolerance < 1e-16 && !approx_pars)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested approximation tolerance \""
                << params.tolerance << "\" is treated as \"1e-6\""
                << std::endl;
        }

        params.tolerance = 1e-6;
    }

    // check aggregation kernel name
    if (
        params.aggr_name.compare("zero") && params.aggr_name.compare("custom")
        && params.aggr_name.compare("const")
        && params.aggr_name.compare("gensum")
        && params.aggr_name.compare("genprod")
    )
    {
        std::cerr << "Error: Aggregation kernel name is not recognized"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    // check aggregation kernel count
    if (params.aggr_count < 0)
    {
        std::cerr << "Error: Requested count of aggregation kernels \""
            << params.aggr_count << "\" is invalid" << std::endl;

        exit(EXIT_FAILURE);
    }

    // check aggregation kernel dimensions
    for (int k = 0; k < params.aggr_count; ++k)
    {
        if (params.aggr_dimensions[k] < 2)
        {
            std::cerr << "Error: Requested dimension \""
                << params.aggr_dimensions[k]
                << "\" can not be set for an aggregation kernel" << std::endl;

            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < k; ++i)
        {
            if (params.aggr_dimensions[k] == params.aggr_dimensions[i])
            {
                if (warning_flag)
                {
                    std::cerr << "Warning: Requested dimension \""
                        << params.aggr_dimensions[k]
                        << "\" duplicate is removed\n" << "    "
                        << "from aggregation kernel dimension list"
                        << std::endl;
                }

                std::swap(
                    params.aggr_dimensions[k],
                    params.aggr_dimensions[params.aggr_count - 1]
                );

                --(params.aggr_count);
                --k;
            }
        }
    }

    // check unary fragmentation kernel name
    if (
        params.unary_frag_name.compare("zero")
        && params.unary_frag_name.compare("custom")
    )
    {
        std::cerr << "Error: Unary fragmentation kernel name is not recognized"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    // check binary fragmentation kernel name
    if (
        params.binary_frag_name.compare("zero")
        && params.binary_frag_name.compare("custom")
        && params.binary_frag_name.compare("const")
        && params.binary_frag_name.compare("gensum")
        && params.binary_frag_name.compare("genprod")
    )
    {
        std::cerr << "Error: Binary fragmentation kernel name is not recognized"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    // check checkpoint period
    if (params.checkpoint_period < 0)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested checkpoint period \""
                << params.checkpoint_period << "\" is treated as \"0\""
                << std::endl;
        }

        params.checkpoint_period = 0;
    }

    // check moment logging period
    if (params.moment_log_period < 0)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested moment logging period \""
                << params.moment_log_period << "\" is treated as \"0\""
                << std::endl;
        }

        params.moment_log_period = 0;
    }

    buf = "mkdir -p ./" + params.dir_name;

    // create result directory if needed
    if (!params.dir_name.empty() && system(buf.c_str()))
    {
        std::cerr << "Error: Can not create requested result data directory"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    // check logging moments count
    if (params.moment_count < 0)
    {
        std::cerr << "Error: Requested moment logging order count \""
            << params.moment_count << "\" is invalid" << std::endl;

        exit(EXIT_FAILURE);
    }
    else if (!params.moment_log_period && params.moment_count)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested moment logging order count \""
                << params.moment_count << "\" is treated as \"0\"\n" << "    "
                << "when using \"0\" moment logging period" << std::endl;
        }

        params.moment_count = 0;
    }

    // check logging moments orders
    for (int k = 0; k < params.moment_count; ++k)
    {
        for (int i = 0; i < k; ++i)
        {
            if (params.moment_orders[k] == params.moment_orders[i])
            {
                if (warning_flag)
                {
                    std::cerr << "Warning: Requested moment order \""
                        << params.moment_orders[k]
                        << "\" duplicate is removed\n" << "    "
                        << "from moment logging list" << std::endl;
                }

                std::swap(
                    params.moment_orders[k],
                    params.moment_orders[params.moment_count - 1]
                );

                --(params.moment_count);
                --k;
            }
        }
    }

    // create checkpoint directory if needed
    buf = "mkdir -p ./" + params.dir_name + "/checkpoints";

    if (params.checkpoint_period && system(buf.c_str()))
    {
        std::cerr << "Error: Can not create checkpoint directory" << std::endl;

        exit(EXIT_FAILURE);
    }

    // create moment log file if needed
    if (params.moment_log_period)
    {
        std::ofstream file(params.dir_name + "/log");
        file.close();
    }

    // tensor train approximation parameters
    if (approx_pars)
    {
        decomp_params = new TensorTrainParameters(*approx_pars);

        if (warning_flag)
        {
            std::cerr
                << "Warning: Provided configuration approximation parameters "
                << std::endl << "were discarded due to in-code settings"
                << std::endl;
        }
    }
    else
    {
        decomp_params = new TensorTrainParameters();

        decomp_params->cross_tolerance = decomp_params->tolerance
            = params.tolerance;

        decomp_params->comp_tolerance = params.comp_tolerance;
    }

    aggr_operator.Set_approx_params(*decomp_params);
    binary_frag_operator.Set_approx_params(*decomp_params);

    // allocate workspace
    if (!params.scheme_name.compare("RK1"))
    {
        workspace = (double *)malloc((params.mode + 1) * sizeof(double));
    }
    else if (!params.scheme_name.compare("RK2"))
    {
        workspace = (double *)malloc(2 * (params.mode + 1) * sizeof(double));
    }
    // params.scheme_name == "RK4"
    else
    {
        workspace = (double *)malloc(3 * (params.mode + 1) * sizeof(double));
    }
}

//============================================================================//
//  Reset integration time limits and time step
//============================================================================//
void Scheme::Reset(
    const double & curr_time,
    const double & fin_time,
    const double & step,
    const int check_period,
    const int mom_log_period
)
{
    // check integration time limits
    if (curr_time > fin_time)
    {
        std::cerr << "Error: Requested integration time limits are invalid"
            << std::endl;

        exit(EXIT_FAILURE);
    }

    // check time step
    if (step < 0.)
    {
        std::cerr << "Error: Requested scheme time step \"" << step
            << "\" is invalid" << std::endl;

        exit(EXIT_FAILURE);
    }

    params.current_time = curr_time;
    params.final_time = fin_time;
    params.time_step = step;

    // check checkpoint period
    if (check_period < 0)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested checkpoint period \""
                << check_period << "\" is treated as \"0\"" << std::endl;
        }

        params.checkpoint_period = 0;
    }
    else { params.checkpoint_period = check_period; }

    // check moment logging period
    if (mom_log_period < 0)
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Requested moment logging period \""
                << mom_log_period << "\" is treated as \"0\"" << std::endl;
        }

        params.moment_log_period = 0;
    }
    else { params.moment_log_period = mom_log_period; }
}

//============================================================================//
//  Deallocation 
//============================================================================//
Scheme::~Scheme()
{
    if (decomp_params) { delete decomp_params; decomp_params = NULL; }
    if (workspace) { free(workspace); workspace = NULL; }
}

//============================================================================//
//  Set actual mode 
//============================================================================//
void Scheme::Set_actual_mode(const int act_mod)
{
    params.actual_mode = std::min(act_mod, params.mode);

    source.Set_actual_mode(act_mod);
    aggr_operator.Set_actual_mode(act_mod);
    unary_frag_operator.Set_actual_mode(act_mod);
    binary_frag_operator.Set_actual_mode(act_mod);
}

//============================================================================//
//  Set thermal dependence
//============================================================================//
void Scheme::Set_kinetic_factor(UnIntUnDoubleElement Elem)
{
    if (params.thermal_flag) { aggr_operator.Set_kinetic_factor(Elem); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided kinetic factor function is discarded\n"
            << "    " << "due to requested thermal dependence option"
            << std::endl;
    }
}

void Scheme::Set_kinetic_factor(UnIntUnDoubleParamElement Elem, void * pars)
{
    if (params.thermal_flag) { aggr_operator.Set_kinetic_factor(Elem, pars); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided kinetic factor function "
            << "and parameters are discarded\n" << "    "
            << "due to requested thermal dependence option"
            << std::endl;
    }
}

void Scheme::Set_thermal_factor(UnIntUnDoubleElement Elem)
{
    if (params.thermal_flag) { aggr_operator.Set_thermal_factor(Elem); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided thermal factor function is discarded\n"
            << "    " << "due to requested thermal dependence option"
            << std::endl;
    }
}

void Scheme::Set_thermal_factor(UnIntUnDoubleParamElement Elem, void * pars)
{
    if (params.thermal_flag) { aggr_operator.Set_thermal_factor(Elem, pars); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided thermal factor function "
            << "and parameters are discarded\n" << "    "
            << "due to requested thermal dependence option"
            << std::endl;
    }
}

//============================================================================//
//  Set initial conditions data
//============================================================================//
void Scheme::Set_initial_condition_data(UnIntElement Elem)
{
    initial_conditions.Set_element(Elem);
}

void Scheme::Set_initial_condition_data(UnIntParamElement Elem, void * pars)
{
    initial_conditions.Set_element(Elem, pars);
}

void Scheme::Set_initial_temperature(const double & temp)
{
    if (params.thermal_flag) { initial_temp = temp; }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided initial temperature is discarded\n"
            << "    " << "due to requested thermal dependence option"
            << std::endl;
    }
}

//============================================================================//
//  Set source data
//============================================================================//
void Scheme::Set_source_data(UnIntParamElement Elem, void * pars)
{
    source.Set_element(Elem, pars);
}

void Scheme::Set_source_data(UnIntUnDoubleElement Elem)
{
    source.Set_element(Elem);
} 

void Scheme::Set_source_data(UnIntUnDoubleParamElement Elem, void * pars)
{
    source.Set_element(Elem, pars);
}

//============================================================================//
//  Set thermal source data
//============================================================================//
void Scheme::Set_thermal_source_data(UnIntElement Elem)
{
    if (params.thermal_flag) { source.Set_thermal_element(Elem); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided thermal source function is discarded\n"
            << "    " << "due to requested thermal dependence option"
            << std::endl;
    }
}

void Scheme::Set_thermal_source_data(UnIntParamElement Elem, void * pars)
{
    if (params.thermal_flag) { source.Set_thermal_element(Elem, pars); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided thermal source function "
            << "and parameters are discarded\n" << "    "
            << "due to requested thermal dependence option"
            << std::endl;
    }
}

void Scheme::Set_thermal_source_data(UnIntUnDoubleElement Elem)
{
    if (params.thermal_flag) { source.Set_thermal_element(Elem); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided thermal source function is discarded\n"
            << "    " << "due to requested thermal dependence option"
            << std::endl;
    }
} 

void Scheme::Set_thermal_source_data(
    UnIntUnDoubleParamElement Elem, void * pars
)
{
    if (params.thermal_flag) { source.Set_thermal_element(Elem, pars); }
    else if (warning_flag)
    {
        std::cerr << "Warning: Provided thermal source function "
            << "and parameters are discarded\n" << "    "
            << "due to requested thermal dependence option"
            << std::endl;
    }
}

//============================================================================//
//  Set aggregation operator data
//============================================================================//
void Scheme::Set_aggr_data(void * const * pars)
{
    if (!params.aggr_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: Provided custom parameters are discarded\n"
            << "    " << "when setting requested trivial (\"zero\")"
            << " aggregation kernel" << std::endl;
    }
    else if (!params.aggr_name.compare("const"))
    {
        aggr_operator.Set_data<ConstTensor>(
            params.aggr_count, params.aggr_dimensions, pars
        );
    }
    else if (!params.aggr_name.compare("gensum"))
    {
        aggr_operator.Set_data<GenSumTensor>(
            params.aggr_count, params.aggr_dimensions, pars
        );
    }
    else if (!params.aggr_name.compare("genprod"))
    {
        aggr_operator.Set_data<GenProdTensor>(
            params.aggr_count, params.aggr_dimensions, pars
        );
    }
    else if (!params.aggr_name.compare("custom"))
    {
        std::cerr << "Error: Can not set requested custom aggregation kernel\n"
            << "    " << "without provided element function" << std::endl;

        exit(EXIT_FAILURE);
    }
}

void Scheme::Set_aggr_data(GenIntElement Elem)
{
    if (!params.aggr_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: "
            << "Provided custom element function is discarded\n" << "    "
            << "when setting requested trivial (\"zero\") aggregation kernel"
            << std::endl;
    }
    else if (!params.aggr_name.compare("custom"))
    {
        aggr_operator.Set_data(params.aggr_count, params.aggr_dimensions, Elem);
    }
    else
    {
        if (warning_flag)
        {
            std::cerr << "Warning: "
                << "Provided custom element function is discarded\n" << "    "
                << "when setting requested predefined (\"" << params.aggr_name
                << "\") aggregation kernel" << std::endl;
        }

        Set_aggr_data();
    }
}

void Scheme::Set_aggr_data(GenIntParamElement Elem, void * const * pars)
{
    if (!params.aggr_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: Provided custom element function "
            << "and parameters are discarded\n" << "    "
            << "when setting requested trivial (\"zero\") aggregation kernel"
            << std::endl;
    }
    else if (!params.aggr_name.compare("custom"))
    {
        aggr_operator.Set_data(
            params.aggr_count, params.aggr_dimensions, Elem, pars
        );
    }
    else
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Provided custom element function "
                << "and parameters are discarded\n" << "    "
                << "when setting requested predefined (\"" << params.aggr_name
                << "\") aggregation kernel" << std::endl;
        }

        Set_aggr_data();
    }
}

//============================================================================//
//  Set unary fragmentation operator data
//============================================================================//
void Scheme::Set_unary_frag_data(UnIntElement Elem)
{
    if (!params.unary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: "
            << "Provided custom element function is discarded\n" << "    "
            << "when setting requested trivial (\"zero\") "
            << "unary fragmentation kernel" << std::endl;
    }
    else { unary_frag_operator.Set_data(Elem); }
}

void Scheme::Set_unary_frag_data(UnIntParamElement Elem, void * pars)
{
    if (!params.unary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: Provided custom element function "
            << "and parameters are discarded\n" << "    "
            << "when setting requested trivial (\"zero\") "
            << "unary fragmentation kernel" << std::endl;
    }
    else { unary_frag_operator.Set_data(Elem, pars); }
}

//============================================================================//
//  Set unary fragmentation operator mass distribution
//============================================================================//
void Scheme::Set_unary_frag_distr(UnIntElement Elem) 
{
    if (!params.unary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: "
            << "Provided custom mass distribution function is discarded\n"
            << "    " << "when setting requested trivial (\"zero\") "
            << "unary fragmentation kernel" << std::endl;
    }
    else { unary_frag_operator.Set_distr(Elem); }
}

void Scheme::Set_unary_frag_distr(UnIntParamElement Elem, void * pars)
{
    if (!params.unary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: Provided custom mass distribution function "
            << "and parameters are discarded\n" << "    "
            << "when setting requested trivial (\"zero\") "
            << "unary fragmentation kernel" << std::endl;
    }
    else { unary_frag_operator.Set_distr(Elem, pars); }
}

//============================================================================//
//  Set binary fragmentation operator data
//============================================================================//
void Scheme::Set_binary_frag_data(BinIntElement Elem)
{
    if (!params.binary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: "
            << "Provided custom element function is discarded\n" << "    "
            << "when setting requested trivial (\"zero\") "
            << "binary fragmentation kernel" << std::endl;
    }
    else if (!params.binary_frag_name.compare("custom"))
    {
        binary_frag_operator.Set_data(Elem);
    }
    else
    {
        if (warning_flag)
        {
            std::cerr << "Warning: "
                << "Provided custom element function is discarded\n" << "    "
                << "when setting requested predefined (\""
                << params.binary_frag_name << "\") binary fragmentation kernel"
                << std::endl;
        }

        Set_binary_frag_data();
    }
}

void Scheme::Set_binary_frag_data(BinIntParamElement Elem, void * pars)
{
    if (!params.binary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: Provided custom element function "
            << "and parameters are discarded\n" << "    "
            << "when setting requested trivial (\"zero\") "
            << "binary fragmentation kernel" << std::endl;
    }
    else if (!params.binary_frag_name.compare("custom"))
    {
        binary_frag_operator.Set_data(Elem, pars);
    }
    else
    {
        if (warning_flag)
        {
            std::cerr << "Warning: Provided custom element function "
                << "and parameters are discarded\n" << "    "
                << "when setting requested predefined (\""
                << params.binary_frag_name << "\") binary fragmentation kernel"
                << std::endl;
        }

        Set_binary_frag_data();
    }
}

/// void Scheme::Set_binary_frag_data(void * pars)
/// {
///     if (!params.binary_frag_name.compare("zero") && warning_flag)
///     {
///         std::cerr << "Warning: Provided custom parameters are discarded\n"
///             << "    " << "when setting requested trivial (\"zero\") "
///             << "binary fragmentation kernel" << std::endl;
///     }
///     else if (!params.binary_frag_name.compare("const"))
///     {
///         binary_frag_operator.Set_data<ConstTensor>(pars);
///     }
///     else if (!params.binary_frag_name.compare("gensum"))
///     {
///         binary_frag_operator.Set_data<GenSumTensor>(pars);
///     }
///     else if (!params.binary_frag_name.compare("genprod"))
///     {
///         binary_frag_operator.Set_data<GenProdTensor>(pars);
///     }
///     else if (!params.binary_frag_name.compare("custom"))
///     {
///         std::cerr << "Error: "
///             << "Can not set requested custom binary fragmentation kernel\n"
///             << "    " << "without provided element function" << std::endl;
/// 
///         exit(EXIT_FAILURE);
///     }
/// }

//============================================================================//
//  Set binary fragmentation operator mass distribution
//============================================================================//
void Scheme::Set_binary_frag_distr(BinIntElement Elem) 
{
    if (!params.binary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: "
            << "Provided custom mass distribution function is discarded\n"
            << "    " << "when setting requested trivial (\"zero\") "
            << "binary fragmentation kernel" << std::endl;
    }
    else { binary_frag_operator.Set_distr(Elem); }
}

void Scheme::Set_binary_frag_distr(BinIntParamElement Elem, void * pars)
{
    if (!params.binary_frag_name.compare("zero") && warning_flag)
    {
        std::cerr << "Warning: Provided custom mass distribution function "
            << "and parameters are discarded\n" << "    "
            << "when setting requested trivial (\"zero\") "
            << "binary fragmentation kernel" << std::endl;
    }
    else { binary_frag_operator.Set_distr(Elem, pars); }
}

} // namespace tpbes

// src/scheme/scheme.cpp
