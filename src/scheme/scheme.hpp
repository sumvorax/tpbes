#ifndef SCHEME_HPP
#define SCHEME_HPP

#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include "../blas/blas.h"
#include "../utility/utility.h"

namespace tpbes {

/*******************************************************************************

    Scheme -- Finite difference integration scheme for TT-based operator

*******************************************************************************/

// finite difference time-integration function
template<Scheme::SchemeStep Step>
void Scheme::Integrate(const double * arg, double * res)
{
    std::string checkpoint_filename;
    const std::string moment_log_filename = "./" + params.dir_name + "/log";

    const double dzero = 0.;

    int i = 0;
    params.current_time = 0.;

    // copy provided initial conditions to workspace
    copy(params.mode + params.thermal_flag, arg, 1, res, 1);

    // set actual mode for operators
    Set_actual_mode(params.actual_mode);

    // integration process
    for ( ; i < int(rint(params.final_time / params.time_step)); ++i) 
    {
        // checkpoint
        if (Checkpoint_criteria(i))
        {
            // come back due to mass conservation law violation
            if (i && Mode_adaptation_criteria(res))
            {
                copy(params.mode, &dzero, 0, workspace, 1);

                Load_checkpoint(checkpoint_filename, res);

                i = int(rint(params.current_time / params.time_step));

                Set_actual_mode(2 * params.actual_mode);
            }
            // save intermediate result and continue
            else
            {
                checkpoint_filename
                    = "./" + params.dir_name + "/checkpoints/_"
                        + std::to_string(params.current_time);

                Save_checkpoint(checkpoint_filename, res);
            }
        }

        // moments & temperature logging
        if (Moment_log_criteria(i))
        {
            current_con = Moment(0, params.mode, res);
            current_mass = Moment(1, params.mode, res);

            if (params.thermal_flag) { current_temp = res[params.mode]; }

            Save_moments(moment_log_filename, res);
        }

        // integration step
        (this->*Step)(params.current_time, params.time_step, res);

        // update current time
        params.current_time = (i + 1) * params.time_step;
    }

    // final checkpoint
    if (Checkpoint_criteria(i))
    {
        checkpoint_filename
            = "./" + params.dir_name + "/checkpoints/_"
                + std::to_string(params.current_time);

        Save_checkpoint(checkpoint_filename, res);
    }

    // final moments & temperature logging
    if (Moment_log_criteria(i))
    {
        current_con = Moment(0, params.mode, res);
        current_mass = Moment(1, params.mode, res);

        if (params.thermal_flag) { current_temp = res[params.mode]; }

        Save_moments(moment_log_filename, res);
    }
}

} // namespace tpbes

#endif // SCHEME_HPP
