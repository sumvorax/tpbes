#ifndef SCHEME_H
#define SCHEME_H

#include <string>

#include "../operator/aggr_operator.h"
#include "../operator/binary_frag_operator.h"
#include "../operator/source.h"
#include "../operator/unary_frag_operator.h"
#include "../tensor/tensor.h"
#include "../utility/definitions.h"
#include "scheme_parameters.h"

namespace tpbes {

//============================================================================//
//  Scheme -- Finite difference integration scheme for TT-based operators
//============================================================================//
class Scheme
{
    // integration step function pointer type
    typedef void (Scheme::* SchemeStep)(
        const double &, const double &, double *
    );

    private:

        // warning regime indicator
        bool warning_flag;

        // scheme parameters
        SchemeParameters params;

        // tensor decomposition parameters
        TensorTrainParameters * decomp_params;

        // initial moments & temperature
        double initial_con;
        double initial_mass;
        double initial_temp;

        // initial moments & temperature
        double current_con;
        double current_mass;
        double current_temp;

        // initial conditions
        WrapTensor initial_conditions;

        // operators
        Source source;
        AggrOperator aggr_operator;
        UnaryFragOperator unary_frag_operator;
        BinaryFragOperator binary_frag_operator;

        // auxiliary memory
        double * workspace;

        // set actual mode
        void Set_actual_mode(const int);

        // checkpoint input/output
        void Save_checkpoint(const std::string &, const double *) const;
        void Load_checkpoint(const std::string &, double *);

        // check checkpoint criteria
        bool Checkpoint_criteria(const int) const;

        // check moment logging criteria
        bool Moment_log_criteria(const int) const;

        // check adaptation criteria
        bool Mode_adaptation_criteria(const double *) const;

        // compute right hand-side
        void operator()(const double &, const double *, double *);

        // explicit Runge-Kutta integration step routines
        void Step_RK1_explicit(const double &, const double &, double *);
        void Step_RK2_explicit(const double &, const double &, double *);
        void Step_RK4_explicit(const double &, const double &, double *);

        // finite difference time-integration function
        template<Scheme::SchemeStep Step>
        void Integrate(const double *, double *);

    public:

        Scheme(
            const SchemeParameters &,
            const bool = true,
            const TensorTrainParameters * = NULL
        );

        ~Scheme();

        // reset integration time limits and time step
        void Reset(
            const double &, const double &, const double &, const int, const int
        );

        // set warning flag
        inline void Set_warning_flag(const bool flag) { warning_flag = flag; }

        // set thermal dependence
        void Set_kinetic_factor(UnIntUnDoubleElement);
        void Set_kinetic_factor(UnIntUnDoubleParamElement, void *);

        void Set_thermal_factor(UnIntUnDoubleElement); 
        void Set_thermal_factor(UnIntUnDoubleParamElement, void *); 

        // set initial condition data
        void Set_initial_condition_data(UnIntElement);
        void Set_initial_condition_data(UnIntParamElement, void *); 

        void Set_initial_temperature(const double &);

        // set source data
        void Set_source_data(UnIntElement Elem) { source.Set_element(Elem); } 
        void Set_source_data(UnIntParamElement, void *);
        void Set_source_data(UnIntUnDoubleElement);
        void Set_source_data(UnIntUnDoubleParamElement, void *); 

        void Set_thermal_source_data(UnIntElement);
        void Set_thermal_source_data(UnIntParamElement, void *);
        void Set_thermal_source_data(UnIntUnDoubleElement);
        void Set_thermal_source_data(UnIntUnDoubleParamElement, void *); 

        // set aggregation operator data
        void Set_aggr_data(GenIntElement); 
        void Set_aggr_data(GenIntParamElement, void * const *); 

        /// TODO /// Change to -> templated Set_aggr_data(const int, Tensor *);
        // void Set_aggr_data(void * const * = NULL); 

        // set unary fragmentation operator data
        void Set_unary_frag_data(UnIntElement); 
        void Set_unary_frag_data(UnIntParamElement, void *); 

        // set unary fragmentation operator mass distribution
        void Set_unary_frag_distr(UnIntElement); 
        void Set_unary_frag_distr(UnIntParamElement, void *); 

        // set binary fragmentation operator data
        void Set_binary_frag_data(BinIntElement); 
        void Set_binary_frag_data(BinIntParamElement, void *); 

        template<typename Tensor>
        void Set_binary_frag_data(Tensor &); 

        // set binary fragmentation operator mass distribution
        void Set_binary_frag_distr(BinIntElement); 
        void Set_binary_frag_distr(BinIntParamElement, void *); 

        template<typename Tensor>
        void Set_binary_frag_distr(Tensor &); 

        // get initial moments & temperature
        inline double Get_initial_con() const { return initial_con; }
        inline double Get_initial_mass() const { return initial_mass; }
        inline double Get_initial_temp() const { return initial_temp; }

        // get current moments & temperature
        inline double Get_current_con() const { return current_con; }
        inline double Get_current_mass() const { return current_mass; }
        inline double Get_current_temp() const { return current_temp; }

        // save moments (& temperature if applicable)
        void Save_moments(const std::string &, const double *) const;

        // save operator ranks
        void Save_ranks(const std::string &) const;

        // compute initial conditions
        void Initial_conditions(double *);

        // unified finite difference time-integration function
        void Integrate(const double *, double *);
};

} // namespace tpbes

#include "scheme.hpp"

#endif // SCHEME_H
