#ifndef BRILL_DEFINITIONS_H
#define BRILL_DEFINITIONS_H

#include <cmath>
#include <iostream>

using namespace std;

//============================================================================//
//  Initial condition
//============================================================================//
double Initial_condition(const int size) { return size? 0.: 1.; }

double Initial_temperature() { return 1.; }

//============================================================================//
//  Source
//============================================================================//
double Source(const int size)
{
    // return size? 0.: 1.;
    return 0.;
}

double Thermal_source(const int size)
{
    // return size? 0.: 1.;
    return 0.;
}

//============================================================================//
//  Ternary coefficient factor
//============================================================================//
double Factor(const double & alpha, const double & u0)
{
    const double alpha_inverse = 1. / alpha;

    return tgamma(alpha_inverse + 1) * pow(0.5 / u0, alpha_inverse);
}

//============================================================================//
//  Kinetic factor -- in fornt of kinetic coefficient
//============================================================================//
/// TODO /// pass alpha as an additional parameter (via 3rd argument: void *)
double Kinetic_factor(const int arity, const double & temperature)
{
    return 1.;

    if (arity == 2) 
    {
        // return  2.*sqrt(2.) * pow(temperature, 0.5); // for constant kernel validation (test 1)
        return pow(temperature, 0.5);// bin + triple const kernels (test 2)
        //return  0.5 * pow(temperature, 0.5); // for ballistic kernel validation (modelling & research)
    }
    if (arity == 3)
    {
        // in general T^{0.5 + 1/alpha}; we take alpha=2.5
        //return pow(temperature, 0.9); // bin + triple const kernels (test 1)
        return pow(temperature, 0.9); // bin + triple const kernels (test 2)
        //return 1. / 6. * pow(temperature, 0.9);
    }
    else { return 0.; }
}

//============================================================================//
//  Thermal factor -- in fornt of thermal coefficient
//============================================================================//
double Thermal_factor(const int arity, const double & temperature)
{
    return 2. * temperature;

    // generic 
    if (arity == 2)
    {
        //return 8.0 * sqrt(2.0) / 3.0 * pow(temperature, 1.5);// for constant kernel validation (test 1)
        return 2.0 * pow(temperature, 1.5);// bin + triple const kernel (test 2)
        //return 2.0 / 3.0 * pow(temperature, 1.5);
    }
    if (arity == 3)
    {
        // in general T^{1.5 + 1/alpha}; we take alpha=2.5        
        //return 2.0 * pow(temperature, 1.9); // bin + triple const kernels (test 1)
        return 2.0 * pow(temperature, 1.9); // bin + triple const kernels (test 2)
        //return 1.3 / 3. * pow(temperature, 1.9);
    }
    else { return 0.; }
}

//============================================================================//
//  Ternary coefficient term
//============================================================================//
double Term(const double & i, const double & j, const double & k)
{
    const double tmp = cbrt(i) + cbrt(j);

    return pow((tmp + cbrt(k)) * tmp, 2.) * sqrt(1. / (i + j) + 1. / k);
}

//============================================================================//
//  Quartic coefficient term
//============================================================================//
double Term(
    const double & i, const double & j, const double & k, const double & l
)
{
    const double tmp1 = cbrt(i) + cbrt(j);
    const double tmp2 = tmp1 + cbrt(k);
    const double tmp3 = tmp2 + cbrt(l);
    
    return pow(tmp1 * tmp2 * tmp3, 2.) * sqrt(i + j + k + l) / sqrt(i + j + k)
        / sqrt(l);
}

//============================================================================//
//  Aggregation coefficient
//============================================================================//
// pars[0] ~ factor
double Aggr_coefficient(const int arity, const int * sizes, void * pars)
{
    if (arity == 2)
    {
        return 1.0; // constant validation kernel (test 1, test 2)
        const double i = sizes[0] + 1.;
        const double j = sizes[1] + 1.;
        return pow(cbrt(i) + cbrt(j), 2.) * sqrt((i + j) / (i * j));
    }
    else if (arity == 3)
    {
        const double i = sizes[0] + 1.;
        const double j = sizes[1] + 1.;
        const double k = sizes[2] + 1.;
        return 1.0; // constant validation kernel (test 1, test 2)
        return *((double *)pars)
            * 1e-1 * (Term(i, j, k) + Term(i, k, j) + Term(k, j, i));
    }
    else if (arity == 4)
    {
        const double i = sizes[0] + 1.;
        const double j = sizes[1] + 1.;
        const double k = sizes[2] + 1.;
        const double l = sizes[3] + 1.;

        return 1e-6 * (
            Term(i, j, k, l) + Term(i, j, l, k) + Term(i, l, k, j)
            + Term(l, k, j, i)
        );
    }
    else { return 0.; }
}

#endif // BRILL_DEFINITIONS_H
