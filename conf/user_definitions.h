#ifndef USER_DEFINITIONS_H
#define USER_DEFINITIONS_H

#include <cmath>

using namespace std;

/*******************************************************************************

    User Definitions for Tensor-based Population Balance Equations Solver

********************************************************************************

/// TODO /// Problem description
    dn_k(t) / dt     = S_k + A^2_k + A^3_k + ... + A^D_k + F^1_k + F^2_k;
    d[n(t)T(t)] / dt = 

    n(t) = n_1(t) + n_2(t) + ... + n_N(t)

*******************************************************************************/

/*******************************************************************************

    Kinetic factor

********************************************************************************

    in: const int arity;
        the dimensionality of array (kernel)
        C = [C_{i_1, i_2, ..., i_arity}]
        of kinetic coefficients C_{i_1, i_2, ..., i_arity}

    in: const double &
        current temperature of particles

    return: double
        thermal-dependent factor in front of kinetic coefficient

*******************************************************************************/

double Kinetic_factor(const int arity, const double & temperature)
{
    return 1.;
}

/*******************************************************************************

    Thermal factor

********************************************************************************

    in: const int arity;
        the dimensionality of array (kernel)
        C = [C_{i_1, i_2, ..., i_arity}]
        of kinetic coefficients C_{i_1, i_2, ..., i_arity}

    in: const double &
        current temperature of particles

    return: double
        thermal-dependent factor in front of temperature coefficient

*******************************************************************************/

double Thermal_factor(const int arity, const double & temperature)
{
    return 0.;
}

/*******************************************************************************

    Initial condition

********************************************************************************

    in: const int size;
        size of particles

    return: double
        the initial value for concentration of particles of given size

*******************************************************************************/

double Initial_condition(const int size)
{
    return size? 0.: 1.;
}

/*******************************************************************************

    Initial temperature

********************************************************************************

    return: double
        the initial value for temperature of particles

*******************************************************************************/

double Initial_temperature()
{
    return 1.;
}

/*******************************************************************************

    Source / sink of particles

********************************************************************************

    in: const int size;
        size of particles

    return: double
        the value for concentration source / sink of particles of given size

*******************************************************************************/

double Source(const int size)
{
    return 0.;
}

double Thermal_source(const int size)
{
    return size? 0.: 1.;
}

/*******************************************************************************

    Aggregation kinetic coefficient

********************************************************************************

    in: const int arity;
        the dimensionality of array (kernel)
        C = [C_{i_1, i_2, ..., i_arity}]
        of kinetic coefficients C_{i_1, i_2, ..., i_arity}

    in: const int sizes[arity];
        the array of sizes: i_1, i_2, ..., i_arity

    return: double
        the value of kinetic coefficient C_{i_1, i_2, ..., i_arity},
        which is the frequency of aggregation of particles with sizes
        i_1, i_2, ..., i_arity

*******************************************************************************/

double Aggr_coefficient(const int arity, const int * sizes)
{
    return 0.;
}

/*******************************************************************************

    Unary fragmentation kinetic coefficient

********************************************************************************

    in: const int size;
        size of particles

    return: double
        the value of kinetic coefficient A_{size}, which is the frequency
        of spontaneous unary fragmentation for particles with given size

*******************************************************************************/

double Unary_frag_coefficient(const int size)
{
    return 0.;
}

/*******************************************************************************

    Unary fragmentation mass distribution coefficient

********************************************************************************

    in: const int size;
        size of particles

    return: double
        the value of coefficient phi_{size},
        which is the mass distribution coefficient of particles with given size,
        forming in process of unary fragmentation

*******************************************************************************/

double Unary_frag_mass_distribution(const int size)
{
    return 0.;
}

/*******************************************************************************

    Binary fragmentation kinetic coefficient

********************************************************************************

    in: const int row;
        row index of matrix (kernel) B = [B_{row, col}]
        of kinetic coefficients B_{row, col};

    in: const int col;
        column index of matrix (kernel) B = [B_{row, col}]
        of kinetic coefficients B_{row, col};

    return: double
        the value of kinetic coefficient B_{row, col},
        which is the fragmentation frequency of particles with size (row + col)
        formed due to binary collision of particles with sizes (row) and (col)

*******************************************************************************/

double Binary_frag_coefficient(const int row, const int col)
{
    return 0.;
}

/*******************************************************************************

    Unary fragmentation mass distribution coefficient

********************************************************************************

    in: const int size;
        size of particles

    return: double
        the value of coefficient psi_{size},
        which is the mass distribution coefficient of particles with given size,
        forming in process of binary fragmentation

*******************************************************************************/

double Binary_frag_mass_distribution(const int size)
{
    return 0.;
}

#endif // USER_DEFINITIONS_H
