// test/test_tensor.cpp

// #include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

#include "../src/tensor/tensor.h"
#include "../src/utility/definitions.h"
#include "../src/utility/blas.h"
#include "../src/utility/memory.h"

#define TENSOR_SIZE 1000
#define EPS         1e-15

using namespace tpbes;

double Element(const int, const int * inds)
{
    return 1. / (((inds[0] + 1) * 10 + inds[1] + 1) * 10 + inds[2] + 1);
}

//============================================================================//
//  Basic tensor test
//============================================================================//
int main(int argc, char ** argv)
{
    const int modes[3] = { 7, 11, 13 };

    TensorWrapper<TensorElementMapping> ten(3, modes, Element);

    const int size = ten.Get_size();
    double * res   = Allocate<double>(size);

    ten.Vectorize(res);

    std::ofstream file("test_log", std::ios::app);

    bool check = true;

    if (file.is_open())
    {
        file << "========================================"
             << "========================================" << std::endl;

        for (int s = 0; s < size; ++s)
        {
            if (std::fabs(res[s] - ten[s]) > EPS)
            {
                std::cout << "First mismatch: ten[" << s << "]: "
                    << res[s] << std::endl << "Reference value: "
                    << ten[s] << std::endl;

                check = false;

                break;
            }
        }

        if (check) { file << "PASSED: Basic tensor test." << std::endl; }
        else       { file << "FAILED: Basic tensor test." << std::endl; }
        
        file.close();

        std::cout << "File './test_log' was appended." << std::endl;
    }
    else
    {
        std::cerr << "ERROR: Can not open './test_log' file." << std::endl;
        exit(EXIT_FAILURE);
    }

    if (check) { std::cout << "PASSED: Basic tensor test." << std::endl; }
    else       { std::cout << "FAILED: Basic tensor test." << std::endl; }

    return 0;
}

// test/test_tensor.cpp
