// test/test_cross.cpp

#include <omp.h>

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

#include "../src/tensor/cross.h"
#include "../src/utility/blas.h"
#include "../src/utility/memory.h"

#define MATRIX_SIZE 1000
#define EPS         1e-15

using namespace tpbes;

//============================================================================//
//  Relative matrix error
//============================================================================//
template<typename Matrix>
double Error(const Matrix & mat, Cross & cross)
{
    double mat_squared_norm = 0.;

#pragma omp parallel for collapse(2) reduction(+: mat_squared_norm)
    for (int r = 0; r < MATRIX_SIZE; ++r)
    {
        for (int c = 0; c < MATRIX_SIZE; ++c)
        {
            mat_squared_norm += std::pow(mat(r, c), 2.);
        }
    }

    double res = 0.;

#pragma omp parallel for collapse(2) reduction(+: res)
    for (int r = 0; r < MATRIX_SIZE; ++r)
    {
        for (int c = 0; c < MATRIX_SIZE; ++c)
        {
            res += std::pow(mat(r, c) - cross(r, c), 2.);
        }
    }

    return std::sqrt(res / mat_squared_norm);
}

//============================================================================//
//  Checking procedure
//============================================================================//
template<typename Matrix, typename Reference>
bool Check(
          std::ofstream & file,
    const Matrix        & mat,
          Cross         & cross,
          CrossParams     pars,
    const Reference     & ref
)
{
    pars.tolerance = ref.tolerance;
    cross.Approximate<Matrix>(mat, pars);

    double error = Error<Matrix>(mat, cross);

    file << std::setprecision(0) << std::scientific
        << mat.desc << ";" << std::endl << "Size: " << MATRIX_SIZE  << " x "
        << MATRIX_SIZE << ";  Tolerance = " << pars.tolerance << ";"
        << std::endl << std::setprecision(15)
        << "Reference values:   Cross rank = " << ref.rank 
        << ";  Relative error = " << ref.error << ";" << std::endl
        << "Calculated values:  Cross rank = " << cross.Get_rank()
        << ";  Relative error = " << error << "." << std::endl << std::endl;

    return cross.Get_rank() == ref.rank && std::abs(error - ref.error) < EPS;
}

//============================================================================//
//  Matrix cross-approximation test
//============================================================================//
int main(int argc, char ** argv)
{
    CrossParams pars;

    pars.iters_max_count = 0;
    pars.max_rank = 10;

    Cross cross;

    //========================================================================//
    //  Matrix of reciprocals
    //========================================================================//
    struct Reciprocal
    {
        std::string desc = "Matrix of reciprocals: A[i, j] = 1 / (i + j + 1)";

        Reciprocal() {}

        inline int Get_row_size() const { return MATRIX_SIZE; }
        inline int Get_col_size() const { return MATRIX_SIZE; } // CHANGED

        double operator()(const int row, const int col) const
        {
            return 1. / (row + col + 1.);
        }
    } reciprocal;

    struct Reference
    {
        int    rank;
        double tolerance;
        double error;

        Reference(int rk, double tol, double err):
            rank(rk), tolerance(tol), error(err)
        {}
    };

    Reference ref_5(14, 1e-5, 5.347088601076301e-07);
    Reference ref_6(16, 1e-6, 9.877107278934539e-08);
    Reference ref_7(18, 1e-7, 1.067637492505297e-09);

    std::ofstream file("test_log", std::ios::app);

    bool check = false;

    if (file.is_open())
    {
        file << "========================================"
             << "========================================" << std::endl;

        double time = omp_get_wtime();

        check =
              Check(file, reciprocal, cross, pars, ref_5)
            & Check(file, reciprocal, cross, pars, ref_6)
            & Check(file, reciprocal, cross, pars, ref_7)
            ;

        file << std::setprecision(2)
            << "Time = " << omp_get_wtime() - time << std::endl;

        std::cout << std::setprecision(3)
            << "Time = " << omp_get_wtime() - time << std::endl;

        if (check) { file << "PASSED: Cross-approximation test." << std::endl; }
        else       { file << "FAILED: Cross-approximation test." << std::endl; }
        
        file.close();

        std::cout << "File './test_log' was appended." << std::endl;
    }
    else
    {
        std::cerr << "ERROR: Can not open './test_log' file." << std::endl;
        exit(EXIT_FAILURE);
    }

    if (check)
    {
        std::cout << "PASSED: Cross-approximation test." << std::endl;
    }
    else { std::cout << "FAILED: Cross-approximation test." << std::endl; }

    return 0;
}

// test/test_cross.cpp
