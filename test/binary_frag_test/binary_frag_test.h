#ifndef BINARY_FRAG_TEST_H
#define BINARY_FRAG_TEST_H

#include <cmath>

#include "../../src/utility/utility.h"

//============================================================================//
//  Binary fragmentation test routines
//============================================================================//

double Initial_condition(const int size) { return size? 0.: 1.; }

double Aggr_coefficient(const int arity, const int * sizes) { return .5; }

double Binary_frag_coefficient(const int row, const int col, void * pars)
{
    const int ind[2] = { row, col };
    const double lambda = *((double *)pars);

    return lambda * Aggr_coefficient(2, ind);
}

double Binary_frag_mass_distribution(const int size) { return size? 0.: 1.; }

double Analytic_solution_con(const double & lambda, const double & time)
{
    return (lambda == 0.)?
        2. / (2. + time):
        2. * lambda / (1. + 2. * lambda - std::exp(-lambda * time));
}

#endif // BINARY_FRAG_TEST_H
