// test/binary_frag_test/binary_frag_test.cpp

#include "binary_frag_test.h"

#include "../../src/scheme/scheme.h"

//============================================================================//
//  Integration test
//============================================================================//
int main(int argc, char ** argv)
{
    //========================================================================//
    //  Read config
    //========================================================================//
    // check if configuration filename is specified
    tpbes::Check_config_presence(argc);

    tpbes::SchemeParameters params;

    // read configuration parameters
    tpbes::Read_config(argv[1], params);

    //========================================================================//
    //  Initialize scheme
    //========================================================================//
    // initialize integration scheme
    tpbes::Scheme scheme(params);

    // set monodisperse initial condition data
    scheme.Set_initial_condition_data(&Initial_condition);

    // set aggregation term
    scheme.Set_aggr_data(&Aggr_coefficient);

    // set binary fragmentation term
    double lambda = 0.001;

    if (lambda)
    {
        scheme.Set_binary_frag_data(&Binary_frag_coefficient, (void *)&lambda);
        scheme.Set_binary_frag_distr(&Binary_frag_mass_distribution);
    }

    //========================================================================//
    //  Integration process
    //========================================================================//
    const int size = params.mode + 1;
    double * init_cons = (double *)malloc(size * sizeof(double));
    double * res_cons = (double *)malloc(size * sizeof(double));

    // initialize concentrations
    scheme.Initial_conditions(init_cons);

    // integration
    scheme.Integrate(init_cons, res_cons);

    //========================================================================//
    //  Print info
    //========================================================================//
    double num_con = scheme.Get_current_con();
    double an_con = Analytic_solution_con(lambda, params.final_time);
    double delta = std::abs(num_con - an_con);

    // print result concentration
    std::cout << "Numeric concentration = " << std::setprecision(15)
        << std::scientific << num_con << std::endl;

    // print analytic concentration
    std::cout << "Analytic concentration = " << std::setprecision(15)
        << std::scientific << an_con << std::endl;

    // print result mass
    std::cout << "Absolute delta = " << std::setprecision(15) << std::scientific
        << delta << std::endl;

    if (init_cons) { free(init_cons); }
    if (res_cons) { free(res_cons); }

    return 0;
}

// test/binary_frag_test/binary_frag_test.cpp
