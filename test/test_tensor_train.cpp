// test/test_tensor_train.cpp

#include <omp.h>

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

#include "../src/utility/blas.h"
#include "../src/utility/memory.h"
#include "../src/tensor/tensor.h"
#include "../src/tensor/tensor_train.h"

#define TENSOR_DIM  3
#define TENSOR_MODE 100
#define EPS         1e-15

using namespace tpbes;

//============================================================================//
//  Relative tensor error
//============================================================================//
template<typename Tensor>
double Error(Tensor & ten, TensorTrain & train)
{
    double res = 0.;

// #pragma omp parallel for reduction(+: res)
    for (int s = 0; s < Size<Tensor>(ten); ++s)
    {
        int inds[ten.Get_dimension()];

        double tmp = Element<Tensor>(ten, s, inds);
#pragma omp critical
        //std::cout << "INDS = [" << inds[0] << ", " << inds[1] << ", " << inds[2] << "]" << std::endl;
        res += std::pow(tmp - train[inds], 2.);
    }

    return std::sqrt(res) / Norm<Tensor>(ten);
}

//============================================================================//
//  Checking procedure
//============================================================================//
template<typename Tensor, typename Reference>
bool Check(
          std::ofstream     & file,
          Tensor            & ten,
          TensorTrain       & train,
          TensorTrainParams & pars,
    const Reference         & ref
)
{
    pars.tolerance = ref.tolerance;

    train.Approximate<Tensor>(ten, pars);

    const int    dim   = ten.Get_dimension();
    const double error = Error<Tensor>(ten, train);

    const int * ranks = train.Get_ranks();

    file << std::setprecision(0) << std::scientific << ten.desc << ";"
        << std::endl << "Size: " << ten.Get_mode(0);
    
    for (int d = 1; d < dim; ++d) { file << " x " << ten.Get_mode(d); }

    file << ";  Tolerance = " << pars.tolerance << ";" << std::endl
        << std::setprecision(15) << "Reference values:   train ranks =";

    for (int r = 0; r <= dim; ++r) { file << " " << ref.ranks[r]; }

    file << ";  Relative error = " << ref.error << ";" << std::endl
        << "Calculated values:  train ranks =";
    
    for (int r = 0; r <= dim; ++r) { file << " " << ranks[r]; }

    file << ";  Relative error = " << error << "." << std::endl << std::endl;

    for (int r = 0; r <= dim; ++r)
    {
        if (train.Get_rank(r) != ref.ranks[r]) { return false; }
    }

    return std::abs(error - ref.error) < EPS;
}

//============================================================================//
//  Tensor train-approximation test
//============================================================================//
int main(int argc, char ** argv)
{
    TensorTrainParams pars;

    //pars.iters_max_count = 0;
    //pars.max_rank = 10;

    TensorTrain train;

    //========================================================================//
    //  Tensor of reciprocals
    //========================================================================//
    struct Reciprocal
    {
        int dimension;

        int * modes;

        std::string desc =
            "Tensor of reciprocals: A[i1 + i2 + ... id] "
            "= 1 / (i1 + i2 + ... + id + 1)";

        ~Reciprocal() { Free<int>(modes); }

        Reciprocal(const int d, const int * m): dimension(d), modes(NULL)
        {
            modes = Allocate<int>(d, m);
        }

        inline int Get_dimension() const { return dimension; }
        inline int Get_mode(const int d) const { return modes[d]; }

        double operator[](const int * inds) const
        {
            int sum = 0;
            
            for (int d = 0; d < dimension; ++d) { sum += inds[d]; }
            
            return 1. / (sum + 1.);
        }
    };

    int modes[TENSOR_DIM]; 
    
    for (int d = 0; d < TENSOR_DIM; ++d) { modes[d] = TENSOR_MODE; }

    Reciprocal reciprocal(TENSOR_DIM, modes);

    struct Reference
    {
        int * ranks;

        double tolerance;
        double error;

        Reference(int * rks, double tol, double err):
            ranks(rks), tolerance(tol), error(err)
        {}
    };

    int ranks_5[4] = { 1, 11, 12, 1 };
    int ranks_6[4] = { 1, 13, 13, 1 };
    int ranks_7[4] = { 1, 14, 15, 1 };

    Reference ref_5(ranks_5, 1e-5, 2.053979714328813e-07);
    Reference ref_6(ranks_6, 1e-6, 1.015489034630437e-08);
    Reference ref_7(ranks_7, 1e-7, 3.205691081674384e-10);

    std::ofstream file("test_log", std::ios::app);

    bool check = false;

    if (file.is_open())
    {
        file << "========================================"
             << "========================================" << std::endl;

        double time = omp_get_wtime();

        check =
              Check(file, reciprocal, train, pars, ref_5)
            & Check(file, reciprocal, train, pars, ref_6)
            & Check(file, reciprocal, train, pars, ref_7)
            ;

        file << std::setprecision(2)
            << "Time = " << omp_get_wtime() - time << std::endl;

        std::cout << std::setprecision(3)
            << "Time = " << omp_get_wtime() - time << std::endl;

        if (check)
        {
            file << "PASSED: Tensor Train approximation test." << std::endl;
        }
        else
        {
            file << "FAILED: Tensor Train approximation test." << std::endl;
        }
        
        file.close();

        std::cout << "File './test_log' was appended." << std::endl;
    }
    else
    {
        std::cerr << "ERROR: Can not open './test_log' file." << std::endl;
        exit(EXIT_FAILURE);
    }

    if (check)
    {
        std::cout << "PASSED: Tensor Train approximation test." << std::endl;
    }
    else
    {
        std::cout << "FAILED: Tensor Train approximation test." << std::endl;
    }

    return 0;
}

// test/test_tensor_train.cpp
