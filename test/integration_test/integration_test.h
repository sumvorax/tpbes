#ifndef INTEGRATION_TEST_H
#define INTEGRATION_TEST_H

#include <cmath>

#include "../../src/utility/utility.h"

//============================================================================//
//  Integration test
//============================================================================//

double Initial_condition(const int size)
{
    return size? 0.: 1.;
}

double Source(const int size)
{
    return 0.;
}

double Source(const int size, const double & time)
{
    return 0.;
}

double Aggr_coefficient(const int arity, const int * sizes)
{
    return 1. / tpbes::Fact(arity);
}

double Unary_frag_coefficient(const int size)
{
    return 0.;
}

double Unary_frag_mass_distribution(const int size)
{
    return 0.;
}

double Binary_frag_coefficient(const int row, const int col)
{
    return 0.;
}

double Binary_frag_mass_distribution(const int size)
{
    return 0.;
}

#endif // INTEGRATION_TEST_H
