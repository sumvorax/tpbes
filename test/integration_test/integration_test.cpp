// test/integration_test/integration_test.cpp

#include "integration_test.h"

#include "../../src/scheme/scheme.h"

//============================================================================//
//  Integration test
//============================================================================//
int main(int argc, char ** argv)
{
    //========================================================================//
    //  Read config
    //========================================================================//
    // check if configuration filename is specified
    tpbes::Check_config_presence(argc);

    tpbes::SchemeParameters params;

    // read configuration parameters
    tpbes::Read_config(argv[1], params);

    //========================================================================//
    //  Initialize scheme
    //========================================================================//
    // initialize integration scheme
    tpbes::Scheme scheme(params);

    // set monodisperse initial condition data
    scheme.Set_initial_condition_data(&Initial_condition);

    // set aggregation term
    scheme.Set_aggr_data(&Aggr_coefficient);

    //========================================================================//
    //  Integration process
    //========================================================================//
    const int size = params.mode + 1;
    double * init_cons = (double *)malloc(size * sizeof(double));
    double * res_cons = (double *)malloc(size * sizeof(double));

    // initialize concentrations
    scheme.Initial_conditions(init_cons);

    // integration
    scheme.Integrate(init_cons, res_cons);

    //========================================================================//
    //  Print info
    //========================================================================//
    // print resulting mass
    std::cout << "Mass = " << std::setprecision(15) << std::scientific
        << scheme.Get_current_mass() << std::endl;

    if (init_cons) { free(init_cons); }
    if (res_cons) { free(res_cons); }

    return 0;
}

// test/integration_test/integration_test.cpp
